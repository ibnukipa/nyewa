@if(!Auth::guest() && Auth::user() != $iklan->user)
<form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/iklan/rating') }}" enctype="multipart/form-data">
    <input type="hidden" name="iklan_id" value="{{ $iklan->id }}">
    {{ csrf_field() }}
    <div class="row" style="padding: 0 1rem">
        <div class="col-md-12">
            <div class="form-group has-feedback {{ $errors->has('feedback') ? ' has-error' : '' }}">
                <label for="rating">Rating Anda <span>*</span></label>
                <input data-active-icon="glyphicon-star" data-inactive-icon="glyphicon-star kosong" value="" type="number" name="rating" class="rating large" required/>
            </div>
        </div>
    </div>

    <div class="row" style="padding: 0 1rem">
        <div class="col-md-12 mr-top2">
            <div class="form-group has-feedback {{ $errors->has('feedback') ? ' has-error' : '' }}">
                <label for="description">Feedback Anda <span>*</span></label>
                <span class="form-control-feedback-icon" aria-hidden="false">
                    <i class="material-icons" style="line-height: inherit; font-size: 23px;">description</i>
                </span>

                <textarea  data-trigger="focus" 
                        data-toggle="popover"
                        data-content="Tuliskan deskripsi yang jelas, singkat dan padat untuk menarik perhatian pelanggan" 
                        id="feedback" class="form-control effect" 
                        placeholder="Masukkan feedback Anda untuk iklan: {{ $iklan->name }}"  
                        name="feedback"
                        style="max-width: 100%; min-height: 8rem"
                        value="{{ old('feedback') }}"
                        required
                        ></textarea>

                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
        </div>
    </div>
    <div class="row" style="padding-left: 1rem; margin-top: 2rem">
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::button('<i class="material-icons middle">send</i> <span class="middle"> Kirim Feedback</span>', array('class' => 'btn-dark blue','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
            </div>
        </div>
    </div>
</form>

<div class="row mr-top2">
    <div class="col-md-12">
        <div class="divider"></div>
    </div>
</div>
@endif
@foreach($iklan->feedbacks as $feedback)
<div class="row">
    <div class="col-md-12">
        <div style="color: rgba(34, 34, 34, .5)">
            <input  data-active-icon="glyphicon-star" 
                    data-inactive-icon="glyphicon-star kosong" 
                    type="number" name="rating" class="rating inline"
                    value="{{ $feedback->rating_value }}"
                    required data-readonly/>
            <span class="middle" style="margin-left: .5rem; font-size: 1.5rem; font-weight: 600; margin-top: 5px;">
                {{ $feedback->user->name }}
            </span>
            <span class="pull-right">
                {{ date(' d ', strtotime($feedback->updated_at)) }}
                {{ $time->bulan[date('n', strtotime($feedback->updated_at))] }}
                {{ date(' Y', strtotime($feedback->updated_at)) }}
                -
                {{ date('H:i:s', strtotime($feedback->updated_at)) }}
            </span>
        </div>
        <p class="mr-top1">
            <span style="font-style: italic">"{{ $feedback->feedback_content }}"</span>
        </p> 
    </div>  
</div>
@endforeach