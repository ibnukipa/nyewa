@extends('layouts.app')

@section('template_title')
    Dashboard
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-sewaaja dashboard">
                {{-- <div class="panel-heading grey-dark">Statistika</div> --}}
                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('partials.footer')
@endsection
