{{---
@include('builder.form',[
    'col_width'         => 12,
    'tipe_input'        => 'input',
    'input_type'        => 'password',
    'icon_input'        => 'fiber_new',
    'id_input'          => 'new_password',
    'value_input'       => Auth::user()->nohp,
    'after_input'       => 'm<sup>2</sup>',
    'name_input'        => 'Password Baru',
    'popover_content'   => 'Masukkan password baru Anda',
    'popover_position'  => 'bottom',
    'foot_note'         => $foot_note,

    'match'             => '#idtomatch',
    //'match_content'     => 'Whoops, konfirmasi password tidak cocok'
])
--}}
@if($tipe_input == 'input')
<div class="col-md-{{ $col_width }}">
    <div class="form-group has-feedback {{ $errors->has($id_input) ? ' has-error' : '' }}">
        <label for="{{ $id_input }}">{{ $name_input }}@if(isset($required)) <span>*</span>@endif</label>

        <span class="form-control-feedback-icon" aria-hidden="false">
            <i class="material-icons" style="line-height: inherit; font-size: 23px;">@if(isset($icon_input)) {{$icon_input}} @else donut_small @endif</i>
        </span>

        <input  data-trigger="focus"
                @if(isset($popover_position))
                data-placement="{{$popover_position}}"
                @endif
                @if(isset($popover_content))
                data-toggle="popover"
                data-content="{{ $popover_content }}" 
                @endif
                @if(isset($match))
                data-match="{{$match}}"
                {{-- data-match-error="{{ $match_content }}" --}}
                @endif
                @if(isset($minlength))
                data-minlength="{{ $minlength }}"
                @endif
                id="{{ $id_input }}" class="form-control effect" 
                placeholder="{{ $name_input }}" type="{{ $input_type }}" name="{{ $id_input }}" value="@if(isset($value_input)){{$value_input}}@else{{old($id_input)}}@endif"
                @if(isset($required))
                required
                @endif
                >
        @if(isset($after_input))
        <span class="form-control-feedback-icon" aria-hidden="false" style="top: 2.5rem;left: inherit;right: -4rem;font-size: 2rem;font-weight: 600;color: rgba(0, 0, 0, .4);">
            {!! $after_input !!}
        </span>
        @endif
        <span class="help-block pull-right effect">
            @if ($errors->has($id_input))
                {{ $errors->first($id_input) }}
            @else
                @if(isset($foot_note))
                {!! $foot_note !!}
                @endif
            @endif
        </span>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
    </div>
</div>
@endif

@if($tipe_input == 'select')
<div class="row">
    <div class="col-md-{{ $col_width }} col-md-offset-1">
        <div class="form-group has-feedback {{ $errors->has($id_input) ? ' has-error' : '' }}">
            <label for="{{ $id_input }}">{{ $name_input }} <span>*</span></label>
            @if(isset($icon) && !$icon)
            @else
            <span class="form-control-feedback-icon" aria-hidden="false">
                <i class="material-icons" style="line-height: inherit; font-size: 23px;">@if(isset($icon_input)) {{$icon_input}} @else donut_small @endif</i>
            </span>
            @endif
            {!! Form::select($id_input, array() , null, array( $select_type => '', 'class' =>'js-example-basic-'.$select_type.' js-states form-control', 'id' => $id_input, 'style' => '', 'required' => 'required')) !!}
            <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
        </div>
    </div>
</div>
<script>
    $('#'+'{{$id_input}}').select2({
        placeholder: 'Pilih '+ '{{$name_input}}',
        data: {!! json_encode($data) !!}
    });
</script>
@endif

@if($tipe_input == 'textarea')
<div class="row">
    <div class="col-md-12 col-md-offset-1">
        <div class="col-md-{{ $col_width }} no-padding">
            <div class="form-group has-feedback {{ $errors->has($id_input) ? ' has-error' : '' }}">
                <label for="{{ $id_input }}">{{ $name_input }}<span>*</span></label>

                <span class="form-control-feedback-icon" aria-hidden="false">
                    <i class="material-icons" style="line-height: inherit; font-size: 23px;">@if(isset($icon_input)) {{$icon_input}} @else donut_small @endif</i>
                </span>

                <textarea   data-trigger="focus" 
                            data-toggle="popover"
                            data-content="{{$popover_content}}" 
                            id="{{ $id_input }}" class="form-control effect" 
                            placeholder="{{ $name_input }}"  
                            name="{{ $id_input }}"
                            style="max-width: 100%; min-height: 10rem"
                            value="{{ old($id_input) }}"
                            required
                            ></textarea>

                @if(isset($after_input))
                <span class="form-control-feedback-icon" aria-hidden="false" style="top: 2.5rem;left: inherit;right: -4rem;font-size: 2rem;font-weight: 600;color: rgba(0, 0, 0, .4);">
                    {!! $after_input !!}
                </span>
                @endif
                <span class="help-block pull-left effect">
                    @if ($errors->has($id_input))
                        {{ $errors->first($id_input) }}
                    @else
                        {{ $foot_note }}
                    @endif
                </span>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
        </div>
    </div>
</div>
@endif