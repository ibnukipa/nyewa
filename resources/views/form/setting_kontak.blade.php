<form id="formupdatekontak" class="form-horizontal" role="form" method="POST" action="{{ url('/setting/kontak/update') }}">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            @include('partials.status')
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="col-md-12">
                <div class="form-group has-feedback {{ $errors->has('provinsi') ? ' has-error' : '' }}">
                    <label for="provinsi">Provinsi <span>*</span></label>
                    <span class="form-control-feedback-icon" aria-hidden="false">
                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">place</i>
                    </span>

                    {!! Form::select('provinsi', array(null => null) , null, array( 'class' =>'js-example-basic-single js-states form-control', 'id' => 'provinsi', 'style' => '', 'required' => 'required')) !!}
                    <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="col-md-12">
                <div class="form-group has-feedback {{ $errors->has('kota') ? ' has-error' : '' }}">
                    <label for="kota">Kota <span>*</span></label>
                    <span class="form-control-feedback-icon" aria-hidden="false">
                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">place</i>
                    </span>

                    {!! Form::select('kota', array(null => null) , null, array( 'class' =>'js-example-basic-single js-states form-control', 'id' => 'kota', 'style' => '', 'required' => 'required')) !!}
                    <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row mr-top2">
        <div class="col-md-5">
            @include('builder.form', 
            [
                'tipe_input'        => 'input',
                'input_type'        => 'text',
                'icon_input'        => 'person',
                'value_input'       => Auth::user()->name,
                'required'          => true,
                'col_width'         => 12,
                'id_input'          => 'name',
                'name_input'        => 'Nama Lengkap',
                //'popover_content'   => 'Nama Lengkap Anda',
                //'foot_note'         => 'contoh: 30, 50, dll',
            ])
        </div>
        <div class="col-md-4">
            
            {!!
                $foot_note = '';
                if(Auth::user()->nohpaktif)
                    $foot_note = "<i class='material-icons middle' style='font-size: 1.5rem; color: #1976D2'>verified_user</i> terverifikasi";
            !!}
            @include('builder.form', 
            [
                'tipe_input'        => 'input',
                'input_type'        => 'text',
                'icon_input'        => 'call',
                'value_input'       => Auth::user()->nohp,
                'col_width'         => 12,
                'id_input'          => 'nohp',
                'name_input'        => 'Nomer Telepon / Nomer HP',
                'popover_content'   => 'Masukkan Nomer HP Anda untuk mempermudah penyewa menghubungi Anda',
                'popover_position'  => 'bottom',
                'required'          => true,
                'foot_note'         => $foot_note,
            ])
        </div>
        <div class="col-md-3">
            {!!
                $foot_note = '';
                if(Auth::user()->nohpaktif)
                    $foot_note = "<i class='material-icons middle' style='font-size: 1.5rem; color: #1976D2'>verified_user</i> terverifikasi";
            !!}
            @include('builder.form', 
            [
                'tipe_input'        => 'input',
                'input_type'        => 'text',
                'icon_input'        => 'chat_bubble',
                'value_input'       => Auth::user()->pinbb,
                'col_width'         => 12,
                'id_input'          => 'pinbb',
                'name_input'        => 'PIN BB',
                'popover_content'   => 'Masukkan PIN BB untuk mempermudah penyewa menghubungi Anda',
                'popover_position'  => 'bottom',
                'foot_note'         => $foot_note,
            ])
        </div>
    </div>
    <div class="row mr-top2">
        <div class="col-md-4">
            {!! Form::button('<i class="material-icons middle">save</i> <span class="middle"> Simpan</span>', array('class' => 'btn-dark blue','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
        </div>
    </div>
</form>

<script>
var listProvinsi = [
    @foreach (Indonesia::allProvinces() as $value)
        { id: '{{ $value->id }}', text: unEntity('{{ $value->name }}') },
    @endforeach  
];

function unEntity(str){
    return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
}

$('#' + 'provinsi').select2({
    data: listProvinsi,
    placeholder: 'Cari provinsi Anda...'
});

$('#kota').select2({
    placeholder: 'Cari kota Anda...'
});

@if(Auth::user()->provinsi_id)
    $('#provinsi').val('{{Auth::user()->provinsi_id}}').change();
    var listKota = [
        @foreach (Indonesia::findProvince(Auth::user()->provinsi_id, ['cities'])->cities as $value)
            { id: '{{ $value->id }}', text: unEntity('{{ $value->name }}') },
        @endforeach  
    ];
    $('#kota').select2({
        data: listKota,
        placeholder: 'Cari kota Anda...'
    });

    $('#kota').val('{{Auth::user()->kota_id}}').change();
@endif

$('#provinsi').on('change', function() {
    var provinsi_id = $(this).val();    
    $.ajax({
        url: '{{ url('/setting/ajak/getKota') }}',
        type: 'GET',
        datatype: 'JSON',
        data: { provinsi_id: provinsi_id },
    }).then(function (response) {
        var select = document.getElementById("kota");
        var length = select.options.length;
        for (i = 0; i < length; i++) {
            select.options[i] = null;
        }
        
        $('#kota').select2({
            placeholder: 'Cari kota Anda...',
            data: response
        });
    });
});
</script>