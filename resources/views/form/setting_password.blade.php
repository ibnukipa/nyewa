<form id="formgantipassword" class="form-horizontal" role="form" method="POST" action="{{ url('/setting/password/update') }}">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-12">
            @include('partials.status')
        </div>
    </div>
    <div class="row mr-top2">
        <div class="col-md-6">
            @include('builder.form',[
                'col_width'         => 12,
                'tipe_input'        => 'input',
                'input_type'        => 'password',
                'icon_input'        => 'lock',
                'id_input'          => 'password_baru',
                'name_input'        => 'Password Baru',
                'popover_content'   => 'Masukkan password baru untuk verifikasi akun Anda setiap masuk SewaAja',
                'foot_note'         => 'minimal 6 karater',
                'minlength'             => '6',
            ])
        </div>
    </div>

    <div class="row mr-top2">
        <div class="col-md-6">
            @include('builder.form',[
                'col_width'             => 12,
                'tipe_input'            => 'input',
                'input_type'            => 'password',
                'icon_input'            => 'lock',
                'id_input'              => 'password_baru_confirmation',
                'name_input'            => 'Konfirmasi Password Baru',
                'popover_content'       => 'Masukkan kembali password baru Anda',
                'foot_note'             => 'konfirmasi password',
                'minlength'             => '6',
                'match'                 => '#password_baru',
                'match_content'         => 'Whoops, konfirmasi password baru tidak cocok'
            ])
        </div>
    </div>
    <div class="row mr-top2">
        <div class="col-md-4">
            {!! Form::button('<i class="material-icons middle">update</i> <span class="middle"> Update</span>', array('class' => 'btn-dark blue','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
        </div>
    </div>
</form>