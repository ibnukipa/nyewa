<script type="text/javascript">
	$('#detailPesan').on('show.bs.modal', function (e) {
        var pesan_id  = $(e.relatedTarget).attr('data-pesan-id');
        $.ajax({
            url         : '{{ url('/pesan/detail/detail/a') }}',
            type        : 'GET',
            datatype    : 'JSON',
            data        : {pesan_id: pesan_id},
            success     : function(data) {
                $('#detailPesan .modal-content').removeClass('hide');
                $('#detailPesan .modal-loading-image').removeClass('show');
                $('#detailPesan').find('.modal-content').html(data);
            },
            beforeSend  : function(jqXHR, options) {
                //progress code
                $('#detailPesan .modal-content').addClass('hide');
                $('#detailPesan .modal-loading-image').addClass('show');
                setTimeout(function() {
                    $.ajax($.extend(options, {beforeSend: $.noop}));
                }, 1000);
                return false;
            }
        });
	});

    $('#detailPesan').on('hidden.bs.modal', function (e) {
        location.reload();
    });
</script>