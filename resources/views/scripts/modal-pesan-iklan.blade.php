<script type="text/javascript">
	$('#sendPesan').on('show.bs.modal', function (e) {
        var pesan_nama_pelapak  = $(e.relatedTarget).attr('data-iklan-pelapak');
		var pesan_nama_iklan    = $(e.relatedTarget).attr('data-iklan-name');
        var iklan_id            = $(e.relatedTarget).attr('data-iklan-id');
        var to_user_id          = $(e.relatedTarget).attr('data-iklan-to_user_id');

		$(this).find('.modal-body #pesan_nama_pelapak').html(pesan_nama_pelapak);
		$(this).find('.modal-body #pesan_nama_iklan').html(pesan_nama_iklan);
        $(this).find('.modal-body #iklan_id').val(iklan_id);
        $(this).find('.modal-body #to_user_id').val(to_user_id);
	});
</script>