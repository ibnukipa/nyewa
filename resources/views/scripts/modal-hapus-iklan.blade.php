<script type="text/javascript">
	$('#confirmDelete').on('show.bs.modal', function (e) {
        var notif   = $(e.relatedTarget).attr('data-body-title');
		var message = $(e.relatedTarget).attr('data-body-content');
		var form    = $(e.relatedTarget).closest('form');

		$(this).find('.modal-body #body-title').html(notif);
		$(this).find('.modal-body #body-content').html(message);
		$(this).find('.modal-footer #confirm').data('form', form);
	});

	$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
	  	$(this).data('form').submit();
	});
</script>