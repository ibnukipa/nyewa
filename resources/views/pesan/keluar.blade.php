<div class="pencarian-pesan">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group has-feedback {{ $errors->has('pencarian_pesan') ? ' has-error' : '' }}">
                <label for="pencarian_pesan">Tampilkan Pesan</label>
                <span class="form-control-feedback-icon" aria-hidden="false">
                    <i class="material-icons" style="line-height: inherit; font-size: 23px;">sort</i>
                </span>

                {!! Form::select('pencarian_pesan[]', array() , null, array( 'class' =>'js-example-basic-single js-states form-control', 'id' => 'pencarian_pesan', 'style' => '', 'required' => 'required')) !!}
                <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
            </div>
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-hover" style="background-color: white">
        <thead style="font-size: 1.3rem; font-weight: 600; color: rgba(33, 33, 33, .5);">
                <td style="padding: 0" width="5%"></td>
                <td style="padding: 0" width="20%"></td>
                <td style="padding: 0" width="60%"></td>
                <td style="padding: 0" width="15%"></td>
        </thead>
        <tbody>
            @foreach($listPesan as $key => $value)
                <?php  $pesan_masuk_dari    = Auth::user()->pesan_masuk()->where('iklan_id', '=', $value->iklan_id)->where('from_user_id', '=', $value->user_penerima->id)->orderBy('created_at', 'desc')->get(); ?>
                <?php  $pesan_keluar_dari   = Auth::user()->pesan_keluar()->where('iklan_id', '=', $value->iklan_id)->where('to_user_id', '=', $value->user_penerima->id)->orderBy('created_at', 'desc')->get(); ?>
                @if(count($pesan_keluar_dari) > 0 && count($pesan_masuk_dari) > 0)
                <?php  $currentValue        = $pesan_keluar_dari[0]->created_at > $pesan_masuk_dari[0]->created_at ? $pesan_keluar_dari[0] : $pesan_masuk_dari[0]; ?>
                @else
                    @if(count($pesan_keluar_dari) > 0)
                        <?php  $currentValue        = $pesan_keluar_dari[0]; ?>
                    @else
                        <?php  $currentValue        = $pesan_masuk_dari[0]; ?>
                    @endif    
                @endif
                <tr style="@if($currentValue->seen == 0 && $currentValue->user_pengirim != Auth::user()) background-color: #ffc; @endif cursor: pointer" data-pesan-id="{{ $value->id }}" data-backdrop="static" data-toggle="modal" data-target="#detailPesan">
                    <td>
                        
                    </td>
                    <td>
                        @if($currentValue->user_pengirim == Auth::user())
                            <i class="material-icons middle">reply</i>
                        @endif
                        @if($currentValue->seen == 0 && $currentValue->user_pengirim == Auth::user())
                            <strong>saya</strong>
                        @else
                            saya
                        @endif 
                        <span style="font-weight: 400">
                            ({!! count($pesan_keluar_dari) !!})
                        </span>
                        ,
                        @if($currentValue->seen == 0 && $currentValue->user_pengirim != Auth::user())
                            <strong>{!! $value->user_penerima->name !!}</strong>
                        @else
                            {!! $value->user_penerima->name !!}
                        @endif 
                        <span style="font-weight: 400">
                            ({!! count($pesan_masuk_dari) !!})
                        </span>
                    <td>
                        <div style="height: 2rem; overflow: hidden">
                            <span style="@if($currentValue->seen == 0) font-weight: 600; @endif">
                                {!! $currentValue->iklan->name !!}
                            </span>
                            <span style="color: rgba(34, 34, 34, .5)">
                            - {!! str_replace("<br>"," " ,$currentValue->value) !!}
                            </span>
                        </div>
                    </td>
                    <td>{{ $currentValue->created_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>

var list_pencarian    = [
    {
        id: '0',
        text: 'Semua pesan iklan orang lain',
    },
    @if(isset($listPesan2))
    @foreach ($listPesan2 as $value)
        { id: 'iklan_{{ $value->iklan_id }}', text: unEntity('- {{ $value->iklan->name }}') },
    @endforeach
    @else
    @foreach ($listPesan as $value)
        { id: 'iklan_{{ $value->iklan_id }}', text: unEntity('- {{ $value->iklan->name }}') },
    @endforeach
    @endif
    
];

function unEntity(str){
    return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
}

$('#' + 'pencarian_pesan').select2({
    data: list_pencarian,
    placeholder: 'Cari pesan...'
});

@if(isset($id_iklan_selected_by_sorting))
    $("#pencarian_pesan").val("iklan_{{ $id_iklan_selected_by_sorting }}").trigger('change');;
@endif

$('#pencarian_pesan').on('change', function() {
    if(this.value != 0 )
        window.location.href = "{{ url('/pesan/keluar/') }}"+"/"+ this.value.replace("iklan_","");
    else
        window.location.href = "{{ url('/pesan/keluar/') }}";
});
</script>