<div class="modal-header blue" style="font-size: 2rem; line-height: 2rem; padding: 1rem; font-weight: 600" >
    
    <div class="inline pull-left" style="line-height: 3.5rem;margin-right: 1rem;">
        <i class="material-icons middle" style="font-size: 3rem">email</i>
    </div>
    <div class="inline" >
        @if(Auth::user() != $pesanOne->user_pengirim)
        <span class="middle" style="display: block; font-weight: 500">{{$pesanOne->user_pengirim->name}}</span>
        <span style="display: block; font-size: 1.5rem; color: rgba(255, 255, 255, .7); font-weight: 500">{{ $pesanOne->user_pengirim->email }}</span>
        @else
        <span class="middle" style="display: block; font-weight: 500">{{$pesanOne->user_penerima->name}}</span>
        <span style="display: block; font-size: 1.5rem; color: rgba(255, 255, 255, .7); font-weight: 500">{{ $pesanOne->user_penerima->email }}</span>
        @endif
    </div>
    <div class="inline pull-right">
        {!! Form::button('<i class="material-icons middle">close</i>', array('class' => 'btn circle white','type' => 'button', 'data-dismiss' => 'modal')) !!}
    </div>
</div>
<div class="modal-body no-padding">
    <div class="list-group-item hovered" style="min-height: 10rem; max-height: 10rem; border: 1px solid #ddd;">
        <div class="row">
            <div class="col-md-2 no-padding-right">
                <div class="image">
                    {{HTML::image(url('img/'.$value->id.'/'.$value->gambarIklan[0]->file_name.'/200'), $alt="Photo", $attributes = array('style' => 'display: block; height: 10rem; width: auto', 'onload' => 'imgLoaded(this)')) }}
                </div>

            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-10 no-padding-right">
                        <div class="content-iklan" style="padding: 0; padding-top: 1rem">
                            <h4 class="list-group-item-heading" style="min-height: 4rem; max-height: 3rem;">
                                {{ $value->name }}
                            </h4>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding-left">
                            <?php $jenisPaket = $value->paket()->where('active','=',1)->first();
                                if($jenisPaket) {
                                    $namePaket      = $jenisPaket->name;
                                    $paketStart     = $value->paket[0]->pivot->start_at;
                                    $paketEnd       = $value->paket[0]->pivot->end_at;
                                    $viewJenisPaket = HTML::image(url('asset/img/p_icon_'.$jenisPaket->value.'.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 3.6rem; position: absolute; right: 4rem; top: 1rem'));
                                } else {
                                    $viewJenisPaket = HTML::image(url('asset/img/p_icon_0.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 3.6rem; position: absolute; right: 4rem; top: 1rem')); 
                                }
                            ?>
                            <a href="#">
                                {{$viewJenisPaket}}
                            </a>                       
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 ">
                        <div class="content-iklan" style="padding: 0">
                            <div class="list-group-item-text" style="min-height: 6rem; max-height: 2rem; padding-top: .44rem">
                                <div class="child" style="font-size: 2rem">
                                    Rp. {!! number_format($value->waktuSewa[0]->harga, 0, ",", ".") !!}
                                    <span class="c-grey">({!! $value->waktuSewa[0]->waktu !!})</span>
                                </div>
                                <div class="child pull-right">
                                    <a target="_blank" href="{{ url('/iklan/detail/'.$value->url_iklan.'.html') }}" class="no-padding">
                                        {!! Form::button('<i class="material-icons middle">remove_red_eye</i><span class="middle"> Lihat</span>', array(
                                                    'class'             => 'btn btn-small transparent left',
                                                    'style'             => 'padding: 4px 12px',
                                                    )) !!}
                                    </a>
                                    @if($value->user == Auth::user())
                                    <a target="_blank" href="{{ url('/iklan/edit/'.$value->id) }}" class="no-padding">
                                        {!! Form::button('<i class="material-icons middle">edit</i><span class="middle"> Edit</span>', array(
                                                    'class'             => 'btn-dark btn-small grey-dark left',
                                                    'style'             => 'padding: 4px 12px',
                                                    )) !!}
                                    </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="isi_pesan" style="overflow-y: scroll; max-height: 55vh; overflow-x: hidden; padding: 1rem;">
        @foreach($pesans as $key => $pesan)
            @if($pesan->from_user_id != Auth::user()->id)
                <div class="row">
                    <div class="col-md-8">
                        <div class="head_pesan @if($key > 0) mr-top2 @endif" style="padding: 0 .5rem">
                            <span class="inline left" style="width: 100%; color: rgba(34, 34, 34, .5)">{{ date('Y/m/d, H:i:s', strtotime($pesan->created_at)) }}</span>
                        </div>
                        <div class="content_pesan">
                            <div class="well no-margin @if($pesan->seen) grey-light @endif" @if(!$pesan->seen) style="background-color: #ffc" @endif>
                                {!! $pesan->value !!}
                                <?php $pesan->seen = 1; $pesan->save(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-8 col-md-offset-4">
                        <div class="head_pesan mr-top2" style="padding: 0 .5rem; font-size: 1.1rem">
                            <span class="inline left" style="width: 100%; color: rgba(34, 34, 34, .5)">{{ date('Y/m/d, H:i:s', strtotime($pesan->created_at)) }}</span>
                        </div>
                        <div class="content_pesan">
                            <div class="well no-margin blue">{!! $pesan->value !!}</div>
                            @if(strtotime($pesan->updated_at) > strtotime($pesan->created_at) && $pesan->seen == 1)
                            <div style="padding: 0 .5rem">
                                <span class="dilihat inline right" style="width: 100%; color: rgba(34, 34, 34, .5); font-size: 1.1rem">
                                    <i class="material-icons middle" style="font-size: 1.4rem">visibility</i><span class="middle"> {{ date('Y/m/d, H:i:s', strtotime($pesan->updated_at)) }}</span>
                                </span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        
    </div>
    <script>
        var $chat = $(".isi_pesan");
        $chat.scrollTop($chat[0].scrollHeight);
    </script>
</div>
<div class="modal-footer" style="border-top: 1px solid rgba(34, 34, 34, .1)">
{!! Form::open(array('url' => '/pesan/balas', 'class' => 'form-button', 'method' => 'POST', 'id' => 'form_balas_pesan')) !!}
        {!! Form::hidden('to_user_id', Auth::user()->id) !!}
        @if($pesanOne->user_pengirim->id == Auth::user()->id)
            {!! Form::hidden('from_user_id', $pesanOne->user_penerima->id) !!}
        @else
            {!! Form::hidden('from_user_id', $pesanOne->user_pengirim->id) !!}
        @endif
        {!! Form::hidden('iklan_id', $pesanOne->iklan->id) !!}
        <div class="row">
            <div class="col-md-10 no-padding-right">
                <div class="form-group" style="margin-right: 2rem">
                    <textarea  data-trigger="focus" 
                            data-toggle="popover"
                            data-content="Tuliskan pesan yang singkat, padat, dan jelas. Agar cepat mendapatkan respon dari pelapak" 
                            id="isi_pesan" class="form-control effect" 
                            placeholder="Tulis pesan.."  
                            name="isi_pesan"
                            style="max-width: 100%; min-height: 1rem; height: auto;"
                            value="{{ old('isi_pesan') }}"
                            onkeydown=""
                            required
                            ></textarea>
                    <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
                </div>
            </div>
            <div class="col-md-2 no-padding">
                {!! Form::button(   '<i class="material-icons middle">reply</i> <span class="middle"> Balas</span>', 
                                    array(  'class'             => 'btn-dark blue left',
                                            'style'             => 'padding: 4px 12px; float: left; margin-left: -1rem',
                                            'type'              => 'submit', 
                                        )
                                ) 
                !!}
            </div>
        </div>
        
{!! Form::close() !!}
</div>
<script>
    $(document).ready(function() {
        $('#form_balas_pesan').on('submit',function (event) {
            event.preventDefault();
            var token       = $('input[name=_token]').val();
            var formData    = {
                'from_user_id'     : $('input[name=from_user_id]').val(),
                'to_user_id'       : $('input[name=to_user_id]').val(),
                'pesan'            : $('textarea[name=isi_pesan]').val(),
                'iklan_id'            : $('input[name=iklan_id]').val(),
            };

            $.ajax({
                url     : '{{ url('/pesan/balas') }}',
                headers : {'X-CSRF-TOKEN': token},
                type    : 'POST',
                datatype: 'JSON',
                data    : formData,
                encode  : true
            })
                .done(function(data) {
                    if(data) {
                        var chat = $(".isi_pesan");
                        var content = "\
                        <div class='row'>\
                            <div class='col-md-8 col-md-offset-4'>\
                                <div class='head_pesan mr-top2' style='padding: 0 .5rem; font-size: 1.1rem'>\
                                    <span class='inline left' style='width: 100%; color: rgba(34, 34, 34, .5)'>"+data.pesan_terbuat+"</span>\
                                </div>\
                                <div class='content_pesan'>\
                                    <div class='well no-margin blue'>"+data.pesan_value+"</div>\
                                </div>\
                            </div>\
                        </div>";

                        chat.append(content);
                        chat.scrollTop(chat[0].scrollHeight);
                        $('textarea#isi_pesan').val(null);
                    }
                        
                })

                .fail(function(data) {
                    console.log(data);
                });
             
        });
    });
</script>