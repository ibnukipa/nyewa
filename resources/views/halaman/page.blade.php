@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9 col-md-offset-3" style="margin-left: 15%">
            <div class="panel panel-sewaaja" style="margin-top: 2rem; margin-bottom: 0">
                <div class="panel-heading red-darken" style="text-align:center;">{{$page->title}}</div>
                <div class="panel-body">
                    {!! $page->content !!}
                </div>

                
            </div>
        </div>
    </div>
</div>
@endsection


@section('footer')
    @include('partials.footer')
@endsection