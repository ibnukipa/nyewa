<div  class="list-group-item mr-top1" onclick="window.location='{{ url('/iklan/detail/'.$value->url_iklan.'.html') }}';" style="min-height: 120px; max-height: 120px; cursor: pointer">
    <div class="row">
        <div class="col-md-2 no-padding">
            <div class="image" style="height: 120px">
                <img style="display: block; margin: auto; height: 100%; width: auto" data-src="{{url('img/'.$value->id.'/'.$value->gambarIklan[0]->file_name.'/200')}}">
            </div>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10 no-padding-right">
                    <div class="content-iklan" style="padding: 0; padding-top: 1rem">
                        <h4 class="list-group-item-heading" style="min-height: 45px; max-height: 45px; font-size: 2rem; margin-bottom: 1rem">
                            {{ $value->name }}
                        </h4>
                    </div>
                </div>
                <div class="col-md-2 no-padding-left">
                        <?php $jenisPaket = $value->paket()->where('active','=',1)->first();
                            if($jenisPaket) {
                                $namePaket      = $jenisPaket->name;
                                $paketStart     = $jenisPaket->pivot->start_at;
                                $paketEnd       = $jenisPaket->pivot->end_at;
                                if($paketEnd >= date('Y-m-d H:i:s',strtotime(\Carbon\Carbon::now()->toDayDateTimeString())))
                                    $viewJenisPaket = HTML::image(url('asset/img/p_icon_'.$jenisPaket->value.'.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 45px; position: absolute; right: 3rem; top: 1rem'));
                                else
                                    $viewJenisPaket = HTML::image(url('asset/img/p_icon_0.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 45px; position: absolute; right: 3rem; top: 1rem'));
                            } else {
                                $viewJenisPaket = HTML::image(url('asset/img/p_icon_0.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 45px; position: absolute; right: 3rem; top: 1rem')); 
                            }
                        ?>
                        
                        <a href="#">
                            {{$viewJenisPaket}}
                        </a>                       
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 ">
                    <div class="content-iklan" style="padding: 0">
                        <div class="list-group-item-text" style="font-size: 100%; border-top: none; padding-top: 0; max-height: 2rem; min-height: 2rem;">
                            <div class="child" style="font-size: 1.3rem; color: rgba(34, 34, 34, .5)">
                                <?php 
                                    $kota = Indonesia::findCity($value->tempat_id, ['province']);
                                    $time = new Time; 
                                ?>
                                {!! $kota->name !!}
                            </div>
                            <div class="child pull-right" style="color: rgba(34, 34, 34, .3)">
                                {{ date(' d ', strtotime($value->updated_at)) }}
                                {{ $time->bulan[date('n', strtotime($value->updated_at))] }}
                                {{ date(' Y', strtotime($value->updated_at)) }}
                                -
                                {{ date('H:i:s', strtotime($value->updated_at)) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 ">
                    <div class="content-iklan" style="padding: 0">
                        <div class="list-group-item-text" style="border-top: none; padding-top: 0; max-height: 2rem; min-height: 2.5rem;">
                            <div class="child" style="font-size: 1.8rem">
                                    Rp. {!! number_format($value->lowest_price, 0, ",", ".") !!}
                                @if(count($value->waktuSewa) != 1)
                                    - Rp. {!! number_format($value->highest_price, 0, ",", ".") !!}
                                @endif
                            </div>
                            <div class="child pull-right" style="color: #faca51">
                                <input data-active-icon="glyphicon-star" data-inactive-icon="glyphicon-star kosong" value="{{ $value->rating }}" type="number" name="rating" id="rating" class="rating" data-readonly/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>