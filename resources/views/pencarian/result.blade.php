@extends('layouts.app')

@section('nav')
    @include('partials.nav')
@endsection

@section('template_title')
    Pencarian
@endsection

@section('content')
<div class="container">
    @include('partials.search')
    <div class="row mr-top1">
    
    @if(count($promotedIklan) > 0 || count($notPromotedIklan) > 0)
        <div class="col-md-10 col-md-offset-1">
            <div class="result-iklan">
                <div class="grey-light" style="padding: .5rem; padding-left: 1rem; color: rgba(33,33,33, .5); border-bottom: 1px solid rgba(33,33,33, .1); height: 4rem">
                    <div class="inline" style="line-height: 3rem; color: rgba(34, 34, 34, .5)">
                        <a style="font-size: 1.3rem; font-weight: 600; color: rgba(111, 20, 23, .7)" href="{{url('/')}}">Beranda</a> /
                        <?php $indexK = count($pathKategori); ?>
                        @while($indexK)    
                            <a 
                                style="font-size: 1.3rem; font-weight: 600; color: rgba(111, 20, 23, .7)" 
                                href="{{ url('/cari') }}?provinsi={{$selected_place_id}}&kategori={{$pathKategori[$indexK-1][0]}}&keyword={{$keyword}}">{{ $pathKategori[--$indexK][1] }}</a> / 
                        @endwhile
                        <a style="font-size: 1.3rem; font-weight: 600; color: rgba(34, 34, 34, .5)" >Hasil pencarian</a>
                    </div>
                    <div class="inline pull-right">
                        {!! Form::select('sortResult', array('terbaru' => 'Terbaru', 'termurah' => 'Termurah', 'termahal' => 'Termahal'), $currentSort, array('style' => 'padding-left: 2px; border-radius: 0; border: 1px solid rgba(34, 34, 34, .3); height: 3rem; min-width: 15rem', 'class' =>'form-control search-panel', 'id' => 'sortResult')) !!}
                        <script>
                            $('#sortResult').on('change', function() {
                                var token = $('input[name=_token]').val();
                                var provinsi = '{{$selected_place_id}}';
                                var kategori = '{{$selected_kategori_id}}';
                                var keyword  = '{{$keyword}}';
                                var sortValue = $(this).val();
                                $.ajax({
                                    url: "{{ url('/cari') }}",
                                    type: "GET",
                                    data: { kategori: kategori, provinsi: provinsi, keyword:keyword, sortValue: sortValue },
                                    headers: {'X-CSRF-TOKEN': token},
                                    datatype: "JSON",
                                    success: function(data) {
                                        $("#result_listiklan").html(data);
                                        $(".loading").removeClass("on");
                                        init();
                                    },
                                    beforeSend: function() {
                                        $(".loading").addClass("on");
                                    },
                                    complete: function() {

                                    }
                                });
                            });
                        </script>
                    </div>
                    <div class="inline pull-right" style="margin-right: 1rem">
                        <div class="btn-group" role="group" aria-label="true">
                            <button 
                                    data-toggle="tooltip" 
                                    data-placement="bottom" 
                                    title="List" 
                                    type="button" class="btn btn-default" style="padding: 2px; width: 3rem; height: 3rem; border-radius: 0" 
                                    {{-- disabled --}}
                                    >
                                <i class="material-icons" style="">view_list</i>
                            </button>
                        </div>
                    </div>
                    <div class="inline pull-right" style="margin-right: .5rem">
                        <div class="btn-group" role="group" aria-label="true">
                            <button 
                                    data-toggle="tooltip" 
                                    data-placement="bottom" 
                                    title="Modular" 
                                    type="button" class="btn btn-default" style="padding: 2px; width: 3rem; height: 3rem; border-radius: 0" 
                                    {{-- disabled --}}
                                    >
                                <i class="material-icons" style="">view_module</i>
                            </button>
                        </div>
                    </div>
                    <!--<div class="inline pull-right" style="margin-right: 1rem">
                        {!! Form::number('harga_ke', null, [
                                    'class' => 'form-control search-panel', 
                                    'style' => 'border-radius: 0; 
                                                border: 1px solid rgba(34, 34, 34, .3); 
                                                height: 3rem; 
                                                width: 10rem',
                                    'placeholder' => 'Harga Ke'
                                    ]); !!}
                    </div>
                    <div class="inline pull-right" style="margin-right: 1rem">
                        <span style="line-height: 3rem"> - </span>
                    </div>
                    <div class="inline pull-right" style="margin-right: 1rem">
                        {!! Form::number('harga_dari', null, [
                                    'class' => 'form-control search-panel', 
                                    'style' => 'border-radius: 0; 
                                                border: 1px solid rgba(34, 34, 34, .3); 
                                                height: 3rem; 
                                                width: 10rem',
                                    'placeholder' => 'Harga dari'
                                    ]); !!}
                    </div>-->
                </div>
                <div id="result_listiklan">
                    @include('pencarian.result-iklan')
                </div>
                <div class='loading'></div>
                
            </div>
        </div>
    @else
        <div class="col-md-12">
            <div class="blank-component">
                <i class="material-icons">search</i>
                <p>Tidak ada hasil untuk Iklan yang Anda cari.</p>
            </div>
        </div>
    @endif

    </div>
</div>
@endsection

@section('footer')
    @include('partials.footer')
@endsection
