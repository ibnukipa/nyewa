
{{-- @if($listIklan->contains(function ($key, $value) {
    return count($value->paket()->where('active','=', 1)->get()) > 0;
})) --}}

@if(count($promotedIklan) > 0)
<div class="iklan-promosi" style="margin-top: 1rem; padding: 1rem 1rem 1rem 1rem; background-color: rgba(219,168,40, .1)">
    <h5 style="margin-top: 0; color: rgb(219,168,40); font-weight: 600; font-size: 1.3rem" class="right">Promosi NyewAja.com</h5>
    @foreach($promotedIklan as $value)
        <?php $curPaket = $value->paket()->where('active','=',1)->where('end_at', '>=', date('Y-m-d H:i:s',strtotime(\Carbon\Carbon::now()->toDayDateTimeString())))->first(); ?>
        @if($curPaket)
            @include('pencarian.result-item')
        @endif
    @endforeach
</div>
@endif
{{-- 
@if($listIklan->contains(function ($key, $value) {
    return count($value->paket()) > 0;
})) --}}
@if(count($notPromotedIklan) > 0)
<div class="iklan-ordinary" style="margin-top: 1rem; padding: 1rem 1rem 1rem 1rem;">
    <h5 style="margin-top: 0; color: rgba(33,33,33, .3); font-weight: 600; font-size: 1.3rem" class="right">Iklan Lainnya</h5>
    @foreach($notPromotedIklan as $value)
        <?php $curPaket = $value->paket()->where('active','=',1)->where('end_at', '>=', date('Y-m-d H:i:s',strtotime(\Carbon\Carbon::now()->toDayDateTimeString())))->first(); ?>
        @if(!$curPaket)
            @include('pencarian.result-item')
        @endif
    @endforeach
</div>
@endif
<nav class="center">
    <ul class="pagination">
        @if($page['current'] == 1)
            <li class="disabled">
                <a aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
        @else
            <li>
                <a href="{{ url('/cari') }}?provinsi={{$selected_place_id}}&kategori={{$selected_kategori_id}}&keyword={{$keyword}}&currentPage={{$page['current']-1}}&sortValue={{$currentSort}}" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
        @endif

        @for($i = 0; $i<$page['max']; $i++)
            @if($page['current']-1 == $i)
                <li class="active"><a>{{ $i+1 }}</a></li>
            @else
                <li><a href="{{ url('/cari') }}?provinsi={{$selected_place_id}}&kategori={{$selected_kategori_id}}&keyword={{$keyword}}&currentPage={{$i+1}}&sortValue={{$currentSort}}">{{ $i+1 }}</a></li> 
            @endif    
        @endfor

        @if($page['current'] == $page['max'])
            <li class="disabled">
                <a aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        @else
            <li>
                <a href="{{ url('/cari') }}?provinsi={{$selected_place_id}}&kategori={{$selected_kategori_id}}&keyword={{$keyword}}&currentPage={{$page['current']+1}}&sortValue={{$currentSort}}" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        @endif
    </ul>
</nav>