<?php $listIklan = Auth::user()->iklan()->where('status', '=', 'approved')->paginate(5) ?>
@foreach($listIklan as $key => $value)
<div class="list-group-item">
    <div class="row">
        <div class="col-md-2">
            <div class="image">
                    <img display="block" margin="auto" data-src="{{url('img/'.$value->id.'/'.$value->gambarIklan[0]->file_name.'/200')}}">
                <span class="badge blue">Tayang</span>
            </div>

        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10 no-padding-right">
                    <div class="content-iklan" style="padding: 0; padding-top: 1rem">
                        <h4 class="list-group-item-heading">
                            {{ $value->name }}
                        </h4>
                    </div>
                </div>
                <div class="col-md-2 no-padding-left">
                        <?php $jenisPaket = $value->paket()->where('active','=',1)->first();
                              if($jenisPaket) {
                                  $namePaket      = $jenisPaket->name;
                                  $paketStart     = $jenisPaket->pivot->start_at;
                                  $paketEnd       = $jenisPaket->pivot->end_at;
                                  if($paketEnd < date('Y-m-d H:i:s',strtotime(\Carbon\Carbon::now()->toDayDateTimeString()))) {
                                      $viewJenisPaket = HTML::image(url('asset/img/p_icon_0.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 80px; position: absolute; right: 4rem; top: 1rem'));
                                  }
                                  else {
                                      $viewJenisPaket = HTML::image(url('asset/img/p_icon_'.$jenisPaket->value.'.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 80px; position: absolute; right: 4rem; top: 1rem'));
                                  }
                              } else {
                                  $viewJenisPaket = HTML::image(url('asset/img/p_icon_0.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 80px; position: absolute; right: 4rem; top: 1rem')); 
                              }
                        ?>
                        
                        <a href="#">
                            {{$viewJenisPaket}}
                        </a>                       
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 ">
                    <div class="content-iklan" style="padding: 0">
                        <div class="list-group-item-text">
                            <div class="child" style="font-size: 2rem">
                                Rp. {!! number_format($value->waktuSewa[0]->harga, 0, ",", ".") !!}
                                <span class="c-grey">({!! $value->waktuSewa[0]->waktu !!})</span>
                            </div>
                            <div class="child hidden-item pull-right animated fadeIn">
                                <a href="{{ url('/iklan/detail/'.$value->url_iklan.'.html') }}" class="no-padding">
                                    {!! Form::button('<i class="material-icons middle">remove_red_eye</i><span class="middle"> Lihat</span>', array(
                                                'class'             => 'btn btn-small transparent left',
                                                'style'             => 'padding: 4px 12px',
                                                )) !!}
                                </a>
                                <a href="{{ url('/iklan/edit/'.$value->id) }}" class="no-padding">
                                    {!! Form::button('<i class="material-icons middle">edit</i><span class="middle"> Edit</span>', array(
                                                'class'             => 'btn-dark btn-small grey-dark left',
                                                'style'             => 'padding: 4px 12px',
                                                )) !!}
                                </a>
                                <a class="no-padding">
                                    {!! Form::open(array('route' => 'iklan.change', 'class' => 'form-button inline', 'method' => 'POST')) !!}
                                        {!! Form::hidden('iklan_id', $value->id) !!}
                                        {!! Form::hidden('tobe_status', 'nonaktif') !!}
                                        {!! Form::button(   '<i class="material-icons middle">block</i> <span class="middle"> Non-Aktifkan</span>', 
                                                            array(  'class'             => 'btn-dark btn-small grey-dark left',
                                                                    'style'             => 'padding: 4px 12px',
                                                                    'type'              => 'button', 
                                                                    'data-toggle'       => 'modal',
                                                                    'data-backdrop'     => 'static',
                                                                    'data-keyboard'     => 'false',
                                                                    //ini untuk detail modal
                                                                    'data-target'       => '#confirmChange', 
                                                                    'data-body-title'   => 'Apakah Anda yakin untuk <strong style="color: rgb(238,43,49);">menon-aktifkan</strong> Iklan ini?', 
                                                                    'data-body-content' => $value->name
                                                                )
                                                        ) 
                                        !!}
                                    {!! Form::close() !!}
                                </a>
                                @if($value->pesan->count() > 0)
                                <a href="{{ url('/pesan/masuk/'.$value->id) }}" class="no-padding">
                                    {!! Form::button('<i class="material-icons middle">email</i><span> Pesan <span class="badge red" style="font-size: 1.2rem">'.$value->pesan->count().'</span></span>', array(
                                                'class'             => 'btn-dark btn-small blue left',
                                                'style'             => 'padding: 4px 12px',
                                                )) !!}
                                </a>
                                @else
                                <a class="no-padding">
                                    {!! Form::button('<i class="material-icons middle">email</i><span> Pesan <span class="badge grey-light" style="font-size: 1.2rem; color: rgba(33, 33, 33, .1)">'.$value->pesan->count().'</span></span>', array(
                                                'class'             => 'btn btn-small grey-light left',
                                                'style'             => 'padding: 4px 12px; color: rgba(33, 33, 33, .1)',
                                                )) !!}
                                </a>
                                @endif
                                @if($jenisPaket) 
                                <a class="no-padding">
                                    {!! Form::button(   '<i class="material-icons middle">grade</i> <span class="middle"> Promosikan</span>', 
                                                        array(  'class'             => 'btn-dark btn-small green left',
                                                                'style'             => 'padding: 4px 12px;',
                                                                'type'              => 'button', 
                                                                'data-toggle'       => 'modal',
                                                                'data-backdrop'     => 'static',
                                                                'data-keyboard'     => 'false',
                                                                //ini untuk detail modal
                                                                'data-target'       => '#modalPromote', 
                                                                //ini data iklan 
                                                                'data-iklan-name'   => $value->name,
                                                                'data-iklan-id'     => $value->id,
                                                                'data-iklan-paket'  => $namePaket.
                                                                                        '<br><span style="font-size: 1.3rem;"> <span style="font-weight: 600">Durasi</span> : '.
                                                                                        date('Y/m/d', strtotime($paketStart)).
                                                                                        ' - '. date('Y/m/d', strtotime($paketEnd)).
                                                                                        '</span>'
                                                            )
                                                    ) 
                                    !!}
                                </a>
                                @else
                                <a class="no-padding">
                                    {!! Form::button(   '<i class="material-icons middle">grade</i> <span class="middle"> Promosikan</span>', 
                                                        array(  'class'             => 'btn-dark btn-small green left',
                                                                'style'             => 'padding: 4px 12px',
                                                                'type'              => 'button', 
                                                                'data-toggle'       => 'modal',
                                                                'data-backdrop'     => 'static',
                                                                'data-keyboard'     => 'false',
                                                                //ini untuk detail modal
                                                                'data-target'       => '#modalPromote', 
                                                                //ini data iklan 
                                                                'data-iklan-name'   => $value->name,
                                                                'data-iklan-id'     => $value->id,
                                                            )
                                                    ) 
                                    !!}
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
{{ $listIklan->links() }}