@extends('layouts.app')

@section('style_container')
    style="width: 72%"
@endsection

@section('template_title')
    {{ $iklan->name }} - {{ $iklan->kategori->name }}
@endsection
<?php $time = new Time; ?>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-sewaaja iklan">
                <div class="panel-heading">
                    {{$iklan->name}}
                    <br>
                    <div class="inline" style="width: 70%">
                        
                        <div class="child">
                            <i class="material-icons middle">place</i>
                            <?php $kota = Indonesia::findCity($iklan->tempat_id, ['province']) ?>
                            <span class="middle">{{ $kota->name }}, {{ $kota->province->name }}</span>
                        </div>
                        <div class="child">
                            <i class="material-icons middle">visibility</i>
                            <span class="middle">{{ $iklan->seen }} kali</span>
                        </div>
                        <div class="child">
                            <input data-active-icon="glyphicon-star" data-inactive-icon="glyphicon-star kosong" value="{{ $iklan->rating }}" type="number" name="rating" id="rating" class="rating" data-readonly/>
                        </div>
                    </div>
                    <div class="inline pull-right" style="width: 30%; text-align: right">
                        <div class="child">
                           <i class="material-icons middle">update</i>
                           
                            <span class="middle">Terakhir update: 
                                {{ date(' d ', strtotime($iklan->updated_at)) }}
                                {{ $time->bulan[date('n', strtotime($iklan->updated_at))] }}
                                {{ date(' Y', strtotime($iklan->updated_at)) }}
                                <!---
                                {{ date('H:i:s', strtotime($iklan->updated_at)) }}-->
                            </span>
                            <!--<?php $jenisPaket = $iklan->paket()->where('active','=',1)->first();
                                if($jenisPaket) {
                                    $namePaket      = $jenisPaket->name;
                                    $paketStart     = $jenisPaket->pivot->start_at;
                                    $paketEnd       = $jenisPaket->pivot->end_at;
                                    if($paketEnd >= date('Y-m-d H:i:s',strtotime(\Carbon\Carbon::now()->toDayDateTimeString())))
                                        $viewJenisPaket = HTML::image(url('asset/img/p_icon_'.$jenisPaket->value.'.png/300'), $alt="Photo", $attributes = array('display'=>'block', 'margin'=>'auto')).'<span class="middle"> </span> ';
                                    else
                                        $viewJenisPaket = HTML::image(url('asset/img/p_icon_0.png/300'), $alt="Photo", $attributes = array('display'=>'block', 'margin'=>'auto')).'<span class="middle"> </span> '; 
                                } else {
                                    $viewJenisPaket = HTML::image(url('asset/img/p_icon_0.png/300'), $alt="Photo", $attributes = array('display'=>'block', 'margin'=>'auto')).'<span class="middle"> </span> ';  
                                }
                            ?>
                            {!!$viewJenisPaket!!}-->
                        </div>
                        
                    </div>     
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="flexslider">
                                <ul class="slides">
                                    @for($i=0; $i <= 5; $i++)
                                        @if($i < count($iklan->gambarIklan))
                                            <li data-thumb="{{ url('img/'.$iklan->id.'/'.$iklan->gambarIklan[$i]->file_name.'/100') }}">
                                                <div class="image">
                                                    <img display="block" margin="auto" data-src="{{url('img/'.$iklan->id.'/'.$iklan->gambarIklan[$i]->file_name.'/300')}}">
                                                </div>
                                            </li>
                                        @else
                                            <li data-thumb="{{ url('img/'.$iklan->id.'/nofound/100') }}">
                                                <img display="block" margin="auto" data-src="{{url('img/'.$iklan->id.'/nofound/300')}}">
                                            </li>
                                        @endif
                                    @endfor
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="tags">
                                        <span class="price-tag">
                                            <a>
                                                <div>
                                                    Rp. {!! number_format($iklan->lowest_price, 0, ",", ".") !!}
                                                    <!--<span style="font-size: 2rem; font-weight: 500; text-transform: lowercase">
                                                        <span style="font-size: 2.5rem">/</span> 
                                                        {!! $iklan->waktuSewa[0]->waktu !!}
                                                    </span>-->
                                                </div>
                                            </a>
                                        </span>
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default detail">
                                        <!-- Default panel contents -->
                                        <div class="panel-heading blue">
                                            Detail Harga
                                            <button class="btn btn-small transparent pull-right" type="button" data-toggle="collapse" data-target="#detailHarga" aria-expanded="true" aria-controls="collapseExample">
                                                <i class="material-icons middle">keyboard_arrow_down</i>
                                            </button>
                                        </div>
                                        
                                        <div class="panel-content collapse" id="detailHarga">
                                            <table class="table no-margin">
                                                <thead>
                                                    <tr>
                                                        <th>Harga Sewa</th>
                                                        <th>Lama Sewa</th> 
                                                    </tr> 
                                                </thead> 
                                                <tbody>
                                                    @foreach($iklan->waktuSewa as $iterator => $waktuSewa)
                                                    <tr>
                                                        <td class="main-value">Rp. {!! number_format($waktuSewa->harga, 0, ",", ".") !!}</td> 
                                                        <td style="text-transform: lowercase">{!! $waktuSewa->waktu !!}</td> 
                                                    </tr> 
                                                    @endforeach
                                                </tbody> 
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default label-box">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2 c-grey">
                                                    <i class="material-icons middle">account_circle</i>
                                                </div>
                                                <div class="col-md-10" style="padding-left: 0">
                                                    <span class="c-blue" style="font-size: 1.6rem; font-weight: 600">
                                                        {{ $iklan->user->name }}
                                                    </span>
                                                    <br>
                                                    <span class="c-grey" style="font-size: 1.1rem">
                                                        Member Sejak {{ date("M Y", strtotime($iklan->user->created_at)) }}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default label-box">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-2 c-grey">
                                                    <i class="material-icons middle">phone_in_talk</i>
                                                </div>
                                                <div class="col-md-10" style="padding-left: 0">
                                                    <span id="phone_user" class="c-blue" style="font-size: 1.6rem; font-weight: 500">
                                                        +6xxxxxxxxxxx
                                                    </span>
                                                    <br>
                                                    {!! Form::button('<span class="middle">show</span>', array('onclick' => 'changeNOHP(this)' , 'class' => 'btn btn-small transparent')) !!}
                                                </div>
                                            </div>
                                            <script>
                                                function changeNOHP(caller) {
                                                    $(caller).remove();
                                                    @if($iklan->user->nohp != '')
                                                        $('#phone_user').text('{!! str_replace('08', '+628', $iklan->user->nohp) !!}');
                                                    @else
                                                        $('#phone_user').text('Tidak ada No HP');
                                                    @endif
                                                }
                                            </script>

                                            <div class="row mr-top2">
                                                <div class="col-md-2 c-grey">
                                                    <i class="material-icons middle">fiber_pin</i>
                                                </div>
                                                <div class="col-md-10" style="padding-left: 0">
                                                    <span id="bbm_user" class="c-blue" style="font-size: 1.6rem; font-weight: 500">
                                                        xxxxxxxx
                                                    </span>
                                                    <br>
                                                    {!! Form::button('<span class="middle">show</span>', array('onclick' => 'changePIN(this)' , 'class' => 'btn btn-small transparent')) !!}
                                                </div>
                                            </div>
                                            <script>
                                                function changePIN(caller) {
                                                    $(caller).remove();
                                                    @if($iklan->user->pinbb != '')
                                                        $('#bbm_user').text('{!! $iklan->user->pinbb !!}');
                                                    @else
                                                        $('#bbm_user').text('Tidak ada PIN BB');
                                                    @endif
                                                }
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    
                                    @if(!Auth::guest() && $iklan->user_id == Auth::user()->id)
                                    <!--{!! Form::button(   '<i class="material-icons middle">receipt</i> <span class="middle"> Iklan Anda</span>', 
                                                        array(  'class'                 => 'btn-dark grey-dark',
                                                                'type'                  => 'button', 
                                                            )
                                                    ) 
                                    !!}-->
                                    @else
                                    {!! Form::button(   '<i class="material-icons middle">email</i> <span class="middle"> Kirim Pesan</span>', 
                                                        array(  'class'                 => 'btn-dark blue',
                                                                'type'                  => 'button', 
                                                                'data-toggle'           => 'modal',
                                                                'data-backdrop'         => 'static',
                                                                'data-keyboard'         => 'false',
                                                                'data-target'           => '#sendPesan',
                                                                'data-iklan-id'         => $iklan->id,  
                                                                'data-iklan-name'       => $iklan->name,
                                                                'data-iklan-pelapak'    => $iklan->user->name,
                                                                'data-iklan-to_user_id' => $iklan->user->id,
                                                            )
                                                    ) 
                                    !!}
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="detail-tab">
                                {{-- Nav Tabs --}}
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a data-target="#detailIklan" aria-controls="detailIklan" role="tab" data-toggle="tab">Detail Iklan</a>
                                    </li>
                                    <li role="presentation">                                        
                                        <a data-target="#feedbackIklan" aria-controls="feedbackIklan" role="tab" data-toggle="tab">Feedback ({{count($iklan->feedbacks)}})</a>
                                    </li>
                                </ul>

                                {{-- Content Tabs --}}
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="detailIklan">
                                        <div class="row">
                                            <ul class="modul-fasilitas" id="modul-spek">
                                                <h3>Spesifikasi</h3>
                                                <?php $spek = 0; ?>
                                                @foreach($iklan->details as $iterator => $detail)
                                                    @if($detail->name != 'fasilitas' && $detail->name != 'alamat lokasi')
                                                        <?php $spek++; ?>
                                                        <li>
                                                            <span class="name">
                                                                {{$detail->name}}
                                                            </span>
                                                            <span class="value">
                                                                : {{$detail->value}}
                                                            </span>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>

                                            <ul class="modul-fasilitas" id="modul-loc">
                                                <h3>Lokasi</h3>
                                                <?php $loc = 0; ?>
                                                @foreach($iklan->details as $iterator => $detail)
                                                    @if($detail->name == 'alamat lokasi')
                                                        <?php $loc++; ?>
                                                        <li style="width: 100%">
                                                            <span class="name" style="width: 12%">
                                                                {{$detail->name}}
                                                            </span>
                                                            <span class="value">
                                                                : {{$detail->value}}
                                                            </span>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>

                                            <ul class="modul-fasilitas" id="modul-facil">
                                                <h3>Fasilitas</h3>
                                                <?php $facil = 0; ?>
                                                @foreach($iklan->details as $iterator => $detail)
                                                    @if($detail->name == 'fasilitas')
                                                        <?php $facil++; ?>
                                                        <li style="width: 20%">
                                                            <i class="material-icons middle" style="color: #4CAF50">check_circle</i>
                                                            <span class="middle">{{ $detail->value }}</span> 
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>

                                            <script>
                                                @if($spek == 0)
                                                    $("#modul-spek").remove();
                                                @endif
                                                @if($loc == 0)
                                                    $("#modul-loc").remove();
                                                @endif
                                                @if($facil == 0)
                                                    $("#modul-facil").remove();
                                                @endif
                                            </script>

                                            <ul class="modul-fasilitas">
                                                <h3>Deskripsi</h3>
                                                <p style="padding: 5px; font-size: 1.5rem">
                                                    {!! $iklan->description !!}
                                                </p>
                                            </ul>

                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="feedbackIklan">
                                        @include('feedback.show')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="form-group">
                        <div class="col-md-6">
                            {{-- <p class="no-margin">
                                <a href="{{ url('/') }}">
                                Kembali ke SewaAja.com
                                </a>
                            </p> --}}
                        </div>
                        <div class="col-md-6 right">
                            {{-- <p class="no-margin">
                                Sudah punya akun? 
                                <a href="{{url('login')}}">
                                Masuk
                                </a>
                            </p> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('modals.modal-pesan-iklan')
@endsection

@section('footer')
    @include('scripts.modal-pesan-iklan')
    @include('partials.footer')
@endsection

@section('footer_script')
<script>
    $('.collapse').on('show.bs.collapse', function (e) {
        $(document).find("[data-target='#" + $(e.target).attr('id') + "']").children().text('keyboard_arrow_up');
    });

    $('.collapse').on('hide.bs.collapse', function (e) {
        $(document).find("[data-target='#" + $(e.target).attr('id') + "']").children().text('keyboard_arrow_down');
    });

    $('.detail-tab .nav-tabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });

    $(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails"
    });
    });
</script>
@endsection