@extends('layouts.app')

@section('style_container')
    style="width: 58%"
@endsection

@section('template_title')
    Edit Iklan 
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-sewaaja">
                <div class="panel-heading red-darken">Edit Iklan</div>
                <div class="panel-body">
                    <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ url('/iklan/update') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12 no-margin" >
                                <div class="divider mr-top1">
                                    <p>Info Umum Iklan</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="form-group has-feedback {{ $errors->has('name_iklan') ? ' has-error' : '' }}">
                                    <label for="name_iklan">Nama iklan <span>*</span></label>

                                    <span class="form-control-feedback-icon" aria-hidden="false">
                                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">receipt</i>
                                    </span>

                                    <input  data-trigger="focus" 
                                            data-toggle="popover"
                                            data-content="Masukkan nama yang menarik untuk menarik perhatian pelanggan" 
                                            id="name_iklan" class="form-control effect" 
                                            placeholder="Nama iklan" type="text" name="name_iklan" value="{{ $iklan->name }}"
                                            required
                                            >

                                    <span class="help-block effect">
                                        @if ($errors->has('name_iklan'))
                                            {{ $errors->first('name_iklan') }}
                                        @else
                                            Maksimal 75 karakter
                                        @endif
                                    </span>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5 col-md-offset-1 mr-top2">
                                <div class="form-group has-feedback {{ $errors->has('kategori') ? ' has-error' : '' }}">
                                    <label for="kategori">Kategori <span>*</span></label>
                                    <span class="form-control-feedback-icon" aria-hidden="false">
                                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">widgets</i>
                                    </span>

                                    {!! Form::select('kategori[]', array(null => null) , null, array( 'class' =>'js-example-basic-single js-states form-control', 'id' => 'kategori', 'style' => '', 'required' => 'required')) !!}
                                    <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
                                </div>
                            </div>

                            <div class="col-md-1 center mr-top2" style="padding-top: 3rem">
                                <i class="material-icons forward">forward</i>
                            </div>

                            <div class="col-md-4 mr-top2">
                                <div class="form-group has-feedback {{ $errors->has('subkategori') ? ' has-error' : '' }}">
                                    <label for="subkategori">Sub-Kategori <span>*</span></label>
                                    <span class="form-control-feedback-icon" aria-hidden="false">
                                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">widgets</i>
                                    </span>

                                    {!! Form::select('kategori[]', array(null => null) , null, array( 'class' =>'js-example-basic-single js-states form-control', 'id' => 'subkategori', 'style' => '', 'disabled' => 'disabled')) !!}
                                    <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5 col-md-offset-1">
                                <div class="form-group has-feedback {{ $errors->has('kategori') ? ' has-error' : '' }}">
                                    <span class="help-block effect">
                                        @if ($errors->has('kategori'))
                                            {{ $errors->first('kategori') }}
                                        @else
                                            Pilih kategori yang sesuai dengan Iklan Anda
                                        @endif
                                    </span>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10 col-md-offset-1  mr-top2">
                                <div class="form-group has-feedback {{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="description">Deskripsi iklan <span>*</span></label>

                                    <span class="form-control-feedback-icon" aria-hidden="false">
                                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">description</i>
                                    </span>

                                    <textarea  data-trigger="focus" 
                                            data-toggle="popover"
                                            data-content="Tuliskan deskripsi yang jelas, singkat dan padat untuk menarik perhatian pelanggan" 
                                            id="description" class="form-control effect" 
                                            placeholder="Deskripsi iklan"  
                                            name="description"
                                            style="max-width: 100%; min-height: 10rem"
                                            required
                                            >{!! str_replace('<br>', '&#10;',$iklan->description) !!}</textarea>

                                    <span class="help-block effect">
                                        @if ($errors->has('description'))
                                            {{ $errors->first('description') }}
                                        @else
                                            Maksimal 4085 karakter
                                        @endif
                                    </span>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 no-margin">
                                <div class="divider mr-top3">
                                    <p>Detail Iklan</p>
                                    <p style="color: rgba(0, 0, 0, .5); font-size: 1.2rem">
                                        Detail Iklan akan menyesuaikan dengan kategori iklan. Pastikan anda telah mengisi kategori iklan.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div id="detailFormKategori">    
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12 no-margin" >
                                <div class="divider mr-top3">
                                    <p>Detail Harga Iklan</p>
                                    <p style="color: rgba(0, 0, 0, .5); font-size: 1.2rem">
                                        Informasikan waktu beserta harga sewa secara rinci.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 col-md-offset-1">
                                <div class="form-group has-feedback {{ $errors->has('waktu[]') ? ' has-error' : '' }}">
                                    <label for="waktu[]">Waktu Sewa <span>*</span></label>

                                    <span class="form-control-feedback-icon" aria-hidden="false">
                                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">date_range</i>
                                    </span>

                                    <input  data-trigger="focus" 
                                            data-toggle="popover"
                                            data-content="Masukkan waktu sewa sesuai dengan harga / tarif sewa Anda" 
                                            id="waktu[]" class="form-control effect" 
                                            placeholder="Waktu sewa" type="text" name="waktu[]" value="{{ $iklan->waktuSewa[0]->waktu }}"
                                            required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>

                            <div class="col-md-1 center" style="padding-top: 3rem">
                                <i class="material-icons forward">forward</i>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group has-feedback {{ $errors->has('harga[]') ? ' has-error' : '' }}">
                                    <label for="harga[]">Harga / Tarif Sewa<span>*</span></label>

                                    <span class="form-control-feedback-icon" aria-hidden="false" style="font-size: 1.8rem; line-height: 35px">
                                        Rp.
                                    </span>

                                    <input  data-trigger="focus" 
                                            data-toggle="popover"
                                            data-content="Masukkan harga / tarif sewa sesuai dengan waktu sewa Anda" 
                                            id="harga[]" class="form-control effect" 
                                            placeholder="Harga sewa" type="number" name="harga[]" value="{{ $iklan->waktuSewa[0]->harga }}"
                                            required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row btn_addharga_group">
                            {{-- Catatan Kaki Waktu / Harga Sewa --}}
                            <div class="col-md-4 col-md-offset-1">
                                <div class="form-group has-feedback {{ $errors->has('waktu') ? ' has-error' : '' }}">
                                    <span class="help-block effect">
                                        @if ($errors->has('waktu'))
                                            {{ $errors->first('waktu') }}
                                        @else
                                            contoh: 12 Jam, 1 Hari, 1 Minggu, dll
                                        @endif
                                    </span>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="col-md-4 col-md-offset-1">
                                <div class="form-group has-feedback {{ $errors->has('harga') ? ' has-error' : '' }}">
                                    <span class="help-block effect">
                                        @if ($errors->has('harga'))
                                            {{ $errors->first('harga') }}
                                        @else
                                            Tidak perlu tanda titik(.) atau koma(,)
                                        @endif
                                    </span>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>
                            {{-- Catatan Kaki Waktu / Harga Sewa --}}

                            <div class="col-md-10 col-md-offset-1 left no-padding" style="margin-top: 2.2rem">
                                {!! Form::button('<i class="material-icons middle">add</i> <span class="middle">Tambah Waktu dan Harga Sewa</span>', array('class' => 'btn-cut btn-full green-light','onclick' => 'addHarga(this)', 'style' => 'padding: 4px 12px')) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 no-margin mr-top2">
                                <div class="divider mr-top3">
                                    <p>Upload Gambar</p>
                                    <p style="color: rgba(0, 0, 0, .5); font-size: 1.2rem">
                                        Tambahkan foto-foto iklan Anda untuk mendapatkan respon penyewa lebih banyak.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 no-padding">
                                {!! Form::file('files[]', array('multiple' => 'multiple', 'id' => 'filer_input', 'accept' => 'image/png, image/jpg, image/x-png, image/gif, image/jpeg')) !!}  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 no-margin mr-top1">
                                <div class="divider mr-top3">
                                    <p>Lokasi Iklan Anda</p>
                                    <p style="color: rgba(0, 0, 0, .5); font-size: 1.2rem">
                                        Secara otomatis lokasi iklan Anda susuai dengan informasi kontak Akun NyewAja Anda. 
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-5 col-md-offset-1">
                                <div class="form-group has-feedback {{ $errors->has('provinsi') ? ' has-error' : '' }}">
                                    <label for="provinsi">Provinsi <span>*</span></label>
                                    <span class="form-control-feedback-icon" aria-hidden="false">
                                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">place</i>
                                    </span>

                                    {!! Form::select('provinsi', array(null => null) , null, array( 'class' =>'js-example-basic-single js-states form-control', 'id' => 'provinsi', 'style' => '', 'required' => 'required')) !!}
                                    <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-md-offset-1 mr-top2">
                                <div class="form-group has-feedback {{ $errors->has('kota') ? ' has-error' : '' }}">
                                    <label for="kota">Kota <span>*</span></label>
                                    <span class="form-control-feedback-icon" aria-hidden="false">
                                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">place</i>
                                    </span>

                                    {!! Form::select('kota', array(null => null) , null, array( 'class' =>'js-example-basic-single js-states form-control', 'id' => 'kota', 'style' => '', 'required' => 'required')) !!}
                                    <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 no-margin mr-top1">
                                <div class="divider mr-top3">
                                    <p>Info Kontak Anda</p>
                                    <p style="color: rgba(0, 0, 0, .5); font-size: 1.2rem">
                                        Secara otomatis informasi kontak Anda pada iklan akan sesuai dengan informasi kontak Akun NyewAja Anda. 
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5 no-padding col-md-offset-1">
                                @include('builder.form', 
                                [
                                    'tipe_input'        => 'input',
                                    'input_type'        => 'text',
                                    'icon_input'        => 'person',
                                    'value_input'       => Auth::user()->name,
                                    'required'          => true,
                                    'col_width'         => 12,
                                    'id_input'          => 'name',
                                    'name_input'        => 'Nama Lengkap',
                                    'popover_content'   => 'Nama Lengkap Anda',
                                ])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5 no-padding col-md-offset-1 mr-top2">
                                @include('builder.form', 
                                [
                                    'tipe_input'        => 'input',
                                    'input_type'        => 'text',
                                    'icon_input'        => 'call',
                                    'value_input'       => Auth::user()->nohp,
                                    'col_width'         => 12,
                                    'id_input'          => 'nohp',
                                    'name_input'        => 'Nomer Telepon / Nomer HP',
                                    'popover_content'   => 'Masukkan Nomer HP Anda untuk mempermudah penyewa menghubungi Anda',
                                    'popover_position'  => 'bottom',
                                    'required'          => true,
                                    //'foot_note'         => $foot_note,
                                ])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 no-padding col-md-offset-1 mr-top2">
                                @include('builder.form', 
                                [
                                    'tipe_input'        => 'input',
                                    'input_type'        => 'text',
                                    'icon_input'        => 'chat_bubble',
                                    'value_input'       => Auth::user()->pinbb,
                                    'col_width'         => 12,
                                    'id_input'          => 'pinbb',
                                    'name_input'        => 'PIN BB',
                                    'popover_content'   => 'Masukkan PIN BB untuk mempermudah penyewa menghubungi Anda',
                                    'popover_position'  => 'bottom',
                                    //'foot_note'         => $foot_note,
                                ])
                            </div>
                        </div>
                        @if($iklan->paket()->where('active','=',0)->where('start_at', '=', null)->first() || count($iklan->paket()->where('active','=',1)->get()) == 0)
                        <div class="row">
                            <div class="col-md-12 no-margin">
                                <div class="divider mr-top3">
                                    <p>Pilih Promosi Iklan</p>
                                    <p style="color: rgba(0, 0, 0, .5); font-size: 1.2rem">
                                        Silahkan pilih salah satu paket yang ingin Anda gunakan untuk mempromosikan iklan Anda :
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-sewaaja">
                                    <div class="panel-heading" style="padding: 2rem 2rem; line-height: 3rem; min-height: 90px; max-height: 90px;">
                                        <span class="left" style="font-weight: 600; font-size: 3rem; font-family: 'Roboto'">
                                            <p style="font-size: 1.2rem; font-weight: 400; line-height: initial; margin: 0">Saldo Anda</p>
                                            Rp. {!! number_format((Auth::user()->saldo), 0, ",", ".") !!}
                                        </span>
                                        <span class="pull-right" style="margin-top: -.6rem">
                                            <a href="{{ url('#') }}" class="no-padding">
                                                {!! Form::button('<i class="material-icons middle">monetization_on</i> <span class="middle"> Top Up Saldo</span>', array('class' => 'btn-dark green','type' => '', 'style' => 'padding: 4px 12px')) !!}
                                            </a>
                                        </span>
                                    </div>
                                    <div class="panel-body no-padding">
                                        <div class="content">
                                            {{-- <div class="heading"> --}}
                                            <table class="table table-striped">
                                                <thead style="font-weight: 600; font-size: 1.3rem">
                                                    <tr>
                                                        <td width="5%"></td>
                                                        <td width="15%">Nama</td>
                                                        <td width="55%">Deskripsi</td>
                                                        <td width="15%">Biaya</td>
                                                        <td width="10%">Waktu</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($paket_iklan as $key => $paket)
                                                    <tr>
                                                        @if($iklan->paket()->where('active','=',0)->where('start_at', '=', null)->first())
                                                        <td><input @if($paket->id == $iklan->paket()->where('active','=',0)->where('start_at', '=', null)->first()->id) checked @endif type="radio" name="paket" value="{{ $paket->id }}" @if($paket->harga > Auth::user()->saldo) disabled @endif></td>
                                                        @else
                                                        <td><input type="radio" name="paket" value="{{ $paket->id }}" @if($paket->harga > Auth::user()->saldo) disabled @endif></td>
                                                        @endif
                                                        <td>
                                                            {{ $paket->name }} 
                                                            <br>
                                                            {{HTML::image(url('asset/img/p_icon_'.$paket->value.'.png/300'), $alt="Photo", $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 45px; right: 4rem; top: 1rem')) }} 
                                                        </td>
                                                        <td>{{ $paket->description }}</td>
                                                        <td>Rp. {!! number_format(($paket->harga), 0, ",", ".") !!}</td>
                                                        <td>{{ $paket->value }} Hari</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-md-12 no-margin">
                                <div class="divider mr-top3">
                                    <p>Paket Promosi Iklan Saat Ini</p>
                                    <p style="color: rgba(0, 0, 0, .5); font-size: 1.2rem; height: 60px; padding-left: 5rem; padding-top: 1rem;">
                                        <?php $jenisPaket = $iklan->paket()->where('active','=',1)->first();
                                            if($jenisPaket) {
                                                $namePaket      = $jenisPaket->name;
                                                $paketStart     = $jenisPaket->pivot->start_at;
                                                $paketEnd       = $jenisPaket->pivot->end_at;
                                                if($paketEnd < date('Y-m-d H:i:s',strtotime(\Carbon\Carbon::now()->toDayDateTimeString()))) {
                                                    $viewJenisPaket = HTML::image(url('asset/img/p_icon_0.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 60px; position: absolute; background: none; left: 1.5rem; top: 6rem'));
                                                }
                                                else {
                                                    $viewJenisPaket = HTML::image(url('asset/img/p_icon_'.$jenisPaket->value.'.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 60px; position: absolute; background: none; left: 1.5rem; top: 6rem'));
                                                }
                                            } else {
                                                $viewJenisPaket = HTML::image(url('asset/img/p_icon_0.png/300'), $alt='Photo', $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 60px; position: absolute; background: none; left: 1.5rem; top: 6rem')); 
                                            }
                                        ?>
                                        {!! $viewJenisPaket !!}
                                        {!!
                                            $namePaket.
                                            '<br><span style="font-size: 1.3rem;"> <span style="font-weight: 600">Durasi</span> : '.
                                            date('Y/m/d', strtotime($paketStart)).
                                            ' - '. date('Y/m/d', strtotime($paketEnd)).
                                            '</span>'
                                        !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 no-padding center">
                                <div class="form-group mr-top2">
                                    {!! Form::button('<i class="material-icons middle">save</i> <span class="middle"> Simpan</span>', array('class' => 'btn-dark blue','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="iklan_id" value="{{ $iklan->id }}">
                    </form>
                </div>
                <div class="panel-footer">
                    <div class="form-group">
                        <div class="col-md-6">
                            <p class="no-margin">
                                <a href="{{ url('/') }}">
                                Kembali ke NyewAja.com
                                </a>
                            </p>
                        </div>
                        <div class="col-md-6 right">
                            {{-- <p class="no-margin">
                                Sudah punya akun? 
                                <a href="{{url('login')}}">
                                Masuk
                                </a>
                            </p> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('partials.footer')
@endsection

@section('footer_script')
<script>
    $(document).ready(function() {
        dataFile = [
            @foreach($iklan->gambarIklan as $gambar)
                {
                    name: "{{ $gambar->file_name }}",
                    size: "{{ $gambar->file_size }}",
                    type: "{{ $gambar->file_mime }}",
                    file: "{{url('img/'.$iklan->id.'/'.$gambar->file_name.'/100')}}",
                    url : "{{url('img/'.$iklan->id.'/'.$gambar->file_name.'/100')}}"
                },
            @endforeach
        ];
        $('#filer_input').filer({
            limit: 6,
            maxSize: 5,
            extensions: ["jpg", "png", "jpeg"],
            showThumbs: true,
            addMore: true,
            files: dataFile,
            captions: {
                button: "Cari Gambar",
                feedback: "Pilih Gambar untuk Iklan Anda",
                feedback2: "Gambar telah dipilih",
                removeConfirmation: "Apakah yakin ingin menghapus gambar ini?"
            },
            onRemove: function(data) {
                var iklan_id = "{{ $iklan->id }}";
                var filerKit = $("#filer_input").prop("jFiler");
                var name_gambar = $(data[0].innerHTML).find(".jFiler-item-title")[0].innerText;
                var token 	    = $('input[name=_token]').val();
                $.ajax({
                    url: '{{ url('/upload/image/delete') }}',
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'JSON',
                    data: { iklan_id: iklan_id, name_gambar: name_gambar },
                    success: function(data) {
                        //console.log(data);
                    }
                });
                
            }, 
            onEmpty: function() {
                $('#' + 'filer_input').prop("required", true);
                $('form').validator('destroy');
                $('form').validator({
                    feedback: {
                        success: 'glyphicon-ok-circle',
                        error: 'glyphicon-remove-circle'
                    }
                });
                $('form').validator('update');
            }
        });
        var filerKit = $("#filer_input").prop("jFiler");
            if(!filerKit.files) {
                $('#' + 'filer_input').prop("required", true);
                $('form').validator('destroy');
                $('form').validator({
                    feedback: {
                        success: 'glyphicon-ok-circle',
                        error: 'glyphicon-remove-circle'
                    }
                });
                $('form').validator('update');
            }
    });
</script>
<script>
    var id_kategori_fix;
    function deleteHarga(caller) {
        $(caller).parent().parent().remove();
        $('form').validator('update');
    }
    @if($iklan)
        @foreach($iklan->waktuSewa as $key => $waktunya)
            @if($key != 0)
            var hargaBuilder = "<div class='row'>\
                            <div class='col-md-4 col-md-offset-1 mr-top2'>\
                                <div class='form-group has-feedback '>\
                                    <span class='form-control-feedback-icon' aria-hidden='false'>\
                                        <i class='material-icons' style='line-height: inherit; font-size: 23px;'>date_range</i>\
                                    </span>\
                                    <input data-trigger='focus' data-toggle='popover' data-content='Masukkan waktu sewa sesuai dengan harga / tarif sewa Anda' id='waktu[]' class='form-control effect' placeholder='Waktu sewa' type='text' name='waktu[]' value='{{$waktunya->waktu}}' required data-original-title='' title=''>\
                                    <span class='glyphicon form-control-feedback' aria-hidden='true'></span>\
                                </div>\
                            </div>\
                            <div class='col-md-1 center' style='padding-top: 3rem'>\
                                <i class='material-icons forward'>forward</i>\
                            </div>\
                            <div class='col-md-4 mr-top2'>\
                                <div class='form-group has-feedback'>\
                                    <span class='form-control-feedback-icon' aria-hidden='false' style='font-size: 1.8rem; line-height: 35px'>\
                                        Rp.\
                                    </span>\
                                    <input data-trigger='focus' data-toggle='popover' data-content='Masukkan harga / tarif sewa sesuai dengan waktu sewa Anda' id='harga[]' class='form-control effect' placeholder='Harga sewa' type='number' name='harga[]' value='{{$waktunya->harga}}' required data-original-title='' title=''>\
                                    <span class='glyphicon form-control-feedback' aria-hidden='true'></span>\
                                </div>\
                            </div>\
                            <div class='col-md-1 right no-padding' style='padding-top: 2.2rem'>\
                                <button class='btn-dark red' onclick='deleteHarga(this)' style='padding: 4px 12px' type='button'><i class='material-icons middle'>delete_forever</i> <span class='middle'></span></button>\
                            </div>\
                        </div>";
                $('.btn_addharga_group').before(hargaBuilder);
                $('form').validator('update');
                $('input').popover();
                @endif
        @endforeach
    @endif
    function addHarga(caller) {
        var hargaBuilder = "<div class='row'>\
                            <div class='col-md-4 col-md-offset-1 mr-top2'>\
                                <div class='form-group has-feedback '>\
                                    <span class='form-control-feedback-icon' aria-hidden='false'>\
                                        <i class='material-icons' style='line-height: inherit; font-size: 23px;'>date_range</i>\
                                    </span>\
                                    <input data-trigger='focus' data-toggle='popover' data-content='Masukkan waktu sewa sesuai dengan harga / tarif sewa Anda' id='waktu[]' class='form-control effect' placeholder='Waktu sewa' type='text' name='waktu[]' value='' required='' data-original-title='' title=''>\
                                    <span class='glyphicon form-control-feedback' aria-hidden='true'></span>\
                                </div>\
                            </div>\
                            <div class='col-md-1 center' style='padding-top: 3rem'>\
                                <i class='material-icons forward'>forward</i>\
                            </div>\
                            <div class='col-md-4 mr-top2'>\
                                <div class='form-group has-feedback'>\
                                    <span class='form-control-feedback-icon' aria-hidden='false' style='font-size: 1.8rem; line-height: 35px'>\
                                        Rp.\
                                    </span>\
                                    <input data-trigger='focus' data-toggle='popover' data-content='Masukkan harga / tarif sewa sesuai dengan waktu sewa Anda' id='harga[]' class='form-control effect' placeholder='Harga sewa' type='number' name='harga[]' value='' required='' data-original-title='' title=''>\
                                    <span class='glyphicon form-control-feedback' aria-hidden='true'></span>\
                                </div>\
                            </div>\
                            <div class='col-md-1 right no-padding' style='padding-top: 2.2rem'>\
                                <button class='btn-dark red' onclick='deleteHarga(this)' style='padding: 4px 12px' type='button'><i class='material-icons middle'>delete_forever</i> <span class='middle'></span></button>\
                            </div>\
                        </div>";
        $(caller).parent().parent().before(hargaBuilder);
        $('form').validator('destroy');
        $('form').validator({
            feedback: {
                success: 'glyphicon-ok-circle',
                error: 'glyphicon-remove-circle'
            }
        });
        $('form').validator('update');
        $('input').popover();
    }

    var token 	    = $('input[name=_token]').val();
    var kategori    = [
        {
            id: '1',
            text: 'Kategori Tersedia',
            children: [
                @foreach ($kategori_utama as $value)
                    { id: '{{ $value->id }}', text: unEntity('{{ $value->name }}') },
                @endforeach
            ]
        },
    ];

    function unEntity(str){
        return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
    }

    $('#' + 'kategori').select2({
        data: kategori,
        placeholder: 'Pilih kategori..'
    });

    $('#' + 'subkategori').select2({
        placeholder: 'Tidak ada sub-kategori',
    });

    $("#subkategori").on("change", function() {
        id_kategori_fix = $(this).val();
        id_iklan        = {{$iklan->id}};
        
        $.ajax({
            url: '{{ url('/iklan/detail/view') }}',
            headers: {'X-CSRF-TOKEN': token},
            type: 'POST',
            datatype: 'JSON',
            data: { id_kategori: id_kategori_fix, id_iklan: id_iklan },
            success: function(data) {
                $('#detailFormKategori').html(data);
                $('form').validator('destroy');
                $('form').validator({
                    feedback: {
                        success: 'glyphicon-ok-circle',
                        error: 'glyphicon-remove-circle'
                    }
                });
                $('form').validator('update');
                $('input').popover();
                $('select').popover();
                $('textarea').popover();
            }
        });

    });

    $("#kategori").on("change", function() {
        id_kategori_fix = $(this).val();
        var url 	= '{{ url('/iklan/getCountSubKategori') }}';
        var params = {
            id_kategori: $(this).val()
        }
        
        $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN': token},
            type: 'POST',
            datatype: 'JSON',
            data: params,
            success: function(data) {
                $('#' + 'subkategori').val(null);
                
                if(data) {
                    $('#' + 'subkategori').prop("disabled", false);
                    $('#' + 'subkategori').prop("required", true);

                    url 	= '{{ url('/iklan/getSubKategori') }}';

                    $.ajax({
                            url: url,
                            headers: {'X-CSRF-TOKEN': token},
                            type: 'POST',
                            datatype: 'JSON',
                            data: params,
                            success: function(data) {
                                
                                var select = document.getElementById("subkategori");
                                var length = select.options.length;
                                for (i = 0; i < length; i++) {
                                    select.options[i] = null;
                                }

                                $('#subkategori').select2({
                                    placeholder: 'Tidak ada sub-kategori',
                                    data: data
                                });

                                @if($iklan->kategori->parent_id != null)
                                    $('#subkategori').val({{ $iklan->kategori_id }}).change();
                                @endif
                            },
                            error: function(data) {
                                location.reload();
                            }
                    });
                } else {
                    $.ajax({
                        url: '{{ url('/iklan/detail/view') }}',
                        headers: {'X-CSRF-TOKEN': token},
                        type: 'POST',
                        datatype: 'JSON',
                        data: { id_kategori: id_kategori_fix },
                        success: function(data) {
                            $('#detailFormKategori').html(data);
                        }
                    });

                    $('#' + 'subkategori').prop("disabled", true);
                    $('#' + 'subkategori').prop("required", false);
                    $('#' + 'subkategori').select2({
                        placeholder: 'Tidak ada sub-kategori',
                    });   
                }
                $('form').validator('destroy');
                $('form').validator({
                    feedback: {
                        success: 'glyphicon-ok-circle',
                        error: 'glyphicon-remove-circle'
                    }
                });
                $('form').validator('update');
            }
        });      
    });
    
    @if($iklan->kategori->parent_id == null)
        $('#kategori').val({{ $iklan->kategori_id }}).change();
    @else
        $('#kategori').val({{ $iklan->kategori->parent_id }}).change();
    @endif
</script>

{{-- Daerah --}}
<script>
var listProvinsi = [
    @foreach (Indonesia::allProvinces() as $value)
        { id: '{{ $value->id }}', text: unEntity('{{ $value->name }}') },
    @endforeach  
];

function unEntity(str){
    return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
}

$('#' + 'provinsi').select2({
    data: listProvinsi,
    placeholder: 'Cari provinsi Anda...'
});

$('#kota').select2({
    placeholder: 'Cari kota Anda...'
});

<?php $kota = Indonesia::findCity($iklan->tempat_id, ['province']) ?>

$('#provinsi').val('{{ $kota->province->id }}').change();
var listKota = [
    @foreach (Indonesia::findProvince($kota->province->id, ['cities'])->cities as $value)
        { id: '{{ $value->id }}', text: unEntity('{{ $value->name }}') },
    @endforeach  
];
$('#kota').select2({
    data: listKota,
    placeholder: 'Cari kota Anda...'
});

$('#kota').val('{{$kota->id}}').change();

$('#provinsi').on('change', function() {
    var provinsi_id = $(this).val();    
    $.ajax({
        url: '{{ url('/setting/ajak/getKota') }}',
        type: 'GET',
        datatype: 'JSON',
        data: { provinsi_id: provinsi_id },
    }).then(function (response) {
        var select = document.getElementById("kota");
        var length = select.options.length;
        for (i = 0; i < length; i++) {
            select.options[i] = null;
        }
        
        $('#kota').select2({
            placeholder: 'Cari kota Anda...',
            data: response
        });
    });
});
</script>
@endsection