@if($tipe_input == 'input')
<div class="row">
    <div class="col-md-12 col-md-offset-1">
        <div class="col-md-{{ $col_width }} no-padding">
            <div class="form-group has-feedback {{ $errors->has($id_input) ? ' has-error' : '' }}">
                <label for="{{ $id_input }}">{{ $name_input }}<span>*</span></label>

                <span class="form-control-feedback-icon" aria-hidden="false">
                    <i class="material-icons" style="line-height: inherit; font-size: 23px;">@if(isset($icon_input)) {{$icon_input}} @else donut_small @endif</i>
                </span>

                <input  data-trigger="focus" 
                        data-toggle="popover"
                        data-content="{{ $popover_content }}" 
                        id="{{ $id_input }}" class="form-control effect" 
                        placeholder="{{ $name_input }}" type="{{ $input_type }}" name="{{ $id_input }}" value="@if(isset($value_input)){{$value_input}}@else{{ old($id_input) }}@endif"
                        required
                        >
                @if(isset($after_input))
                <span class="form-control-feedback-icon" aria-hidden="false" style="top: 2.5rem;left: inherit;right: -4rem;font-size: 2rem;font-weight: 600;color: rgba(0, 0, 0, .4);">
                    {!! $after_input !!}
                </span>
                @endif
                <span class="help-block pull-left effect">
                    @if ($errors->has($id_input))
                        {{ $errors->first($id_input) }}
                    @else
                        {{ $foot_note }}
                    @endif
                </span>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
        </div>
    </div>
</div>
@endif

@if($tipe_input == 'select')
<div class="row">
    <div class="col-md-{{ $col_width }} col-md-offset-1">
        <div class="form-group has-feedback {{ $errors->has($id_input) ? ' has-error' : '' }}">
            <label for="{{ $id_input }}">{{ $name_input }} <span>*</span></label>
            @if(isset($icon) && !$icon)
            @else
            <span class="form-control-feedback-icon" aria-hidden="false">
                <i class="material-icons" style="line-height: inherit; font-size: 23px;">@if(isset($icon_input)) {{$icon_input}} @else donut_small @endif</i>
            </span>
            @endif
            {!! Form::select($id_input, array() , null, array( $select_type => '', 'class' =>'js-example-basic-'.$select_type.' js-states form-control', 'id' => $id_input, 'style' => '', 'required' => 'required')) !!}
            <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
        </div>
    </div>
</div>
<script>
    $('#'+'{{$id_input}}').select2({
        placeholder: 'Pilih '+ '{{$name_input}}',
        data: {!! json_encode($data) !!}
    });
    @if(isset($value_input))
        $('#'+'{{$id_input}}').val('{{$value_input}}').change();
    @endif
</script>
@endif

@if($tipe_input == 'textarea')

<div class="row">
    <div class="col-md-12 col-md-offset-1">
        <div class="col-md-{{ $col_width }} no-padding">
            <div class="form-group has-feedback {{ $errors->has($id_input) ? ' has-error' : '' }}">
                <label for="{{ $id_input }}">{{ $name_input }}<span>*</span></label>

                <span class="form-control-feedback-icon" aria-hidden="false">
                    <i class="material-icons" style="line-height: inherit; font-size: 23px;">@if(isset($icon_input)) {{$icon_input}} @else donut_small @endif</i>
                </span>
                
                <textarea   data-trigger="focus" 
                            data-toggle="popover"
                            data-content="{{$popover_content}}" 
                            id="{{ $id_input }}" class="form-control effect" 
                            placeholder="{{ $name_input }}"  
                            name="{{ $id_input }}"
                            style="max-width: 100%; min-height: 10rem"
                            required
                            >@if(isset($value_input)){!! str_replace('<br>', '&#10;',$value_input) !!}@endif</textarea>
                @if(isset($after_input))
                <span class="form-control-feedback-icon" aria-hidden="false" style="top: 2.5rem;left: inherit;right: -4rem;font-size: 2rem;font-weight: 600;color: rgba(0, 0, 0, .4);">
                    {!! $after_input !!}
                </span>
                @endif
                <span class="help-block pull-left effect">
                    @if ($errors->has($id_input))
                        {{ $errors->first($id_input) }}
                    @else
                        {{ $foot_note }}
                    @endif
                </span>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
        </div>
    </div>
</div>
@endif