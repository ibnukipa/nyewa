<!--Motor Mobil-->

<?php $count_detail = 0 ?>
@if($kategori->hasDetail('merk'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'text',
            'col_width'         => 5,
            'id_input'          => 'merk',
            'name_input'        => 'Merk '.$kategori->name,
            'popover_content'   => 'Merk '.$kategori->name.' yang anda iklan-kan',
            'foot_note'         => 'contoh: Honda, Yamaha, Asus, Samsung, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php
            $selectedDetail = $iklan->details->where('name', 'merk');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('tipe'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'text',
            'col_width'         => 5,
            'id_input'          => 'tipe',
            'name_input'        => 'Tipe '.$kategori->name,
            'popover_content'   => 'Tipe '.$kategori->name.' yang anda iklan-kan',
            'foot_note'         => 'contoh: Supra X, Beat, Xperia Z5, Galaxy Note 7, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'tipe');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('transmisi'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'text',
            'col_width'         => 4,
            'id_input'          => 'transmisi',
            'name_input'        => 'Transmisi',
            'popover_content'   => 'Transimisi kendaraan yang anda iklan-kan',
            'foot_note'         => 'contoh: Manual, Automatic, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'transmisi');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('tahun'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'number',
            'col_width'         => 3,
            'id_input'          => 'tahun',
            'name_input'        => 'Tahun',
            'popover_content'   => 'Tahun kendaraan yang anda iklan-kan',
            'foot_note'         => 'contoh: 2005, 2011, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'tahun');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('luas tanah'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'number',
            'after_input'       => 'm<sup>2</sup>',
            'col_width'         => 3,
            'id_input'          => 'luas_tanah',
            'name_input'        => 'Luas Tanah',
            'popover_content'   => 'Luas Tanah yang anda iklan-kan',
            'foot_note'         => 'contoh: 100, 1000, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'luas tanah');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('luas bangunan'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'number',
            'after_input'       => 'm<sup>2</sup>',
            'col_width'         => 3,
            'id_input'          => 'luas_bangunan',
            'name_input'        => 'Luas Bangunan',
            'popover_content'   => 'Luas Bangunan yang anda iklan-kan',
            'foot_note'         => 'contoh: 30, 50, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'luas bangunan');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('lantai'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'number',
            'col_width'         => 2,
            'id_input'          => 'lantai',
            'name_input'        => 'Lantai',
            'popover_content'   => 'Lantai yang anda iklan-kan',
            'foot_note'         => 'contoh: 1, 23, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'lantai');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('kamar tidur'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'number',
            'col_width'         => 3,
            'id_input'          => 'kamar_tidur',
            'name_input'        => 'Kamar Tidur',
            'popover_content'   => 'Jumlah kamar tidur yang anda iklan-kan',
            'foot_note'         => 'contoh: 1, 5, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'kamar tidur');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('kamar mandi'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'number',
            'col_width'         => 3,
            'id_input'          => 'kamar_mandi',
            'name_input'        => 'Kamar Mandi',
            'popover_content'   => 'Jumlah kamar mandi yang anda iklan-kan',
            'foot_note'         => 'contoh: 1, 2, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'kamar mandi');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('sertifikasi'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'select',
            'col_width'         => 4,
            'icon_input'        => 'verified_user',
            'id_input'          => 'sertifikasi',
            'select_type'       => 'single',
            'name_input'        => 'Sertifikasi',
            'popover_content'   => 'Jumlah kamar mandi yang anda iklan-kan',
            'foot_note'         => 'contoh: 1, 2, dll',
            'data'              => [
                                        ['id' => 'SHM - Sertifikat Hak Milik', 'text' => 'SHM - Sertifikat Hak Milik'],
                                        ['id' => 'HGB - Hak Guna Bangun', 'text' => 'HGB - Hak Guna Bangun'],
                                        ['id' => 'Lainnya (PPJB,Girik,Adat,dll)', 'text' => 'Lainnya (PPJB,Girik,Adat,dll)'],
                                    ]
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'sertifikasi');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('fasilitas'))
<?php $count_detail++ ?>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="form-group has-feedback {{ $errors->has('fasilitas') ? ' has-error' : '' }}">
            <label for="fasilitas">Fasilitas</label>
            @if(isset($icon) && !$icon)
            @else
            <span class="form-control-feedback-icon" aria-hidden="false">
                <i class="material-icons" style="line-height: inherit; font-size: 23px;"></i>
            </span>
            @endif
            {!! Form::select('fasilitas[]', $fasilitas , null, array( 'multiple' => '', 'class' =>'js-example-basic-multiple js-states form-control', 'id' => 'fasilitas', 'style' => '', 'required' => 'required')) !!}
            <span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
        </div>
    </div>
</div>
<?php
    if(isset($iklan)) {
        $fasilitass = $iklan->details->where('name', 'fasilitas');
        $fasilitass = $fasilitass->all();
    }
?>

<script>
    $('#'+'fasilitas').select2({
        placeholder: 'Pilih '+ 'Fasilitas'
    });
    var selectedFasilitas = [];
    @if(isset($iklan))
        @foreach($fasilitass as $key => $fasil)
            selectedFasilitas.push("{{$fasil->value}}");
        @endforeach
        $('#'+'fasilitas').val(selectedFasilitas).change();
    @endif
</script>
@endif

@if($kategori->hasDetail('alamat lokasi'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'textarea',
            'input_type'        => 'text',
            'icon_input'        => 'place', 
            'col_width'         => 10,
            'id_input'          => 'alamat_lokasi',
            'name_input'        => 'Alamat Lokasi',
            'popover_content'   => 'Detail alamat lokasi yang anda iklan-kan',
            'foot_note'         => 'contoh: Jalan Arif Rahman Hakim No.113',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'alamat lokasi');
            $selectedDetail = $selectedDetail->first();
            
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('ukuran'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'text',
            'col_width'         => 3,
            'id_input'          => 'ukuran',
            'name_input'        => 'Ukuran',
            'popover_content'   => 'Ukuran barang yang anda iklan-kan',
            'foot_note'         => 'contoh: XL, M, 38, 42, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'ukuran');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('warna'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'text',
            'col_width'         => 3,
            'id_input'          => 'warna',
            'name_input'        => 'Warna',
            'popover_content'   => 'Warna barang yang anda iklan-kan',
            'foot_note'         => 'contoh: Merah, Biru Donker, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'warna');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('luas lapangan'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'number',
            'after_input'       => 'm<sup>2</sup>',
            'col_width'         => 3,
            'id_input'          => 'luas_lapangan',
            'name_input'        => 'Luas Lapangan',
            'popover_content'   => 'Luas Lapangan yang anda iklan-kan',
            'foot_note'         => 'contoh: 50, 100, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'luas lapangan');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('pengarang'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'text',
            'icon_input'        => 'person',
            'col_width'         => 5,
            'id_input'          => 'pengarang',
            'name_input'        => 'Nama Pengarang',
            'popover_content'   => 'Nama lengkap pengarang yang anda iklan-kan',
            'foot_note'         => 'contoh: Agatha Christie, Tere Liye, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'pengarang');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($kategori->hasDetail('tahun cetak'))
    <?php $count_detail++ ?>
    <?php
        $data = [
            'tipe_input'        => 'input',
            'input_type'        => 'number',
            'col_width'         => 3,
            'id_input'          => 'tahun_cetak',
            'name_input'        => 'Tahun Cetak',
            'popover_content'   => 'Tahun cetak yang anda iklan-kan',
            'foot_note'         => 'contoh: 2010, 2015, dll',
        ];
    ?>
    @if(isset($iklan))
        <?php 
            $selectedDetail = $iklan->details->where('name', 'tahun cetak');
            $selectedDetail = $selectedDetail->first();
            if($selectedDetail) 
                $data['value_input']    = $selectedDetail->value;
        ?>
    @endif
    @include('iklan.detailFormBuilder', $data)
@endif

@if($count_detail == 0)
<p style="color: rgba(0, 0, 0, .3); font-size: 2rem; font-weight: 300">
    Tidak ada detail iklan yang perlu ditambahkan
</p>
@endif