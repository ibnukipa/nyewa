@foreach(Auth::user()->iklan()->where('status', '=', 'new')->orWhere('status', '=', 'edited')->get() as $key => $value)
<div class="list-group-item">
    <div class="row">
        <div class="col-md-2">
            <div class="image">
                <img display="block" margin="auto" data-src="{{url('img/'.$value->id.'/'.$value->gambarIklan[0]->file_name.'/200')}}">
                <span class="badge green">Pending</span>
            </div>

        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10 no-padding-right">
                    <div class="content-iklan" style="padding: 0; padding-top: 1rem">
                        <h4 class="list-group-item-heading">
                            {{ $value->name }}
                        </h4>
                    </div>
                </div>
                <div class="col-md-2 no-padding-left">
                        <?php $jenisPaket = $value->paket()->first(); ?>
                        @if($jenisPaket)
                        <a>
                            {{HTML::image(url('asset/img/p_icon_'.$jenisPaket->value.'.png/300'), $alt="Photo", $attributes = array(
                                    'style'         => 'display: block; margin-auto; width: auto; height: 80px; position: absolute; right: 4rem; top: 1rem',
                                    'data-trigger'  => 'focus',
                                    'data-toggle'   => 'popover',
                                    'data-content'  => 'popover',
                                    'rel'           => 'popover'
                                    )) }}
                        </a>
                        @else
                            {{HTML::image(url('asset/img/p_icon_0.png/300'), $alt="Photo", $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 80px; position: absolute; right: 4rem; top: 1rem')) }}
                        @endif                    
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 ">
                    <div class="content-iklan" style="padding: 0">
                        <div class="list-group-item-text">
                            <div class="child" style="font-size: 2rem">
                                Rp. {!! number_format($value->waktuSewa[0]->harga, 0, ",", ".") !!}
                                <span class="c-grey">({!! $value->waktuSewa[0]->waktu !!})</span>
                            </div>
                            <div class="child pull-right">
                                <a href="{{ url('/iklan/detail/'.$value->url_iklan.'.html') }}" class="no-padding">
                                    {!! Form::button('<i class="material-icons middle">remove_red_eye</i><span class="middle"> Lihat</span>', array(
                                                'class'             => 'btn btn-small transparent left',
                                                'style'             => 'padding: 4px 12px',
                                                )) !!}
                                </a>
                                <a href="{{ url('/iklan/edit/'.$value->id) }}" class="no-padding">
                                    {!! Form::button('<i class="material-icons middle">edit</i><span class="middle"> Edit</span>', array(
                                                'class'             => 'btn-dark btn-small grey-dark left',
                                                'style'             => 'padding: 4px 12px',
                                                )) !!}
                                </a>
                                <a class="no-padding">
                                    {!! Form::open(array('route' => 'iklan.destroy', 'class' => 'form-button inline', 'method' => 'POST')) !!}
                                        {!! Form::hidden('iklan_id', $value->id) !!}
                                        {!! Form::button(   '<i class="material-icons middle">delete</i><span class="middle"> Hapus</span>', 
                                                            array(  'class'             => 'btn-dark btn-small red left',
                                                                    'style'             => 'padding: 4px 12px',
                                                                    'type'              => 'button', 
                                                                    'data-toggle'       => 'modal',
                                                                    'data-backdrop'     => 'static',
                                                                    'data-keyboard'     => 'false',
                                                                    //ini untuk detail modal
                                                                    'data-target'       => '#confirmDelete', 
                                                                    'data-body-title'   => 'Apakah Anda yakin untuk <strong>menghapus</strong> Iklan ini?', 
                                                                    'data-body-content' => $value->name
                                                                )
                                                        ) 
                                        !!}
                                    {!! Form::close() !!}
                                </a>
                                {{-- <a href="#" class="no-padding">
                                    {!! Form::button('<i class="material-icons middle">grade</i> <span class="middle"> Promosikan</span>', array('class' => 'btn-dark btn-small green left','type' => '', 'style' => 'padding: 4px 12px')) !!}
                                </a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
{{ $listIklan->links() }}