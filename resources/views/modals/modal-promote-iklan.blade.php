<div class="modal animated fadeIn" id="modalPromote" role="dialog" aria-labelledby="modalPromoteLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 60%">
		<div class="modal-content">
			<div class="modal-header green" style="font-size: 2rem; line-height: 2rem; padding: 1rem; font-weight: 600" >
				<i class="material-icons middle" style="font-size: 3rem">grade</i> <span class="middle"> Promosikan Iklan</span>
			</div>
			<div class="modal-body">
				<span id="body-title" style="font-size: 1.5rem; font-weight: 400; color: rgba(34, 34, 34, .5)">
					<div class="row">
						<div class="col-md-2">
							Iklan
						</div>
						<div class="col-md-1 no-padding right">
							:
						</div>
						<div class="col-md-9">
							<strong id="promote_name_iklan">Judul Iklan yang akan dibahas pada pesan ini</strong>
						</div>
					</div>
                    <div class="row">
						<div class="col-md-2">
							Paket saat ini
						</div>
						<div class="col-md-1 no-padding right">
							:
						</div>
						<div class="col-md-9" id="promote_paket_iklan">
							Tidak ada
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 no-margin">
							<div class="divider mr-top1">
							</div>
						</div>
					</div>
				</span>
				{!! Form::open(array('route' => 'iklan.promote', 'class' => 'form-button', 'method' => 'POST')) !!}
					{!! Form::hidden('iklan_id', null, array('id' => 'iklan_id')) !!}
					<div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-sewaaja">
                                <div class="panel-heading" style="padding: 2rem 2rem; line-height: 3rem; min-height: 90px; max-height: 90px;">
                                    <span class="left" style="font-weight: 600; font-size: 3rem; font-family: 'Roboto'">
                                        <p style="font-size: 1.2rem; font-weight: 400; line-height: initial; margin: 0">Saldo Anda</p>
                                        Rp. {!! number_format((Auth::user()->saldo), 0, ",", ".") !!}
                                    </span>
                                    <span class="pull-right" style="margin-top: -.6rem">
                                        <a href="{{ url('#') }}" class="no-padding">
                                            {!! Form::button('<i class="material-icons middle">monetization_on</i> <span class="middle"> Top Up Saldo</span>', array('class' => 'btn-dark green','type' => '', 'style' => 'padding: 4px 12px')) !!}
                                        </a>
                                    </span>
                                </div>
                                <div class="panel-body no-padding">
                                    <div class="content">
                                        {{-- <div class="heading"> --}}
                                        <table class="table table-striped">
                                            <thead style="font-weight: 600; font-size: 1.3rem">
                                                <tr>
                                                    <td width="5%"></td>
                                                    <td width="15%">Nama</td>
                                                    <td width="55%">Deskripsi</td>
                                                    <td width="15%">Biaya</td>
                                                    <td width="10%">Waktu</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $paket_iklan = App\Paket::get(); ?>
                                                @foreach($paket_iklan as $key => $paket)
                                                <tr>
                                                    <td><input required type="radio" name="paket" value="{{ $paket->id }}" @if($paket->harga > Auth::user()->saldo) disabled @endif></td>
                                                    <td>
                                                        {{ $paket->name }} 
                                                        <br>
                                                        {{HTML::image(url('asset/img/p_icon_'.$paket->value.'.png/300'), $alt="Photo", $attributes = array('style' => 'display: block; margin-auto; width: auto; height: 45px; right: 4rem; top: 1rem')) }} 
                                                    </td>
                                                    <td>{{ $paket->description }}</td>
                                                    <td>Rp. {!! number_format(($paket->harga), 0, ",", ".") !!}</td>
                                                    <td>{{ $paket->value }} Hari</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
			</div>
			<div class="modal-footer center">
				{!! Form::button('<i class="material-icons middle">close</i> <span class="middle"> Batal</span>', array('class' => 'btn btn-half grey','type' => 'button', 'data-dismiss' => 'modal')) !!}
				{!! Form::button('<i class="material-icons middle">grade</i> <span class="middle"> Promosikan Iklan</span>', array('class' => 'btn-dark btn-half green','type' => 'submit')) !!}

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>