<div class="modal animated fadeIn" id="changeBlockedUser" role="dialog" aria-labelledby="changeBlockedUserLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			{{-- <div class="modal-header btn-green">
				<h4 class="modal-title center">
					<span style="font-size: 1.5rem; font-weight: 600">
						Apakah Anda yakin?
					</span>
				</h4>
			</div> --}}
			<div class="modal-body">
				<span id="body-title" style="font-size: 1.5rem; font-weight: 400; color: rgba(34, 34, 34, .5)"></span>
				<br>
				<p id="body-content" style="font-size: 2rem;"></p>
			</div>
			<div class="modal-footer center">
				{!! Form::button('<span class="middle"> BATAL</span>', array('class' => 'btn btn-half grey','type' => 'button', 'data-dismiss' => 'modal')) !!}
				{!! Form::button('<span class="middle"> YA </span>', array('class' => 'btn-dark btn-half green','type' => 'button', 'id' => 'confirm')) !!}
			</div>
		</div>
	</div>
</div>