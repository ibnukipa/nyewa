<div class="modal animated fadeIn" id="sendPesan" role="dialog" aria-labelledby="sendPesanLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header blue" style="font-size: 2rem; line-height: 2rem; padding: 1rem; font-weight: 600" >
				<i class="material-icons middle" style="font-size: 3rem">email</i> <span class="middle"> KIRIM PESAN</span>
			</div>
			<div class="modal-body">
				<span id="body-title" style="font-size: 1.5rem; font-weight: 400; color: rgba(34, 34, 34, .5)">
					<div class="row" style="margin-bottom: 5px">
						<div class="col-md-2">
							Kepada
						</div>
						<div class="col-md-1 no-padding right">
							:
						</div>
						<div class="col-md-9">
							<strong id="pesan_nama_pelapak">Ibnu Prayogi</strong>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							Iklan
						</div>
						<div class="col-md-1 no-padding right">
							:
						</div>
						<div class="col-md-9">
							<strong id="pesan_nama_iklan">Judul Iklan yang akan dibahas pada pesan ini</strong>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 no-margin">
							<div class="divider mr-top1">
							</div>
						</div>
					</div>
				</span>
				{!! Form::open(array('route' => 'pesan.iklan.store', 'class' => 'form-button', 'method' => 'POST')) !!}
					{!! Form::hidden('iklan_id', null, array('id' => 'iklan_id')) !!}
					{!! Form::hidden('to_user_id', null, array('id' => 'to_user_id')) !!}
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="isi_pesan">Isi Pesan <span>*</span></label>

								<textarea  data-trigger="focus" 
										data-toggle="popover"
										data-content="Tuliskan pesan yang singkat, padat, dan jelas. Agar cepat mendapatkan respon dari pelapak" 
										id="isi_pesan" class="form-control effect" 
										placeholder="Isi pesan"  
										name="isi_pesan"
										style="max-width: 100%; min-height: 10rem"
										value="{{ old('isi_pesan') }}"
										required
										></textarea>

								<span class="help-block effect">
									Maksimal 4085 karakter
								</span>
								<span class="glyphicon form-control-feedback select" aria-hidden="true"></span>
							</div>
						</div>
					</div>
			</div>
			<div class="modal-footer center">
				{!! Form::button('<i class="material-icons middle">close</i> <span class="middle"> Batal</span>', array('class' => 'btn btn-half grey','type' => 'button', 'data-dismiss' => 'modal')) !!}
				{!! Form::button('<i class="material-icons middle">send</i> <span class="middle"> Kirim</span>', array('class' => 'btn-dark btn-half blue','type' => 'submit')) !!}

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>