@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-sewaaja">
                <div class="panel-heading blue-darken">Daftar</div>
                <div class="panel-body">
                    <form class="form-horizontal not-valid" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-12">
                                @include('partials.status')
                            </div>
                        </div>
                        <div class="col-md-8 col-md-offset-2">
                            @include('builder.form',[
                                'col_width'         => 12,
                                'tipe_input'        => 'input',
                                'input_type'        => 'name',
                                'icon_input'        => 'person',
                                'id_input'          => 'name',
                                'value_input'       => old('name'),
                                //'after_input'       => 'm<sup>2</sup>',
                                'name_input'        => 'Nama',
                                'popover_content'   => 'Masukkan nama lengkap Anda',
                                //'popover_position'  => 'bottom',
                                //'foot_note'         => '',
                            ])
                        </div>

                        <div class="col-md-8 col-md-offset-2 mr-top2">
                            @include('builder.form',[
                                'col_width'         => 12,
                                'tipe_input'        => 'input',
                                'input_type'        => 'email',
                                'icon_input'        => 'email',
                                'id_input'          => 'email',
                                'value_input'       => old('email'),
                                //'after_input'       => 'm<sup>2</sup>',
                                'name_input'        => 'Email',
                                'popover_content'   => 'Masukkan email yang valid untuk proses aktifasi akun Anda',
                                //'popover_position'  => 'bottom',
                                //'foot_note'         => '',
                            ])
                        </div>

                        <div class="col-md-8 col-md-offset-2 mr-top2">
                            @include('builder.form',[
                                'col_width'         => 12,
                                'tipe_input'        => 'input',
                                'input_type'        => 'password',
                                'icon_input'        => 'lock',
                                'id_input'          => 'password',
                                'value_input'       => old('password'),
                                //'after_input'       => 'm<sup>2</sup>',
                                'name_input'        => 'Password',
                                'popover_content'   => 'Masukkan password untuk verifikasi akun Anda setiap masuk SewaAja',
                                //'popover_position'  => 'bottom',
                                'foot_note'         => 'minimal 6 karater',

                                'minlength'             => '6',
                            ])
                        </div>

                        <div class="col-md-8 col-md-offset-2">
                            @include('builder.form',[
                                'col_width'             => 12,
                                'tipe_input'            => 'input',
                                'input_type'            => 'password',
                                'icon_input'            => 'lock',
                                'id_input'              => 'password_confirmation',
                                'value_input'           => old('password_confirmation'),
                                //'after_input'         => 'm<sup>2</sup>',
                                'name_input'            => 'Konfirmasi Password',
                                'popover_content'       => 'Masukkan kembali password',
                                //'popover_position'    => 'bottom',
                                'foot_note'             => 'konfirmasi password',

                                'minlength'             => '6',
                                'match'                 => '#password',
                                //'match_content'       => 'Whoops, konfirmasi password tidak cocok'
                            ])
                        </div>
                        {{-- <div class="col-md-8 col-md-offset-2 mr-top2">
                            <div class="form-group">
                                <div class="checkbox" style="padding-bottom: 2rem; line-height: inherit">
                                    <label style="font-weight: 400">
                                        <input type="checkbox" name="remember" required> Dengan mendaftar di SewaAja, Anda menyetujui <a href="#">Syarat & Ketentuan SewaAja.com</a>
                                    </label>
                                </div>
                            </div>
                        </div> --}}
                        <div class="col-md-12 center">
                            <div class="form-group">
                                {!! Form::button('<i class="material-icons middle">assignment_turned_in</i> <span class="middle"> Daftar</span>', array('class' => 'btn-dark green-dark','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
                            </div>    
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <div class="form-group">
                        <div class="col-md-6">
                            <p class="no-margin">
                                <a href="{{ url('/') }}">
                                Kembali ke SewaAja.com
                                </a>
                            </p>
                        </div>
                        <div class="col-md-6 right">
                            <p class="no-margin">
                                Sudah punya akun? 
                                <a href="{{url('login')}}">
                                Masuk
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('partials.footer')
@endsection