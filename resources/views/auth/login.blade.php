@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-sewaaja" style="margin-top: 2rem; margin-bottom: 0">
                <div class="panel-heading red-darken">Masuk</div>
                <div class="panel-body">
                    <form class="form-horizontal not-valid" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            @if (session('warning'))
                                <div class="alert alert-warning">
                                    {{ session('warning') }}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-8 col-md-offset-2 no-padding">
                            @include('builder.form',[
                                'col_width'         => 12,
                                'tipe_input'        => 'input',
                                'input_type'        => 'email',
                                'icon_input'        => 'email',
                                'id_input'          => 'email',
                                'value_input'       => old('email'),
                                'name_input'        => 'Email',
                                'popover_content'   => 'Masukkan email yang valid untuk proses aktifasi akun Anda',
                            ])
                        </div>

                        <div class="col-md-8 col-md-offset-2 mr-top2 no-padding">
                            @include('builder.form',[
                                'col_width'         => 12,
                                'tipe_input'        => 'input',
                                'input_type'        => 'password',
                                'icon_input'        => 'lock',
                                'id_input'          => 'password',
                                'value_input'       => old('password'),
                                'name_input'        => 'Password',
                                'popover_content'   => 'Masukkan password untuk verifikasi akun Anda setiap masuk SewaAja',
                                'minlength'         => '6',
                            ])
                        </div>

                        {{-- <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="checkbox" style="padding-top: 0">
                                    <label style="font-weight: 400">
                                        <input type="checkbox" name="remember"> Biarkan saya tetap masuk
                                    </label>
                                </div>
                            </div>
                        </div> --}}

                        {{-- <div class="col-md-8 col-md-offset-2 mr-top1">
                            <div class="form-group">
                                <div class="checkbox" style="padding-bottom: 2rem; line-height: inherit">
                                    <label style="font-weight: 400">
                                        <input type="checkbox" name="remember"> Biarkan saya tetap masuk
                                    </label>
                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group ">
                            <div class="col-md-12 center mr-top1">
                                {!! Form::button('<i class="material-icons middle">exit_to_app</i> <span class="middle"> Masuk</span>', array('class' => 'btn-dark green-dark','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
                            </div>
                        </div>
                    </form>
                </div>

                <div class="panel-footer">
                    <div class="form-group">
                        <div class="col-md-6">
                            <p class="no-margin">
                                <a href="{{ url('password/reset') }}">
                                Saya tidak bisa masuk?
                                </a>
                            </p>
                        </div>
                        <div class="col-md-6 right">
                            <p class="no-margin">
                                Belum punya akun? 
                                <a href="{{url('register')}}">
                                Daftar
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('auth.login-social')
</div>
@endsection


@section('footer')
    @include('partials.footer')
@endsection