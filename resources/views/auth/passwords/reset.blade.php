@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-sewaaja">
                <div class="panel-heading blue-darken">Reset Password</div>

                <div class="panel-body">
                    <form class="form-horizontal not-valid" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">Email</label>
                                <input id="email" class="form-control effect" placeholder="Email" type="email" name="email" value="{{  $email or old('email') }}">
                                <span class="form-control-feedback" aria-hidden="true">
                                    <i class="material-icons" style="line-height: inherit; font-size: 23px;">email</i>
                                </span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        {{ $errors->first('email') }}
                                    </span>
                                @else
                                    <span class="help-block">
                                        {{-- Catatan kaki --}}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-8 col-md-offset-2 mr-top2">
                            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password">Password Baru</label>
                                <input id="password" class="form-control effect" placeholder="Password Baru" type="password" name="password">
                                <span class="form-control-feedback" aria-hidden="true">
                                    <i class="material-icons" style="line-height: inherit; font-size: 23px;">lock</i>
                                </span>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        {{ $errors->first('password') }}
                                    </span>
                                @else
                                    <span class="help-block">
                                        {{-- Catatan kaki --}}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-8 col-md-offset-2 mr-top2">
                            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label for="password_confirmation">Konfirmasi Password Baru</label>
                                <input id="password_confirmation" class="form-control effect" placeholder="Konfirmasi Password Baru" type="password" name="password_confirmation">
                                <span class="form-control-feedback" aria-hidden="true">
                                    <i class="material-icons" style="line-height: inherit; font-size: 23px;">lock</i>
                                </span>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        {{ $errors->first('password_confirmation') }}
                                    </span>
                                @else
                                    <span class="help-block">
                                        {{-- Catatan kaki --}}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 center" style="margin-top: 1rem">
                                {!! Form::button('<i class="material-icons middle">cached</i> <span class="middle"> Reset Password</span>', array('class' => 'btn-dark green-dark','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
