@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-sewaaja">
                <div class="panel-heading blue-darken">Ganti Password</div>
                
                <div class="panel-body ">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal not-valid" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">Email</label>
                                <input id="email" class="form-control effect" placeholder="Email" type="email" name="email" value="{{ old('email') }}">
                                <span class="form-control-feedback" aria-hidden="true">
                                    <i class="material-icons" style="line-height: inherit; font-size: 23px;">email</i>
                                </span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        {{ $errors->first('email') }}
                                    </span>
                                @else
                                    <span class="help-block">
                                        {{-- Catatan kaki --}}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 center" style="margin-top: 1rem">
                                {!! Form::button('<i class="material-icons middle">send</i> <span class="middle"> Kirim Request Ganti Password</span>', array('class' => 'btn-dark green-dark','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <div class="form-group">
                        <div class="col-md-6">
                            <p class="no-margin">
                                Sudah punya akun?
                                <a href="{{ url('login') }}">
                                Masuk
                                </a>
                            </p>
                        </div>
                        <div class="col-md-6 right">
                            <p class="no-margin">
                                Belum punya akun? 
                                <a href="{{url('register')}}">
                                Daftar
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('partials.footer')
@endsection