<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-sewaaja" style="margin-top: 2rem">
            <div class="panel-body red-darken">
                <div class="row">
                    <div class="col-md-6 center">
                        <p>Masuk dengan Facebook</p>
                        <a href="{{ route('social.login', ['facebook']) }}" style="display: inline-block">
                            {!! Form::button('<i class="fa fa-facebook" aria-hidden="true"></i> Facebook</span>', array('class' => 'btn-dark blue','type' => '', 'style' => 'padding: 4px 12px')) !!}
                        </a>
                    </div>
                    <div class="col-md-6 center">
                        <p>Masuk dengan Google</p>
                        <a href="{{ route('social.login', ['google']) }}" style="display: inline-block">
                        {!! Form::button('<i class="fa fa-google-plus" aria-hidden="true"></i> Google+</span>', array('class' => 'btn-dark red','type' => '', 'style' => 'padding: 4px 12px')) !!}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
