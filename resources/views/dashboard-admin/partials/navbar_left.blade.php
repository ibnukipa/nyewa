 <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ url('/admin') }}"><i class="fa fa-home fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-tag fa-fw"></i> Iklan<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('/admin/iklan/baru') }}">Iklan Baru</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/iklan/edit') }}">Edit Approval</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/iklan') }}">Iklan aktif</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/iklan/blocked') }}">Iklan terblokir</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-comments fa-fw"></i> Komentar<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('/admin/komentar/moderasi') }}">Moderasi Komentar</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/komentar') }}">Semua Komentar</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{ url('/admin/komplain') }}"><i class="fa fa-exclamation-circle fa-fw"></i> Komplain</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Akun User<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('/admin/user') }}">Akun Aktif</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/user/blocked') }}">Akun terblokir</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-book fa-fw"></i> Laporan<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('/admin/laporan/keuangan') }}">Keuangan</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/laporan/jumlah_user') }}">Jumlah User</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench   fa-fw"></i> Setting<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Iklan<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{ url('admin/setting/iklan/fasilitas') }}">Fasilitas</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">Halaman<span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="{{ url('admin/setting/halaman/baru') }}">Membuat Halaman Baru</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('admin/setting/halaman') }}">Semua Halaman</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/setting/papaniklan') }}">Papan Sponsor</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>