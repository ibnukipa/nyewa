@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Edit Papan Iklan</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
                <div class="panel panel-default" style="background:#e7e7e7">
                   <h4 style="padding-left:5px;">Papan Iklan (@Home)</h4>
                </div>
                   <img src="{{ URL::asset('img/slogan_banner.png') }}" alt="Logo sewaaja" style="padding:3px;padding-left:20px;height:300px">
                   <div class="form-group">
                    <label>Ubah Gambar</label>
                    <input type="file" name="gambar" >
                    <input type="hidden" name="image" value="">
                </div>
                </div>

        <!-- /.col-lg-12 -->
    </div>
@endsection