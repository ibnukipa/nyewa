@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-9">
            <h2 class="page-header">List Fasilitas</h2>
        </div>
        <div class="col-lg-3">
            <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-bottom: 9px;
    margin: 40px 0 20px;">
                <a href="#" data-toggle="modal" data-target="#model"><i class="fa fa-plus-square fa-fw"></i>Tambah Fasilitas</a></button>
        </div>
        <!-- /.col-lg-12 -->
    </div>
     @include('partials.status-alert')
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Deskripsi</th>
                                <th>Tgl Update</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($fasilitas as $key => $value)
                            <tr>
                                <td>{{$value->name}}</td>
                                <td>{{$value->description}}</td>
                                <td>{{$value->updated_at}}</td>
                                <td>
                                    <div class="btn-group">
                                      <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
                                        <li><a href="#" data-target="#editmodel" data-toggle="modal" data-id="{{$value->id}}" data-nama="{{$value->name}}" data-desc="{{$value->description}}"><i class="fa fa-edit fa-fw" style="color:green";></i> Edit</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#" data-target="#hapusmodel" data-toggle="modal" data-id2="{{$value->id}}"><i class="fa fa-trash fa-fw" style="color:red";></i> Hapus</a></li>
                                      </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach                    
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

<div class="modal fade" id="model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Tambah Fasilitas</h4>
          </div>
          <div class="modal-body">
            <form id="form-change-password" data-toggle="validator"  method="POST" action="{{ url('admin/setting/iklan/fasilitas/input') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input name="name" class="form-control" placeholder="Masukkan nama fasilitas">
              </div>
              <br>
              <div class="form-group">
                <label for="exampleInputPassword1">Deskripsi</label>
                <textarea name="description" class="form-control" placeholder="Masukkan deskripsi fasilitas di sini"></textarea>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            {!! Form::button('<span class="middle"> Simpan</span>', array('class' => 'btn-dark blue','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
            </form>
          </div>
        </div>
      </div>
    </div>

<div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Edit Fasilitas</h4>
          </div>
          <div class="modal-body">
            <form id="form-change-password" data-toggle="validator"  method="POST" action="{{ url('admin/setting/iklan/fasilitas/edit') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input name="name" class="form-control" id="nama">
              </div>
              <br>
              <div class="form-group">
                <label for="exampleInputPassword1">Deskripsi</label>
                <input name="description" class="form-control" id="dc">
                <input type="hidden" name="id" class="form-control" id="idf">
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            {!! Form::button('<span class="middle"> Simpan</span>', array('class' => 'btn-dark blue','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
            </form>
          </div>
        </div>
      </div>
    </div>

<div class="modal fade" id="hapusmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Apakah Anda yakin menghapus fasilitas ini?</h4>
          </div>
          <div class="modal-body">
            <form id="form-change-password" data-toggle="validator"  method="POST" action="{{ url('admin/setting/iklan/fasilitas/hapus') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <input type="hidden" name="id" class="form-control" id="idfh">
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            {!! Form::button('<span class="middle"> Hapus</span>', array('class' => 'btn-dark blue','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
            </form>
          </div>
        </div>
      </div>
    </div>

<script>
    $('a[data-toggle=modal').click(function () {
     var nm = $(this).data('nama');
     var dc = $(this).data('desc');
     var idf = $(this).data('id');
     var idfh = $(this).data('id2');
     $("#idf").val( idf );
     $("#idfh").val( idfh );
     $("#nama").val( nm );
     $("#dc").val( dc );
});
</script>
@endsection