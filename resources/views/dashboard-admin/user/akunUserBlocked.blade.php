@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Akun User Terblokir</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    @include('partials.status-alert')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No HP</th>
                                <th>Pin BB</th>
                                <th>Saldo</th>
                                <th>Tgl buat</th>
                                <th>Block</th>
                            </tr>
                        </thead>
                        <tbody>
                             @foreach($user as $key => $value)
                            <tr class="odd gradeX">
                                <td>{{ $value->name }}</td>
                                <td> {{ $value->email }} </td>
                                <td> {{ $value->nohp }} </td>
                                <td> {{ $value->pinbb }} </td>
                                <td style="text-align:right">{{ $value->saldo }}</td>
                                <td class="center">{{ $value->created_at }}</td>
                                <td class="center">
                                        {!! Form::open(array('route' => 'user.unblocked', 'class' => 'form-button inline', 'method' => 'POST')) !!}
                                        {!! Form::hidden('user_id', $value->id) !!}
                                        {!! Form::button('<span class="middle"> Unblock </span>', 
                                                            array(  'class'             => 'btn btn-info',
                                                                    'style'             => 'padding: 4px 12px',
                                                                    'type'              => 'button', 
                                                                    'data-toggle'       => 'modal',
                                                                    'data-backdrop'     => 'static',
                                                                    'data-keyboard'     => 'false',
                                                                    //ini untuk detail modal
                                                                    'data-target'       => '#changeBlockedUser', 
                                                                    'data-body-title'   => 'Apakah Anda yakin untuk <strong>tidak lagi memblokir</strong> user ini?', 
                                                                    'data-body-content' => $value->name
                                                                )
                                                        ) 
                                        !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

@include('modals.modal-blocked-user')
@endsection

@section('footer')
    @include('scripts.modal-blocked-user')
@endsection