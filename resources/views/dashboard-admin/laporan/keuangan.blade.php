@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Laporan Keuangan</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">

        <div class="col-lg-3">
            <div class="dataTables_length" id="dataTables-example_length">
                    <select name="data" aria-controls="dataTables-example" class="form-control input-sm">
                        <option value="harian" >Harian</option>
                        <option value="bulanan" selected>Bulanan</option>
                        <option value="tahunan">Tahunan</option>
                    </select> 
            </div>
        </div>

        <div class="col-lg-5">
           <form action="action_page.php">
              Search:
              <input type="date" name="bday">
              <input type="submit">
            </form>
        </div>

        <div class="col-lg-12">
                    <div id="harian" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>

        <div class="col-lg-12">
                    <div id="bulanan" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>

        <div class="col-lg-12">
                    <div id="tahunan" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
        
    </div>
@endsection

