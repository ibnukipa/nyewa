@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Laporan Jumlah User</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">

        <div class="col-lg-3">
            <div class="dataTables_length" id="dataTables-example_length">
                    <select name="data" aria-controls="dataTables-example" class="form-control input-sm" onChange="selectjs()" id="selectid">
                        <option selected="true" disabled="disabled">Pilih...</option>
                        <option value="harian">Harian</option>
                        <option value="bulanan">Bulanan</option>
                        <option value="tahunan">Tahunan</option>
                    </select> 
            </div>
        </div>



       <div class="col-lg-12">
                    <div id="harian_jumlah"></div>

       </div>

        <div class="col-lg-12">
                    <div id="bulanan_jumlah"></div>
        </div>

        <div class="col-lg-12">
                    <div id="tahunan_jumlah"></div>
        </div>         
    </div>

<script>
    function selectjs() {
        if(document.getElementById('selectid').value == "harian") {
                $('#harian_jumlah').css('display','block');
                $('#bulanan_jumlah').css('display','none');
                $('#tahunan_jumlah').css('display','none');
        }
        else if(document.getElementById('selectid').value == "bulanan") {
                $('#harian_jumlah').css('display','none');
                $('#bulanan_jumlah').css('display','block');
                $('#tahunan_jumlah').css('display','none');
        }
        else if(document.getElementById('selectid').value == "tahunan") {
                $('#harian_jumlah').css('display','none');
                $('#bulanan_jumlah').css('display','none');
                $('#tahunan_jumlah').css('display','block');
        }
    };

    $(function () {
        $('#harian_jumlah').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Laporan Harian'
            },
            colors: ['#e65100'],
            xAxis: {
                categories: [
                    {!! json_encode($tanggal['hari1']) !!},
                    {!! json_encode($tanggal['hari2']) !!},
                    {!! json_encode($tanggal['hari3']) !!},
                    {!! json_encode($tanggal['hari4']) !!},
                    {!! json_encode($tanggal['hari5']) !!},
                    {!! json_encode($tanggal['hari6']) !!},
                    {!! json_encode($tanggal['hari7']) !!}
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'user'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} user </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: "Jumlah User",
                data: [
                    {!! json_encode($jumlah_hari['hari1']) !!},
                    {!! json_encode($jumlah_hari['hari2']) !!},
                    {!! json_encode($jumlah_hari['hari3']) !!},
                    {!! json_encode($jumlah_hari['hari4']) !!},
                    {!! json_encode($jumlah_hari['hari5']) !!},
                    {!! json_encode($jumlah_hari['hari6']) !!},
                    {!! json_encode($jumlah_hari['hari7']) !!},
                ]
            }]
        });
    });

$(function () {
        $('#bulanan_jumlah').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Laporan Bulanan Tahun '+{!! json_encode($jumlah_bulan['tahun']) !!}
            },
            colors: ['#e65100'],
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'user'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} user </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: "Jumlah User",
                data: [
                    {!! json_encode($jumlah_bulan['bulan1']) !!},
                    {!! json_encode($jumlah_bulan['bulan2']) !!},
                    {!! json_encode($jumlah_bulan['bulan3']) !!},
                    {!! json_encode($jumlah_bulan['bulan4']) !!},
                    {!! json_encode($jumlah_bulan['bulan5']) !!},
                    {!! json_encode($jumlah_bulan['bulan6']) !!},
                    {!! json_encode($jumlah_bulan['bulan7']) !!},
                    {!! json_encode($jumlah_bulan['bulan8']) !!},
                    {!! json_encode($jumlah_bulan['bulan9']) !!},
                    {!! json_encode($jumlah_bulan['bulan10']) !!},
                    {!! json_encode($jumlah_bulan['bulan11']) !!},
                    {!! json_encode($jumlah_bulan['bulan12']) !!},
                ]
            }]
        });
    });

$(function () {
        $('#tahunan_jumlah').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Laporan Tahunan'
            },
            colors: ['#e65100'],
            xAxis: {
                categories: [
                    {!! json_encode($tahun['tahun1']) !!},
                    {!! json_encode($tahun['tahun2']) !!},
                    {!! json_encode($tahun['tahun3']) !!},
                    {!! json_encode($tahun['tahun4']) !!},
                    {!! json_encode($tahun['tahun5']) !!}
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'user'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} user </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: "Jumlah User",
                data: [
                    {!! json_encode($jumlah_tahun['tahun1']) !!},
                    {!! json_encode($jumlah_tahun['tahun2']) !!},
                    {!! json_encode($jumlah_tahun['tahun3']) !!},
                    {!! json_encode($jumlah_tahun['tahun4']) !!},
                    {!! json_encode($jumlah_tahun['tahun5']) !!}
                ]
            }]
        });
    });
</script>
@endsection

