<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta property="og:image" content="{{ URL::asset('img/favicon.png') }}">
    <meta property="og:type" content="website">

    <link rel="icon" type="image/png" href="{{ URL::asset('img/favicon.png') }}">

    <title>Dashboard Nyewaja</title>
    <!-- style -->
     <!--<link rel="stylesheet" href="{{ URL::asset('css/Admin/bootstrap.min.css') }}" />-->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
     <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" />
     <link rel="stylesheet" href="{{ URL::asset('css/Admin/metisMenu.min.css') }}" />
     <link rel="stylesheet" href="{{ URL::asset('css/Admin/morris.css') }}" /> 
     <link rel="stylesheet" href="{{ URL::asset('css/Admin/sb-admin-2.css') }}" /> 
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
     <link rel="stylesheet" href="{{ URL::asset('css/Admin/dataTables.bootstrap.css') }}" /> 
     <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" />
      <script src="{{ URL::asset('js/Admin/jquery.min.js') }}"></script>
     <script src="//cdn.ckeditor.com/4.6.1/full/ckeditor.js"></script>
</head>

<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-defaultright navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a href="{{url('/')}}"> <img src="{{ URL::asset('img/logo.png') }}" alt="Logo sewaaja" style="padding-left:20px;padding-top:5px;height:45px"></a>
                <!-- <a class="navbar-brand" href="index.html">Nyewaja</a> -->
            </div>
            @include('dashboard-admin.partials.navbar_right')
            @include('dashboard-admin.partials.navbar_left')
        </nav>
        <!-- /#page-wrapper -->
        <div class="hide" role="dialog" aria-hidden="true" id="flash_notif" style="position: fixed; z-index: 1; text-align: center; left: 50%; transform: translateX(-50%);">
        <div class="modal-dialog no-margin">
            <div class="modal-content" style="border-radius: 6px">
                <div class="alert alert-{{ session()->get('flash_notif')['type'] }} no-margin" role="alert" style="background-color: #edf8e8">
                    {{ session()->get('flash_notif')['pesan'] }}
                </div>
            </div>
        </div>
    </div>
    @if(session()->get('flash_notif'))
        <script>
            $('#flash_notif').removeClass('hide');
            $('#flash_notif').addClass('animated fadeInDown');
            $('#flash_notif').modal({
                keyboard: false,
                backdrop: false,
                show: true
            });
            $('body.modal-open').css('overflow', 'inherit');
            setTimeout(function() {$('#flash_notif').addClass('fadeOutUp');}, 4000);
        </script>
    @endif
        <div id="page-wrapper">
            @yield('content')
        </div>
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ URL::asset('js/Admin/metisMenu.min.js') }}"></script>
    <script src="{{ URL::asset('js/Admin/sb-admin-2.js') }}"></script>
    <script src="{{ URL::asset('js/Admin/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('js/Admin/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/Admin/dataTables.responsive.js') }}"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://cdn.oesmith.co.uk/morris-0.4.1.min.js"></script>
    <script src="{{ URL::asset('js/Admin/bootstrap.min.js') }}"></script>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Ubah Password</h4>
          </div>
          <div class="modal-body">
            <form id="form-change-password" data-toggle="validator"  method="POST" action="{{ url('admin/ubah_password') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="exampleInputEmail1">Password Lama</label>
                <input type="password" name="old" class="form-control" id="exampleInputEmail1" placeholder="Masukkan password lama di sini">
              </div>
              <br>
              <div class="form-group">
                <label for="exampleInputPassword1">Password Baru</label>
                <input type="password" name="new" class="form-control" id="exampleInputPassword1" placeholder="Masukkan password baru di sini">
              </div>
              <br>
              <div class="form-group">
                <label for="exampleInputPassword1">Konfirmasi Password Baru</label>
                <input type="password" name="confirm" class="form-control" id="exampleInputPassword1" placeholder="Masukkan konfirmasi password baru di sini">
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            {!! Form::button('<span class="middle"> Simpan</span>', array('class' => 'btn-dark blue','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
            </form>
          </div>
        </div>
      </div>
    </div>

  @yield('footer')
</body>

</html>
