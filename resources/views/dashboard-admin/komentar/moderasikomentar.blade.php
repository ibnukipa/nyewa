@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Moderasi Komentar</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>User</th>
                                <th>Judul Iklan</th>
                                <th>Komentar</th>
                                <th>Verifikasi</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach($moderasi as $key => $value)
                            <tr>
                                <td>{{ $value->created_at }}</td>
                                <td>{{ $value->user->name }}</td>
                                <td>{{ $value->iklan->name }}</td>
                                <td> {{ $value->feedback_content }}</td>
                                <td class="center"><button type="button" class="btn btn-danger">Hapus</button></td>
                            </tr>
                            @endforeach 
                            
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection