@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Edit Halaman</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    @include('partials.status-alert')
    @foreach($page as $key => $value)
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form id="form-halaman" data-toggle="validator"  method="POST" action="{{ url('admin/setting/halaman/update') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {!! Form::hidden('page_id', $value->id) !!}
                        <h4>Judul</h4>
                        <input type="text" value="{{$value->title}}" class="form-control" placeholder="Masukkan judul di sini" aria-describedby="basic-addon1" name="judul">
                        <br>
                        <div style="width:30%">
                        <h4>Sub Halaman</h4>
                        {!! Form::select('sub', array(1 => 'Sewaja', 2=> 'Pengguna') , $value->sub, array( 'class' =>'form-control', 'id' => 'sub', 'required' => 'required')) !!}
                        <!-- <select id="select" class="form-control" name="sub" value="{{$value->sub}}">
                            <option value="1">SEWAJA</option>
                            <option value="2">PENGGUNA</option>
                        </select> -->
                        </div>
                        <br>
                        <textarea class="form-control" name="datainput">{!! $value->content !!}</textarea>
                        <br>
                        <div style="text-align:right">
                            {!! Form::button('<span class="middle"> Terbitkan</span>', array('class' => 'btn-dark blue','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
                        </div>
                    </form>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    @endforeach
@endsection

@section('footer')
    <script>CKEDITOR.replace('datainput');</script>
@endsection