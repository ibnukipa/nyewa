@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Halaman Baru</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    @include('partials.status-alert')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <form id="form-halaman" data-toggle="validator"  method="POST" action="{{ url('admin/setting/halaman/baru/terbitkan') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <h4>Judul</h4>
                        <input type="text" class="form-control" placeholder="Masukkan judul di sini" aria-describedby="basic-addon1" name="judul">
                        <br>
                        <div style="width:30%">
                        <h4>Sub Halaman</h4>
                        <select id="select" class="form-control" name="sub">
                            <option value="1">SEWAJA</option>
                            <option value="2">PENGGUNA</option>
                        </select>
                        </div>
                        <br>
                        <textarea class="form-control" name="datainput"></textarea>
                        <br>
                        <div style="text-align:right">
                            {!! Form::button('<span class="middle"> Terbitkan</span>', array('class' => 'btn-dark blue','type' => 'submit', 'style' => 'padding: 4px 12px')) !!}
                        </div>
                    </form>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection

@section('footer')
    <script>CKEDITOR.replace('datainput');</script>
@endsection