@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">List Halaman</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
     @include('partials.status-alert')
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Sub</th>
                                <th>Judul</th>
                                <th>Tgl Update</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($page as $key => $value)
                            <tr>
                                @if($value->sub == 1)
                                    <td>Sewaja</td>
                                @elseif($value->sub == 2)
                                    <td>Pengguna</td>
                                @else <td></td>
                                @endif
                                <td>{{ $value->title }}</td>
                                <td>{{ $value->updated_at }}</td>
                                <td>
                                    <div class="btn-group">
                                      <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
                                        <li><a href="{{ url('/hc/'.$value->url) }}"><i class="fa fa-eye fa-fw" style="color:blue";></i> Lihat</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="{{ url('admin/setting/halaman/edit/'.$value->id) }}"><i class="fa fa-edit fa-fw" style="color:green";></i> Edit</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="{{ url('admin/iklan/ditolak/'.$value->id) }}"><i class="fa fa-trash fa-fw" style="color:red";></i> Hapus</a></li>
                                      </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach                    
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection