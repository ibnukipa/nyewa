@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Detail Iklan</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                   <div class="form-group">
                        <label for="disabledSelect">Judul Iklan</label>
                        <input class="form-control" id="disabledInput" type="text" placeholder="Mobil mewah" disabled>
                    </div>

                     <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea class="form-control" rows="3" disabled></textarea>
                    </div>

                    <div class="form-group">
                        <label for="disabledSelect">Kategori</label>
                        <input class="form-control" id="disabledInput" type="text" placeholder="Mobil mewah" disabled>
                    </div>

                     <div class="form-group" style="padding-left:30px;">
                        <label for="disabledSelect">Sub Kategori</label>
                        <input class="form-control" id="disabledInput" type="text" placeholder="Mobil mewah" disabled>
                    </div>
                    <label for="disabledSelect">Foto</label>
                    <br>
                    <div class="row">
                        <div class="col-lg-3">
                         <img src="{{ URL::asset('img/sa1.jpg') }}" alt="Logo sewaaja" style="padding:3px;padding-left:20px;height:300px">
                        </div>

                        <div class="col-lg-3">
                         <img src="{{ URL::asset('img/sa1.jpg') }}" alt="Logo sewaaja" style="padding:3px;padding-left:20px;height:300px">
                        </div>
                    </div>        
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection