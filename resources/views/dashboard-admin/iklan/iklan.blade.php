@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<!-- ini alert -->
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Daftar Iklan</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    @include('partials.status-alert')
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Kategori</th>
                                <th>Judul</th>
                                <th>Tgl dibuat</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listIklan as $key => $value)
                            <tr>
                                <td>{{ $value->user->name }}</td>
                                <td>{{ $value->kategori->name }}</td>
                                <td>{{ $value-> name }}</td>
                                <td class="center">{{ $value->created_at }}</td>
                                <td>
                                    <div class="btn-group">
                                      <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
                                        <li><a href="{{ url('/iklan/detail/'.$value->url_iklan.'.html') }}"><i class="fa fa-eye fa-fw" style="color:blue";></i> Lihat</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li>
                                            {!! Form::open(array('route' => 'iklan.ditolak', 'class' => 'form-button inline', 'method' => 'POST')) !!}
                                            {!! Form::hidden('getIklan', $value->id) !!}
                                                <a style="    display: block;
                                                    padding: 3px 20px;
                                                    clear: both;
                                                    font-weight: 400;
                                                    line-height: 1.42857143;
                                                    padding: 10px 20px;
                                                    color: #333;
                                                    white-space: nowrap;" 
                                                    data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#changeBlockedUser" data-body-title="Apakah Anda yakin untuk <strong>memblokir</strong> iklan ini?" data-body-content={{$value->name}}>
                                                <i class="fa fa-ban fa-fw" style="color:red";></i> Block
                                                </a>
                                            {!! Form::close() !!}
                                        </li>
                                      </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach                            
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@include('modals.modal-blocked-user')
@endsection

@section('footer')
    @include('scripts.modal-blocked-user')
@endsection