@extends('dashboard-admin.layouts.dashboard_admin')

@section('content')
<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Daftar Iklan Terblokir</h2>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    @include('partials.status-alert')
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Kategori</th>
                                <th>Judul</th>
                                <th>Tgl dibuat</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listIklan as $key => $value)
                            <tr>
                                <td>{{ $value->user->name }}</td>
                                <td>{{ $value->kategori->name }}</td>
                                <td>{{ $value-> name }}</td>
                                <td class="center">{{ $value->created_at }}</td>
                                <td>
                                    <div class="btn-group">
                                      <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
                                        <li><a href="{{ url('/iklan/detail/'.$value->url_iklan.'.html') }}"><i class="fa fa-eye fa-fw" style="color:blue";></i> Lihat</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="{{ url('admin/iklan/diterima/'.$value->id) }}"><i class="fa fa-check fa-fw" style="color:green";></i> Terima</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#"><i class="fa fa-trash-o fa-fw" style="color:red";></i> Hapus</a></li>
                                      </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach                    
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection