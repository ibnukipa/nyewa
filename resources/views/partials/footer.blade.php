<footer class="footer-wrapper">
    <div class="footer-main">
        <div class="container">
            <div class="row">
            <?php $page = \DB::table('halaman')->get(); ?>
                <!--<div class="col-md-2 center">
                    <img src="{{ URL::asset('img/favicon-grey.png') }}" alt="" style="width: 150px;">
                </div>-->
                <div class="col-md-4">
                    <ul>
                        <h5>NyewAJa.com</h5>
                        @foreach($page as $key => $value)
                        @if($value->sub==1)
                        <li>
                            <a href="{{ url('/hc/'.$value->url) }}">{{$value->title}}</a>
                        </li>
                        @endif
                        @endforeach
                        <!-- <li>
                            <a href="#">Pusat Bantuan</a>
                        </li>
                        <li>
                            <a href="#">Syarat & Ketentuan</a>
                        </li>
                        <li>
                            <a href="#">Kebijakan Privasi</a>
                        </li> -->
                        
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul>
                        <h5>PENGGUNA</h5>
                        @foreach($page as $key => $value)
                        @if($value->sub==2)
                        <li>
                            <a href="{{ url('/hc/'.$value->url) }}">{{$value->title}}</a>
                        </li>
                        @endif
                        @endforeach
                        <!-- <li>
                            <a href="#">Tips Menyewakan yang Aman</a>
                        </li>
                        <li>
                            <a href="#">Tips Menyewa yang Aman</a>
                        </li> -->
                    </ul>
                </div>
                <!--<div class="col-md-4">
                    <div class="panel panel-sewaaja">
                        <div class="panel-body">
                            <img src="{{ URL::asset('img/logo.png') }}" alt="" style="width: 100%">
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>

    <p class="no-margin" style="padding: 1rem 3rem; border-top: 1px solid rgba(0, 0, 0, .2); color: #314973">
        2016 © NyewAja.com
    </p>
</footer>