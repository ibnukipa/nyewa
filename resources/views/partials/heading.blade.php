<div class="container" @if (trim($__env->yieldContent('template_title')))@yield('style_container') | @endif >
    <div class="heading">

        <div class="heading_head">

            <!--User Action-->
            <a href="{{ url('/iklan/create') }}" class="no-padding pull-right">
                {!! Form::button('<i class="material-icons middle">add</i> <span class="middle"> Pasang Iklan</span>', array('class' => 'btn-dark red','type' => '', 'style' => 'padding: 4px 12px')) !!}
            </a>
            
            @if(Auth::guest())
            <a href="{{ url('/login') }}" class="no-padding pull-right">
                {!! Form::button('<i class="material-icons middle">exit_to_app</i> <span class="middle"> Login</span>', array('class' => 'btn grey-light','type' => '', 'style' => 'padding: 4px 12px')) !!}
            </a>
            @else
            <div class="dropdown pull-right" style="line-height: 7rem; padding: 0">
                {!! Form::button('<i class="material-icons middle">person</i>', array('class' => 'btn-dark red-darken dropdown-toggle', 'data-toggle' => 'dropdown' ,'type' => 'button', 'style' => 'padding: 4px 12px')) !!}
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                    <li class="dropdown-header">Hai, <div class="name">{{ Auth::user()->name }}</div></li>
                    <li class="dropdown-header" style="padding-top: 0">Saldo Anda <div class="name">Rp. {!! number_format((Auth::user()->saldo), 0, ",", ".") !!}</div></li>
                    <li role="separator" class="divider"></li>

                    <li>
                        <a href="{{ url('/dashboard') }}">
                            <i class="material-icons middle">dashboard</i> <span class="middle"> Dashboard</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('/iklan/aktif') }}">
                            <i class="material-icons middle">receipt</i> <span class="middle"> Iklan</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('/pesan/masuk') }}">
                            <i class="material-icons middle">mail</i> <span class="middle"> Pesan</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('/dompet/success') }}">
                            <i class="material-icons middle">account_balance_wallet</i> <span class="middle"> Dompet</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('/setting/kontak') }}">
                            <i class="material-icons middle">settings</i> <span class="middle"> Pengaturan</span>
                        </a>
                    </li>

                    <li role="separator" class="divider"></li>

                    <li class="danger">
                        <a href="{{ url('logout') }}">
                            <i class="material-icons middle">power_settings_new</i> <span class="middle"> Keluar</span>
                        </a>
                    </li>
                </ul>
            </div>
            @endif

            <!--Call Center-->
            <!--<a class="no-padding pull-right">
                {!! Form::button('<i class="material-icons middle">call</i> <span class="middle"> +62 857 6886 4959</span>', array('class' => 'btn white','type' => '', 'style' => '')) !!}
            </a>-->

            <div>
                <a href="{{ url('/') }}" class="head_logo">
                    <img src="{{ URL::asset('img/logo-dark.png') }}" alt="Logo sewaaja" style="height: 100%">
                </a>
            </div>
            
            
        </div>

    </div>
</div>