<nav id="nav-top" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button class="navbar-toggle collapsed btn blue-light no-padding" type="button" style="width: 4rem; border: none" data-toggle="collapse" data-target="#app-navbar-collapse">
                <i class="material-icons" style="line-height: inherit; font-size: 23px;">menu</i>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                <img height="100%" src="{{ URL::asset('img/logo.png') }}" alt="">
            </a>

        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
        
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li>
                    <!--<a>No. 1</a>-->
                </li>
            </ul>

            <!--Pencarian-->
            {{-- <form class="navbar-form navbar-left" style="margin-top: 10px">
                <div class="form-group has-feedback">
                    <input class="form-control search-top" placeholder="Semua provinsi" type="text">
                    <span class="form-control-feedback search-top" aria-hidden="true">
                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">place</i>
                    </span>
                    <span class="form-control-feedback search-top" aria-hidden="true" style="right: 0; left: inherit">
                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">arrow_drop_down</i>
                    </span>
                </div>

                <div class="form-group has-feedback ">
                    <input class="form-control search-top" placeholder="Semua kategori" type="text">
                    <span class="form-control-feedback search-top" aria-hidden="true">
                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">widgets</i>
                    </span>
                    <span class="form-control-feedback search-top" aria-hidden="true" style="right: 0; left: inherit">
                        <i class="material-icons" style="line-height: inherit; font-size: 23px;">arrow_drop_down</i>
                    </span>
                </div>

                <div class="input-group">
                    <input class="form-control search-top" placeholder="Kata pencarian" type="text">
                    <span class="input-group-btn" >
                        <button class="btn blue-light no-padding" type="submit" style="width: 4rem;">
                            <i class="material-icons" style="line-height: inherit; font-size: 23px;">search</i>
                        </button>
                    </span>
                </div>
            </form> --}}

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">

                    @if(Auth::guest())
                    <li>
                        <a href="{{ url('/login') }}" class="no-padding">
                            {!! Form::button('<i class="material-icons middle">exit_to_app</i> <span class="middle"> Masuk</span>', array('class' => 'btn white','type' => '', 'style' => 'padding: 4px 12px')) !!}
                        </a>
                    </li>
                    @else
                    <li >
                        <div class="dropdown">
                            {!! Form::button('<i class="material-icons middle">person</i>', array('class' => 'btn white dropdown-toggle', 'data-toggle' => 'dropdown' ,'type' => 'button', 'style' => 'padding: 4px 12px')) !!}
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">

                                <li class="dropdown-header">Hai, <div class="name">{{ Auth::user()->name }}</div></li>
                                <li class="dropdown-header" style="padding-top: 0">Saldo Anda <div class="name">Rp. {!! number_format((Auth::user()->saldo), 0, ",", ".") !!}</div></li>
                                <li role="separator" class="divider"></li>

                                <li>
                                    <a href="{{ url('/dashboard') }}">
                                        <i class="material-icons middle">dashboard</i> <span class="middle"> Dashboard</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ url('/iklan/aktif') }}">
                                        <i class="material-icons middle">receipt</i> <span class="middle"> Iklan</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ url('/pesan/masuk') }}">
                                        <i class="material-icons middle">mail</i> <span class="middle"> Pesan</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ url('/dompet/success') }}">
                                        <i class="material-icons middle">account_balance_wallet</i> <span class="middle"> Dompet</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ url('/setting/kontak') }}">
                                        <i class="material-icons middle">settings</i> <span class="middle"> Pengaturan</span>
                                    </a>
                                </li>

                                <li role="separator" class="divider"></li>

                                <li class="danger">
                                    <a href="{{ url('logout') }}" >
                                        <i class="material-icons middle">power_settings_new</i> <span class="middle"> Keluar</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    @endif
                    <li >
                        <a href="{{ url('/iklan/create') }}" class="no-padding">
                            {!! Form::button('<i class="material-icons middle">add</i> <span class="middle"> Pasang Iklan</span>', array('class' => 'btn-dark red','type' => '', 'style' => 'padding: 4px 12px')) !!}
                        </a>
                    </li>
            </ul>
        </div>
    </div>
</nav>