{{ csrf_field() }}
<form role="form" data-toggle="validator" class="search not-valid" method="GET" action="{{ url('/cari') }}">
    <div class="row inner-width">
        <div class="col-md-3 no-padding inner-width ">
            <div class="form-group has-feedback">
                <label for="location">Pilih provinsi dan kota</label>
                {!! Form::select('provinsi', array(null => null) , null, array( 'class' =>'js-example-basic-single js-states form-control search-panel', 'id' => 'provinsi')) !!}
                <!--<input id="location" class="form-control search-panel" placeholder="Semua provinsi" type="text">-->
                <span class="form-control-feedback search-panel" aria-hidden="true">
                    <i class="material-icons" style="line-height: 37px; font-size: 23px;">place</i>
                </span>
                <!--<span class="form-control-feedback search-panel" aria-hidden="true" style="right: 0; left: inherit">
                    <i class="material-icons" style="line-height: inherit; font-size: 23px;">arrow_drop_down</i>
                </span>-->
            </div>
        </div>
        <div class="col-md-3 no-padding inner-width ">
        <div class="form-group has-feedback ">
            <label for="category">Pilih kategori</span></label>
            {!! Form::select('kategori', array(null => null) , null, array( 'class' =>'js-example-basic-single js-states form-control search-panel', 'id' => 'kategori')) !!}
            <!--<input id="category" class="form-control search-panel" placeholder="Semua kategori" type="text">-->
            <span class="form-control-feedback search-panel" aria-hidden="true">
                <i class="material-icons" style="line-height: 37px; font-size: 23px;">widgets</i>
            </span>
            <!--<span class="form-control-feedback search-panel" aria-hidden="true" style="right: 0; left: inherit">
                <i class="material-icons" style="line-height: inherit; font-size: 23px;">arrow_drop_down</i>
            </span>-->
        </div>
        </div>
        <div class="col-md-6 no-padding inner-width ">
        <div class="input-group">
            <label for="keyword">Masukkan kata pencarian</label>
            <input value="{{ isset($keyword) ? $keyword : null }}" id="keyword" name="keyword" class="form-control search-panel" placeholder="Kata pencarian" type="text" style="border: 1px solid rgba(27,54,100, 1);border-bottom-left-radius: 4px; border-top-left-radius: 4px; height: 37px">
            <span class="input-group-btn" style="top: 13px; ">
                <button class="btn red-light no-padding" type="submit" style="width: 4rem; margin-left: 0">
                    <i class="material-icons" style="line-height: 33px; font-size: 23px;">search</i>
                </button>
            </span>
        </div>
        </div>
    </div>
</form>
<script>
var place    = [
    {
        id: '0',
        text: 'Semua Provinsi',
    },
    @foreach (Indonesia::allProvinces() as $provinsi)
        {
            text: '{{ $provinsi->name }}',
            children: [
                {
                    id: '{{$provinsi->id}}',
                    text: 'Semua {{$provinsi->name}}'
                },
                @foreach (Indonesia::findProvince($provinsi->id, ['cities'])->cities as $value)
                    { id: '{{ $value->id }}', text: unEntity('{!! $value->name !!}') },
                @endforeach
            ]
        },
    @endforeach
];

var kategori    = [
    {
        id: '0',
        text: 'Semua Kategori',
    },
    @foreach (DB::table('kategori_iklan')->where('level', '=', 1)->get() as $kategori)
        {
            text: '{!! $kategori->name !!}',
            children: [
                {
                    id: '{{$kategori->id}}',
                    text: 'Semua {!!$kategori->name!!}'
                },
                @foreach (DB::table('kategori_iklan')->where('level', '=', 2)->where('parent_id', '=', $kategori->id)->get() as $value)
                    { id: '{{ $value->id }}', text: unEntity('{!! $value->name !!}') },
                @endforeach
            ]
        },
    @endforeach
];

function unEntity(str){
    return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
}

$('#provinsi').select2({
    data: place,
    placeholder: 'Cari provinsi Anda...'
});

$('#kategori').select2({
    data: kategori,
    placeholder: 'Pilih kategori...'
});
@if(isset($selected_place_id))
$('#provinsi').val({{$selected_place_id}}).trigger('change');
@else
$('#provinsi').val(0).trigger('change');
@endif

@if(isset($selected_kategori_id))
$('#kategori').val({{$selected_kategori_id}}).trigger('change');
@else
$('#kategori').val(0).trigger('change');
@endif
</script>