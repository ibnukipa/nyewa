@if(session()->get('status'))
<div class="alert alert-{!! session()->get('status')['status'] !!} alert-dismissible fade in mr-top1" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {!! session()->get('status')['content'] !!}
</div>
@endif

@if(count($errors) > 0)
<div class="alert alert-danger alert-dismissible fade in mr-top1" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    @foreach($errors->all() as $error)
        {{ $error }}
        <br>
    @endforeach
</div>
@endif