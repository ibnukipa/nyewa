@extends('layouts.app')

@section('nav')
    @include('partials.nav')
@endsection

@section('content')
<div class="container">
    @include('partials.search')
    <div class="row" style="margin-top: 3rem">
        <div class="col-md-8">
            <div class="quicksearch">
                <div class="row">
                    @foreach($listKategori as $value)
                        <div class="col-md-3">
                            <?php $urlValue = strtolower(str_replace('--', '-', preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ','-',$value->name)))) ?>
                            <a href="{{ url('/'.$urlValue.'/cari') }}?provinsi=0&kategori={{$value->id}}&keyword=" style="display: block">
                                <img src="{{ URL::asset($value->path_icon) }}" alt="" style="height: 150px;">
                                <button class="btn grey-light" type="button">
                                    {{ $value->name }}
                                </button>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <img src="{{ URL::asset('img/slogan_banner.png') }}" alt="" style="width: 100%">
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('partials.footer')
@endsection
