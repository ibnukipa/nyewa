@extends('layouts.app')

@section('template_title')
    Iklan
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="pageinfo">
                <h4>
                    Iklan
                </h4>
                <p>
                    Iklan Anda - Anda dapat mengelolanya di sini
                </p>
            </div>
            @include('dashboard-user.nav')

            <div class="panel panel-sewaaja dashboard">
                <div class="panel-heading">
                    <ul>
                        <li @if(isset($iklanState) && $iklanState == 'aktif') class="active" @endif>
                            <a @if($iklanState != 'aktif') href="{{ url('/iklan/aktif') }}" @endif >
                                <span class="middle"> Aktif ({!! Auth::user()->iklan()->where('status', '=', 'approved')->count() !!})</span>
                            </a>
                        </li>
                        <li @if(isset($iklanState) && $iklanState == 'moderasi') class="active" @endif>
                            <a @if($iklanState != 'moderasi') href="{{ url('/iklan/moderasi') }}" @endif >
                                <span class="middle"> Moderasi ({!! Auth::user()->iklan()->where('status', '=', 'new')->orWhere('status', '=', 'edited')->count() !!})</span>
                            </a>
                        </li>
                        <li @if(isset($iklanState) && $iklanState == 'gagal') class="active" @endif>
                            <a @if($iklanState != 'gagal') href="{{ url('/iklan/gagal') }}" @endif >
                                <span class="middle"> Gagal ({!! Auth::user()->iklan()->where('status', '=', 'blocked')->count() !!})</span>
                            </a>
                        </li>

                        <li @if(isset($iklanState) && $iklanState == 'nonaktif') class="active" @endif>
                            <a @if($iklanState != 'nonaktif') href="{{ url('/iklan/nonaktif') }}" @endif >
                                <span class="middle"> Non-Aktif ({!! Auth::user()->iklan()->where('status', '=', 'nonaktif')->count() !!})</span>
                            </a>
                        </li>
                    </ul>
                </div>
                
                <div class="panel-body no-padding">
                    <div class="content">
                        <div class="body no-padding" style="padding-top: 2rem">
                            @include('partials.status-alert')
                            @if(count($listIklan) > 0)
                                @include('iklan.'.$iklanState)
                            @else
                            <div class="blank-component">
                                <i class="material-icons">receipt</i>
                                <p>Anda tidak memiliki iklan {{ $iklanState }} saat ini.</p>
                                <p>
                                    <a href="{{ route('iklan.create') }}" class="no-padding">
                                        {!! Form::button('<i class="material-icons middle">add</i> <span class="middle"> Pasang Iklan</span>', array('class' => 'btn-dark blue','type' => '', 'style' => 'padding: 4px 12px')) !!}
                                    </a>
                                </p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('modals.modal-hapus-iklan')
@include('modals.modal-change-iklan')
@include('modals.modal-promote-iklan')
@endsection

@section('footer')
    @include('scripts.modal-hapus-iklan')
    @include('scripts.modal-change-iklan')
    @include('scripts.modal-promote-iklan')
    @include('partials.footer')
@endsection
