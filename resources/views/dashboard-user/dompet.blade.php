@extends('layouts.app')

@section('template_title')
    Dompet
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="pageinfo">
                <h4>
                    Dompet
                </h4>
                <p>
                    Saldo dan riwayat semua transaksi pembayaran Top Up Anda
                </p>
            </div>
            @include('dashboard-user.nav')

            <div class="panel panel-sewaaja dashboard">
                <div class="panel-heading" style="padding: 2rem 2rem; line-height: 3rem; min-height: 90px; max-height: 90px;">
                    <span class="left" style="font-weight: 600; font-size: 3rem; font-family: 'Roboto'">
                        <p style="font-size: 1.2rem; font-weight: 400; line-height: initial; margin: 0">Saldo Anda</p>
                        Rp. {!! number_format((Auth::user()->saldo), 0, ",", ".") !!}
                    </span>
                    <span class="pull-right" style="margin-top: -.6rem">
                        <a href="{{ url('#') }}" class="no-padding">
                            {!! Form::button('<i class="material-icons middle">monetization_on</i> <span class="middle"> Top Up Saldo</span>', array('class' => 'btn-dark green','type' => '', 'style' => 'padding: 4px 12px')) !!}
                        </a>
                    </span>
                </div>

                <div class="panel-heading">
                    <p style="font-size: 1.2rem; font-weight: 400; line-height: initial; margin: 0 0 0 2rem">Riwayat Transaksi Pembayaran</p>
                    <ul>
                        <li @if(isset($iklanState) && $iklanState == 'success') class="active" @endif>
                            <a @if($iklanState != 'success') href="{{ url('/dompet/success') }}" @endif >
                                <span class="middle"> Success (0)</span>
                            </a>
                        </li>
                        <li @if(isset($iklanState) && $iklanState == 'invalid') class="active" @endif>
                            <a @if($iklanState != 'invalid') href="{{ url('/dompet/invalid') }}" @endif >
                                <span class="middle"> Invalid (0)</span>
                            </a>
                        </li>
                        <li @if(isset($iklanState) && $iklanState == 'pending') class="active" @endif>
                            <a @if($iklanState != 'pending') href="{{ url('/dompet/pending') }}" @endif >
                                <span class="middle"> Pending (0)</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body no-padding">
                    <div class="content">
                        <div class="heading">
                            Transaksi {!! $iklanState !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('partials.footer')
@endsection
