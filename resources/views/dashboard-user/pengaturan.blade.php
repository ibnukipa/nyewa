@extends('layouts.app')

@section('template_title')
    Pengaturan
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="pageinfo">
                <h4>
                    Pengaturan Akun
                </h4>
                <p>
                    Anda dapat mengubah pengaturan yang terkait dengan akun Anda di sini
                </p>
            </div>
            @include('dashboard-user.nav')

            <div class="panel panel-sewaaja dashboard">
                <div class="panel-heading" style="padding: 2rem 2rem; line-height: 3rem; min-height: 90px; max-height: 90px;">
                    <span class="left " style="font-weight: 600; font-size: 2rem; font-family: 'Roboto'">
                        <p style="font-size: 1.2rem; font-weight: 400; line-height: initial; margin: 0">Email Anda</p>
                        {{ Auth::user()->email }}
                    </span>
                    <span class="pull-right" style="margin-top: -.6rem">
                        bergabung sejak {{ Auth::user()->created_at->format('d') }}
                        <?php $time = new Time ?> 
                        {!! $time->bulan[Auth::user()->created_at->format('n')] !!}
                        {{ Auth::user()->created_at->format('Y') }}
                    </span>
                </div>

                <div class="panel-heading">
                    <ul>
                        <li @if(isset($iklanState) && $iklanState == 'kontak') class="active" @endif>
                            <a @if($iklanState != 'kontak') href="{{ url('/setting/kontak') }}" @endif >
                                <span class="middle"> Informasi Kontak</span>
                            </a>
                        </li>
                        <li @if(isset($iklanState) && $iklanState == 'password') class="active" @endif>
                            <a @if($iklanState != 'password') href="{{ url('/setting/password') }}" @endif >
                                <span class="middle"> Ganti Password</span>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="panel-body no-padding">
                    <div class="content">
                        <div class="heading">
                            Pengaturan {!! $iklanState !!}
                        </div>
                        <div class="body">
                            @if($iklanState == 'kontak')
                                @include('form.setting_kontak')
                            @elseif($iklanState == 'password')
                                @include('form.setting_password')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    @include('partials.footer')
@endsection
