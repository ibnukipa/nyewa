<ul class="nav nav-tabs dashboard">
    <li role="presentation" @if(isset($iklan) && $iklan) class="active" @endif>
        <a @if(!isset($iklan)) href="{{ url('/iklan/aktif') }}" @endif >
            <i class="material-icons middle">receipt</i> <span class="middle"> Iklan</span>
        </a>
    </li>
    <li role="presentation" @if(isset($pesan) && $pesan) class="active" @endif>
        <a @if(!isset($pesan)) href="{{ url('/pesan/masuk') }}" @endif >
            <i class="material-icons middle">mail</i> <span class="middle"> Pesan @if(Auth::user()->pesan_masuk()->where('seen', '=', 0)->count() > 0) <span class="badge red">{!! Auth::user()->pesan_masuk()->where('seen', '=', 0)->count() !!}</span> @endif</span>
        </a>
    </li>
    <li role="presentation" @if(isset($dompet) && $dompet) class="active" @endif>
        <a @if(!isset($dompet)) href="{{ url('/dompet/success') }}" @endif >
            <i class="material-icons middle">account_balance_wallet</i> <span class="middle"> Dompet</span>
        </a>
    </li>
    <li role="presentation" @if(isset($setting) && $setting) class="active" @endif>
        <a @if(!isset($setting)) href="{{ url('/setting/kontak') }}" @endif >
            <i class="material-icons middle">settings</i> <span class="middle"> Pengaturan</span>
        </a>
    </li>
</ul>