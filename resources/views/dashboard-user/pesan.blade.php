@extends('layouts.app')

@section('template_title')
    Pesan
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="pageinfo">
                <h4>
                    Pesan
                </h4>
                <p>
                    Pesan untuk iklan Anda dan pesan untuk iklan lain
                </p>
            </div>
            @include('dashboard-user.nav')

            <div class="panel panel-sewaaja dashboard">
                <div class="panel-heading">
                    <ul>
                        <li @if(isset($iklanState) && $iklanState == 'masuk') class="active" @endif>
                            <a @if($iklanState != 'masuk') href="{{ url('/pesan/masuk') }}" @endif >
                                <span class="middle"> Pesan Iklan Anda
                                @if(count($listPesan) > 0 || count($opositeListPesan) > 0)
                                    @if(count($listPesan) > 0)
                                        @if($listPesan[0]->iklan->user == Auth::user())
                                            <?php $tempCount = count(Auth::user()->pesan_masuk()->whereIn('iklan_id', $listPesan->lists('iklan_id'))->where('seen', '=', 0)->get()); ?>
                                            @if($tempCount>0)
                                                <span class="badge red">
                                                    {{$tempCount}}
                                                </span>
                                            @endif
                                        @endif
                                    @elseif(count($opositeListPesan) > 0)
                                        @if($opositeListPesan->first()->iklan->user == Auth::user())
                                            <?php $tempCount = count(Auth::user()->pesan_masuk()->whereIn('iklan_id', $opositeListPesan->lists('iklan_id'))->where('seen', '=', 0)->get()); ?>
                                            @if($tempCount>0)
                                                <span class="badge red">
                                                    {{$tempCount}}
                                                </span>
                                            @endif
                                        @endif
                                    @endif
                                @endif
                                </span>
                            </a>
                        </li>
                        <li @if(isset($iklanState) && $iklanState == 'keluar') class="active" @endif>
                            <a @if($iklanState != 'keluar') href="{{ url('/pesan/keluar') }}" @endif >
                                <span class="middle"> Pesan Iklan Orang Lain
                                @if(count($listPesan) > 0 || count($opositeListPesan) > 0)
                                    @if(count($listPesan) > 0)
                                        @if($listPesan[0]->iklan->user != Auth::user())
                                            <?php $tempCount = count(Auth::user()->pesan_masuk()->whereIn('iklan_id', $listPesan->lists('iklan_id'))->where('seen', '=', 0)->get()); ?>
                                            @if($tempCount>0)
                                                <span class="badge red">
                                                    {{$tempCount}}
                                                </span>
                                            @endif
                                        @endif
                                    @elseif(count($opositeListPesan) > 0)
                                        @if($opositeListPesan->first()->iklan->user != Auth::user())
                                            <?php $tempCount = count(Auth::user()->pesan_masuk()->whereIn('iklan_id', $opositeListPesan->lists('iklan_id'))->where('seen', '=', 0)->get()); ?>
                                            @if($tempCount>0)
                                                <span class="badge red">
                                                    {{$tempCount}}
                                                </span>
                                            @endif
                                        @endif
                                    @endif
                                @endif
                                
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body no-padding">
                    <div class="content">
                    </div>
                    <div class="content">
                        <div class="body no-padding">
                            @include('partials.status-alert')
                            
                            @if((($iklanState == 'masuk') || ($iklanState == 'keluar')) && count($listPesan) > 0)
                                @include('pesan.'.$iklanState)
                            @else
                            <div class="blank-component">
                                <i class="material-icons">email</i>
                                <p>Anda tidak memiliki pesan {{ $iklanState }}.</p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('modals.modal-pesan-detail')
@endsection

@section('footer')
    @include('scripts.modal-pesan-detail')
    @include('partials.footer')
@endsection
