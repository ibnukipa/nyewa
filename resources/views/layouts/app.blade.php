<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="">
    <meta property="og:url" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="{{ URL::asset('img/favicon.png') }}">
    <meta property="og:type" content="website">

    <link rel="icon" type="image/png" href="{{ URL::asset('img/favicon.png') }}">

    <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif NyewAja</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    @yield('template_linked_fonts')

    <!-- Styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/reset.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ URL::asset('css/build.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.filer.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/themes/jquery.filer-dragdropbox-theme.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('Slider/flexslider.css') }}" />

    <!--PLUPLOAD-->
    <link type="text/css" rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/smoothness/jquery-ui.min.css" media="screen">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('js/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css') }}" media="screen">

    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" />

    <!--Icon-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- JavaScripts -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js" charset="UTF-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ URL::asset('js/jquery.filer.min.js') }}"></script>
    <script src="{{ URL::asset('Slider/jquery.flexslider.js') }}"></script>
    <script src="{{ URL::asset('Slider/jquery.easing.js') }}"></script>
    <script src="{{ URL::asset('Slider/jquery.mousewheel.js') }}"></script>
    <script src="{{ URL::asset('js/rating.js') }}"></script>
    <script src="{{ URL::asset('js/custom.js') }}"></script>

    <style>
        body {
            font-family: 'Droid Sans', sans-serif;
        }

        #nav-top {
            -webkit-transition: -webkit-transform .5s ease-in-out;
            transition: -webkit-transform .5s ease-in-out;
            transition: transform .5s ease-in-out;
            transition: transform .5s ease-in-out, -webkit-transform .5s ease-in-out;
            -webkit-transform: translateY(-54px);
            -ms-transform: translateY(-54px);
            transform: translateY(-54px);
        }

        #nav-top.up {
            -webkit-transform: translateY(0);
            -ms-transform: translateY(0);
            transform: translateY(0);
        }
    </style>

</head>
<body id="app-layout">
    @yield('nav')
    @include('partials.heading')
    <div class="hide" role="dialog" aria-hidden="true" id="flash_notif" style="position: fixed; z-index: 1; text-align: center; left: 50%; transform: translateX(-50%);">
        <div class="modal-dialog no-margin">
            <div class="modal-content" style="border-radius: 6px">
                <div class="alert alert-{{ session()->get('flash_notif')['type'] }} no-margin alert-dismissible" role="alert">
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ session()->get('flash_notif')['pesan'] }}
                </div>
            </div>
        </div>
    </div>
    @if(session()->get('flash_notif'))
        <script>
            $('#flash_notif').removeClass('hide');
            $('#flash_notif').addClass('animated fadeInDown');
            $('#flash_notif').modal({
                keyboard: false,
                backdrop: false,
                show: true
            });
            $('body.modal-open').css('overflow', 'inherit');
            setTimeout(function() {$('#flash_notif').addClass('fadeOutUp');}, 4000);
        </script>
    @endif
    @yield('content')
    @yield('footer')

    @yield('footer_script')
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js" charset="UTF-8"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plupload/plupload.full.min.js') }}" charset="UTF-8"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plupload/jquery.ui.plupload/jquery.ui.plupload.min.js') }}" charset="UTF-8"></script>
    <script>
        function init() {
            var imgDefer = document.getElementsByTagName('img');
            for (var i=0; i<imgDefer.length; i++) {
                if(imgDefer[i].getAttribute('data-src')) {
                    imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
                } 
            }
            $(imgDefer).css('background', 'none');
            $('input.rating').rating();
        }
        window.onload = init;
        $(function() {
            $('[data-toggle="tooltip"]').tooltip();

            $('a[href*="#"]:not([href="#"])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html, body').animate({
                    scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
                }
            });
        });
        $('.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(100).slideDown(100);
            }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(400).slideUp(150);
        });

        $('input').popover();
        $('textarea').popover();
        $('select').popover();
        $('form:not(.not-valid)').validator({
            feedback: {
                success: 'glyphicon-ok-circle',
                error: 'glyphicon-remove-circle'
            }
        });
    </script>
</body>
</html>
