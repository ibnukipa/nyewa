<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Password anda telah berhasil di reset!',
    'sent' => 'Kami telah meng-email link untuk me-reset password anda!',
    'token' => 'Link reset password tidak valid.',
    'user' => "Maaf! kami tidak bisa menemukan User dengan email tersebut",

];
