<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'activation_code', 'active', 'blocked', 'saldo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function iklan() {
        return $this->hasMany('App\Iklan', 'user_id');
    }

    public function pesan_keluar() {
        return $this->hasMany('App\Pesan', 'from_user_id');
    }

    public function pesan_masuk() {
        return $this->hasMany('App\Pesan', 'to_user_id');
    }

    // USER ROLES
    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }
    public function hasRole($name)
    {
        foreach($this->roles as $role)
        {
            if($role->name == $name) return true;
        }
        return false;
    }
    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }
    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }


}
