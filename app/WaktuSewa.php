<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaktuSewa extends Model
{
    protected $table = 'waktu_sewa_iklan';

    protected $fillable = [
        'waktu', 'harga'
    ];

    public function iklan() {
        return $this->belongsTo('App\Iklan');
    }
}
