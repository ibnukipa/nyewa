<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FasilitasIklan extends Model
{
    protected $table = 'fasilitas_iklan';

    protected $fillable = [
        'name', 'description'
    ];
}
