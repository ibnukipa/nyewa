<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori_iklan';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'parent_id', 'level'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function iklan() {
        return $this->hasMany('App\Iklan');
    }

    // Kategori Detail
    public function details()
    {
        return $this->belongsToMany('App\KategoriDetail', 'detail_kategori', 'ketegori_id', 'kategori_iklan_detail_id')->withTimestamps();
    }

    public function hasDetail($name)
    {
        foreach($this->details as $detail)
        {
            if($detail->name == $name) return true;
        }
        return false;
    }
    public function assignDetail($detail)
    {
        return $this->details()->attach($detail);
    }
    public function removeDetail($detail)
    {
        return $this->details()->detach($detail);
    }
}
