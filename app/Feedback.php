<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedback';

    protected $fillable = [
        'feedback_title', 'feedback_content', 'rating_value'
    ];

    public function iklan() {
        return $this->belongsTo('App\Iklan', 'iklan_id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
