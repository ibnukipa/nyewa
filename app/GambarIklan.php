<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GambarIklan extends Model
{
    protected $table = 'gambar_iklan';

    protected $fillable = [
        'path', 'file_name'
    ];

    public function iklan() {
        return $this->belongsTo('App\Iklan');
    }
}
