<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IklanDetail extends Model
{
    protected $table = 'iklan_detail';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'value', 'iklan_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function iklan() {
        return $this->belongsTo('App\Iklan');
    }
}
