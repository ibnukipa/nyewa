<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iklan extends Model
{
    protected $table = 'iklan';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'kategori_id', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function kategori() {
        return $this->belongsTo('App\Kategori');
    }

    public function feedbacks() {
        return $this->hasMany('App\Feedback');
    }

    public function details() {
        return $this->hasMany('App\IklanDetail');
    }

    public function waktuSewa() {
        return $this->hasMany('App\WaktuSewa');
    }

    public function gambarIklan() {
        return $this->hasMany('App\GambarIklan');
    }

    public function pesan() {
        return $this->hasMany('App\Pesan', 'iklan_id');
    }

    public function paket() {
        return $this->belongsToMany('App\Paket', 'paket_iklan', 'iklan_id', 'paket_id')->withPivot('start_at', 'end_at', 'active');
    }

    public function peket_saatini() {
        return $this->belongsToMany('App\Paket', 'paket_iklan', 'iklan_id', 'paket_id')->wherePivot('active','=', 1)->withPivot('start_at', 'end_at');
    }
}
