<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriDetail extends Model
{
    protected $table = 'kategori_iklan_detail';

    protected $fillable = [
        'name'
    ];
}
