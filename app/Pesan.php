<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesan extends Model
{
    protected $table = 'pesan';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_user_id', 'to_user_id', 'iklan_id', 'value', 'seen'
    ];

    public function user_pengirim() {
        return $this->belongsTo('App\User', 'from_user_id');
    }

    public function user_penerima() {
        return $this->belongsTo('App\User', 'to_user_id');
    }

    public function iklan() {
        return $this->belongsTo('App\Iklan', 'iklan_id');
    }
}
