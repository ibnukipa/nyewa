<?php

namespace App\Services;


use Illuminate\Mail\Mailer;
use Illuminate\Mail\Message;
use App\User;

class ActivationService
{

    protected $mailer;

    protected $activationRepo;

    protected $resendAfter = 24;

    public function __construct(Mailer $mailer, ActivationRepository $activationRepo)
    {
        $this->mailer = $mailer;
        $this->activationRepo = $activationRepo;
    }

    public function sendActivationMail($user)
    {

        if ($user->activated || !$this->shouldSend($user)) {
            return;
        }

        $token = $this->activationRepo->createActivation($user);
        // dd($token);
        $link = route('user.activate', $token);
        // dd($link);
        $message = sprintf('Activate account <a href="%s">%s</a>', $link, $link);

        $data = [
            'name' => $user->name,
            'link' => $link,
            'head' => 'Aktifasi Akun SewaAja',
            'open' => 'Hai, ' . $user->name,
            'body' => 'Silahkan aktifasi Akun SewaAja anda dengan mengeklik tombol Aktifasi dibawah ini :',
            'btn_text' => 'Aktifasi',
            'foot' => 'sewaaja.com'
        ];

        $this->mailer->queue('emails.activateAccount', $data, function($message) use ($user) {
			$message->subject( 'Aktivasi Akun SewaAja' );
			$message->to($user->email);
		});

        // $this->mailer->raw($message, function (Message $m) use ($user) {
        //     $m->to($user->email)->subject('Activation mail');
        // });
    }

    public function activateUser($token)
    {
        $activation = $this->activationRepo->getActivationByToken($token);
        
        if ($activation === null) {
            return null;
        }

        $user = User::find($activation->user_id);

        $user->active = true;

        $user->save();

        $this->activationRepo->deleteActivation($token);
        
        return $user;

    }

    private function shouldSend($user)
    {
        $activation = $this->activationRepo->getActivation($user);
        return $activation === null || strtotime($activation->created_at) + 60 * 60 * $this->resendAfter < time();
    }

}