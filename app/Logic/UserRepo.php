<?php namespace App\Logic;

use App\User;

class UserRepo {

    public function __construct() {
        //
    }

    private function user($id = null) {
        if($id == null) {
            return \Auth::user();
        } else {
            return User::find($id);
        }
    }

    public function getUser($id = null) {
        return $this->user($id);
    }

    public function updateKontak($dataKontak, $id = null) {
        $user = $this->getUser($id);
        
        $user->name     = $dataKontak['name'];
        $user->nohp     = $dataKontak['nohp'];
        $user->pinbb    = $dataKontak['pinbb'];
        // $user->provinsi_id = $dataKontak['provinsi'];
        // $user->kota_id     = $dataKontak['kota'];
        
        if($user->isDirty()) {
            if($user->save())
            {
                return true;
            }
            else 
                return false;
        } else {
            return 'nochange'; 
        }
    }
}