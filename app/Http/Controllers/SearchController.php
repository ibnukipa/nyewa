<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Kategori;
use App\Iklan;
use Carbon\Carbon;
class SearchController extends Controller
{
    private function mapKategori($kategoriFromURL) {
        $nameKategori;
        
        if($kategoriFromURL == 'mobil')
            $nameKategori = 'Mobil';
        else if($kategoriFromURL == 'motor')
            $nameKategori = 'Motor';
        else if($kategoriFromURL == 'properti')
            $nameKategori = 'Properti';
        else if($kategoriFromURL == 'elektronik-gadget')
            $nameKategori = 'Elektronik & Gadget';
        else if($kategoriFromURL == 'keperluan-pribadi')
            $nameKategori = 'Keperluan Pribadi';
        else if($kategoriFromURL == 'hobi-olahraga')
            $nameKategori = 'Hobi & Olahraga';
        else if($kategoriFromURL == 'rumah-tangga')
            $nameKategori = 'Rumah Tangga';
        else if($kategoriFromURL == 'jasa')
            $nameKategori = 'Jasa';
        else
            abort('404');

        return Kategori::where('name', '=', $nameKategori)
                            ->where('parent_id', '=', null)
                            ->first(); 
    }

    private function getListIklan($tempatID, $kategoriID, $keyword) {
        $tempatLengthString = strlen($tempatID);

        // Inisiasi semua iklan yang tersedia
        $listIklan = Iklan::where('status', '=', 'approved');

        //Filter berdasarkan tempat
        if($tempatLengthString >= 2) {
            // Disuatu kota atau provinsi
            $listIklan = $listIklan->where('tempat_id', 'like', $tempatID.'%');
        } else {
            //Semua tempat
        }

        // Filter berdasarkan kategori
        $kategori           = Kategori::find($kategoriID);
        if($kategori) {
            if($kategori->level == 1) {
                //Iklan di sub-pertama
                $listID = Kategori::where('parent_id', '=', $kategori->id)->lists('id')->toArray();
                array_push($listID, $kategori->id);
                $listIklan = $listIklan->whereIn('kategori_id', $listID);
            } elseif($kategori->level == 2) {
                //Iklan di sub-kedua
                $listIklan = $listIklan->where('kategori_id', '=', $kategori->id);
            }
        }

        //Filter berdasarkan keyword
        if($keyword) {
            $listIklan = $listIklan->where('name', 'like', '%'.$keyword.'%');
        }
        
        $listIklan->orderBy('created_at', 'DESC');
        
        return $listIklan->with('paket')->get();
    }

    private function rekursifKategori($kategori) {
        if($kategori->parent_id == null)
            return [$kategori->id, $kategori->name];
        
        return [$kategori->id, $kategori->name];
    }

    private function getPromotedIklan($listIklan) {
        return $listIklan->filter(function($iklan) {
             $paket = $iklan->paket;
             if(count($paket) > 0) {
                 $paketku =  $paket->filter(function ($paketan) {
                     if($paketan->pivot->active && $paketan->pivot->end_at >= date('Y-m-d H:i:s',strtotime(Carbon::now()->toDayDateTimeString()))) {   
                         return true;
                     } 
                 });   
                 if(count($paketku) > 0)
                    return true;
                 else
                    return false;
             } else 
                return false;
        })
        ->sortBy(function($iklan) {
            return $iklan->created_at;
        })
        ->sortBy(function ($iklan){
            return $iklan->paket[0]->id;
        })
        
        ;
    }

    private function getOrdinaryIklan($listIklan) {
        return $listIklan->filter(function($iklan) {
            $paket = $iklan->paket;
             if(count($paket) > 0) {
                 $paketku =  $paket->filter(function ($paketan) {
                     if($paketan->pivot->active && $paketan->pivot->end_at >= date('Y-m-d H:i:s',strtotime(Carbon::now()->toDayDateTimeString()))) {
                         return true;
                     }
                 });   
                 if(count($paketku) > 0)
                    return false;
                 else
                    return true;
             } else
                return true;
        });
    }
    
    public function generalSearch(Request $request) {
        $provinsiOrKotaID   = $request->input('provinsi');
        $kategoriID         = $request->input('kategori');
        $keyword            = $request->input('keyword');
        
        // Get All Iklan
        $listIklan = $this->getListIklan($provinsiOrKotaID, $kategoriID, $keyword);
        
        //Filter Paket Iklan
        $promotedIklan      = $this->getPromotedIklan($listIklan);
        
        $notPromotedIklan   = $this->getOrdinaryIklan($listIklan);
        //Filter Terbaru, Termurah, Termahal
        if($request->input('sortValue') === "termahal") {
            // Iklan Termahal
            $notPromotedIklan = $notPromotedIklan->sortBy(function($iklan) {
                return $iklan->lowest_price;
            })->reverse();
            $promotedIklan = $promotedIklan->sortBy(function($iklan) {
                return $iklan->lowest_price;
            })->reverse();
        } elseif($request->input('sortValue') === "termurah") {
            // Iklan Termurah
            $notPromotedIklan = $notPromotedIklan->sortBy(function($iklan) {
                return $iklan->lowest_price;
            });
            $promotedIklan = $promotedIklan->sortBy(function($iklan) {
                return $iklan->lowest_price;
            });
        }
        
        //Filtered Iklan
        $newListIklan = collect();
        foreach ($promotedIklan as $key => $value) {
            $newListIklan->push($value);
        }
        foreach ($notPromotedIklan as $key => $value) {
            $newListIklan->push($value);
        }
        
        // Path Halaman
        $pathKategori = array();
        if($request->input('kategori') > 0) {
            $kategori = Kategori::find($request->input('kategori'));
            array_push($pathKategori, [$kategori->id,$kategori->name]);
            if($kategori->parent_id != null) {  
                $kategori = Kategori::find($kategori->parent_id);
                array_push($pathKategori, $this->rekursifKategori($kategori));
            }
        }
        
        // Setup Page
        $currentPage        = $request->input('currentPage') != null ? $request->input('currentPage') : 1; 
        $limitItem          = $request->input('limitPage')  != null ? $request->input('limitPage') : 7; 
        $currentSort        = $request->input('sortValue') != null ? $request->input('sortValue') : "terbaru";
        
        $maxListIklanCount  = count($promotedIklan) > count($notPromotedIklan) ? count($promotedIklan) : count($notPromotedIklan);
        if((float)$maxListIklanCount/$limitItem > (float) floor($maxListIklanCount/$limitItem)) {
            $maxPage = (int) ceil($maxListIklanCount/$limitItem);
        } else {
            $maxPage = (int) floor($maxListIklanCount/$limitItem);
        }

        //Setup Data Page
        $data = [
            'page'                  => ['current' => $currentPage, 'limit' => $limitItem, 'max' => $maxPage],
            'promotedIklan'         => $promotedIklan->forPage($currentPage, $limitItem),
            'notPromotedIklan'      => $notPromotedIklan->forPage($currentPage, $limitItem),
            'selected_place_id'     => $request->input('provinsi'),
            'selected_kategori_id'  => $request->input('kategori'),
            'keyword'               => $request->input('keyword'),
            'currentSort'           => $currentSort,
            'pathKategori'          => $pathKategori,
        ];
          
        // dd($data);
        if($request->input('sortValue') && $request->input('currentPage') == null) {
            return view('pencarian.result-iklan')->with($data);
        } else {
            return view('pencarian.result')->with($data);
        }
    }

    public function keywordSuggestion($key) {

    }
}
