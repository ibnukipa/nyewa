<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DompetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($dompet = 'success') {
        return view('dashboard-user.dompet')
            ->with([
                'dompet'        => true,
                'iklanState'    => $dompet,
            ]);
    }
}
