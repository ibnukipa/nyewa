<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Kategori;
use App\FasilitasIklan;
use App\Iklan;
use App\GambarIklan;

use Storage;
use Auth;
use File;

class AjaxController extends Controller
{
    public function __construct()
    {
        
    }

    public function storeUploadImage() {
        
    }

    public function deleteUploadImage(Request $request) {
        $nama_file = $request->input('name_gambar');
        $iklan_id  = $request->input('iklan_id');
        
        $selected_gambar = GambarIklan::where('iklan_id', '=', $iklan_id)
                                        ->where('file_name', '=', $nama_file)
                                        ->first();
        if($selected_gambar) {
            Storage::delete($selected_gambar->path);
            $selected_gambar->delete();
        }
        
        return response()->json($selected_gambar);
    }
    
    public function getCountSubKategori(Request $request) {
        $kategori_id = $request->input('id_kategori');
        $data = Kategori::where('parent_id', '=', $kategori_id)
                        ->where('level', '=', 2)
                        ->count();
        if($data > 0) {
            $data = true;
        } else {
            $data = false;
        }
        return response()->json($data);
    }

    public function getSubKategori(Request $request) {

        $kategori_id = $request->input('id_kategori');

        $listSubKategori = Kategori::where('parent_id', '=', $kategori_id)
                                    ->where('level', '=', 2)
                                    ->get();
        
        foreach ($listSubKategori as $key => $value) {
            $data[] = array('id' => $value->id, 'text' => $value->name);
        }

        return response()->json($data);
    }

    public function getDetailIklanView(Request $request) {
        $kategori_id    = $request->input('id_kategori');
        $kategori       = Kategori::where('id', '=', $kategori_id)->first();
        $fasilitas      = FasilitasIklan::lists('name', 'name');
        
        if($request->input('id_iklan')) {
            $data = [
                'kategori'  => $kategori,
                'fasilitas' => $fasilitas,
                'iklan'     => Iklan::find($request->input('id_iklan'))
            ];
        } else {
            $data = [
                'kategori'  => $kategori,
                'fasilitas' => $fasilitas,
            ];
        }
        
        return view('iklan.detailForm')->with($data);
    }

    public function getKota(Request $request) {
        $provinsi_id    = $request->input('provinsi_id');

        foreach(\Indonesia::findProvince($provinsi_id, ['cities'])->cities as $key => $value) {
            $data[] = array('id' => $value->id, 'text' => $value->name);
        }
        return response()->json($data);
    }
}
