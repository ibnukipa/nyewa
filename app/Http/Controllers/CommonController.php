<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Halaman;

class CommonController extends Controller
{
    public function show($url) {
        $page = Halaman::where('url', '=', $url)->first();
        
        return view('halaman.page')->with([
            'page'     => $page,
        ]);
            
    }
}
