<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Iklan;
use App\IklanDetail;
use App\WaktuSewa;
use App\Kategori;
use App\Paket;
use App\Halaman;
use App\Feedback;
use App\FasilitasIklan;
use DB;

use Carbon\Carbon;

class AdminController extends Controller
{
    public function index (){
        $newiklan = Iklan::where('status', '=', 'new')->count();
        $editiklan = Iklan::where('status', '=', 'edited')->count();
        $newfeed = Feedback::where('status', '=', 'moderasi')->count();
        return view('dashboard-admin.index')->with([
            'count' => [
                'newiklan'  => $newiklan,
                'editiklan' => $editiklan,
                'newfeed'  => $newfeed
            ]
        ]);
    }

    public function user (){
        $user = User::where('blocked', '=', 0)->get();
        return view('dashboard-admin.user.akunUser')->with([
                    'user'     => $user
        ]);
    }

    public function userBlocked (){
        $user = User::where('blocked', '=', 1)->get();
        return view('dashboard-admin.user.akunUserBlocked')->with([
                    'user'     => $user
        ]);
    }

    
    public function blocked(Request $request) {
        $user_id = $request->input('user_id');

        $user = User::where('id', '=', $user_id)
                ->update(['blocked' => 1]);

        $pesanAlert = "Akun barhasil diblokir!";

        return redirect()->back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
    }

    public function unblocked(Request $request) {
        $user_id = $request->input('user_id');

        $user = User::where('id', '=', $user_id)
                ->update(['blocked' => 0]);

        $pesanAlert = "Akun barhasil diaktifkan!";

        return redirect()->back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
    }

    public function iklanbaru (){
        $listIklan = Iklan::where('status', '=', 'new')->get();
        foreach($listIklan as $key => $value)
        {
             $value->user->name;
             $value->kategori->name;
        }
        return view('dashboard-admin.iklan.iklanbaru')->with([
                    'listIklan'     => $listIklan
        ]);
    }

    public function editiklan (){
        $listIklan = Iklan::where('status', '=', 'edited')->get();
        foreach($listIklan as $key => $value)
        {
             $value->user->name;
             $value->kategori->name;
        }
        return view('dashboard-admin.iklan.editApproval')->with([
                    'listIklan'     => $listIklan
        ]);
    }

    public function blokiriklan (){
        $listIklan = Iklan::where('status', '=', 'blocked')->get();
        foreach($listIklan as $key => $value)
        {
             $value->user->name;
             $value->kategori->name;
        }
        return view('dashboard-admin.iklan.iklanBlocked')->with([
                    'listIklan'     => $listIklan
        ]);
    }

    public function iklan (){
        $listIklan = Iklan::where('status', '=', 'approved')->get();
        foreach($listIklan as $key => $value)
        {
             $value->user->name;
             $value->kategori->name;
        }
        return view('dashboard-admin.iklan.iklan')->with([
                    'listIklan'     => $listIklan
        ]);
    }

    public function approved_iklan ($id){
        $getIklan = Iklan::where('id', '=', $id)
                // ->update(['status' => 'approved'])
                ->first();
        $getIklan->status = 'approved';

        if(count($getIklan->paket) > 0) {
            $chosenPaket    = Paket::find($getIklan->paket()->first()->pivot->paket_id);
            $durationPaket  = $chosenPaket->value;

            $current_time   = Carbon::now()->toDayDateTimeString();
            $startPaket     = strtotime($current_time);
            $endPaket       = mktime(date('H', strtotime($current_time)), date('i', strtotime($current_time)), date('s', strtotime($current_time)), date('m', strtotime($current_time)), date('d', strtotime($current_time)) + $durationPaket, date('Y', strtotime($current_time)));
            $getIklan->paket()->first()->pivot->update(['start_at' => date('Y-m-d H:i:s', $startPaket), 'end_at' => date('Y-m-d H:i:s', $endPaket), 'active' => 1]);
        }

        $getIklan->save();

        return redirect()->back()->with([
                'alert'         => [    'type'      => 'success', 
                                        'message'   => "Iklan berhasil ditayangkan"]
            ]);
    }

    public function blocked_iklan (Request $request){
        $getIklan = $request->input('getIklan');

        $iklan = Iklan::where('id', '=', $getIklan)
                ->update(['status' => 'blocked']);
                

        return redirect()->back()->with([
                'alert'         => [    'type'      => 'info', 
                                        'message'   => "Iklan berhasil ditolak"]
            ]);
    }

    public function moderasi (){
        $moderasi = Feedback::where('status', '=', 'moderasi')->get();
        foreach($moderasi as $key => $value)
        {
             $value->user->name;
             $value->iklan->name;
        }
        return view('dashboard-admin.komentar.moderasikomentar')->with([
                    'moderasi'     => $moderasi
        ]);
    }

    public function komentar (){
        $feedback = Feedback::where('status', '=', 'approved')->get();
        foreach($feedback as $key => $value)
        {
             $value->user->name;
             $value->iklan->name;
        }
        return view('dashboard-admin.komentar.komentar')->with([
                    'feedback'     => $feedback
        ]);
    }

    public function komplain (){
        return view('dashboard-admin.komplain.komplain');
    }

    public function keuangan (){
        return view('dashboard-admin.laporan.keuangan');
    }

    public function jumlahUser (){
        $current = Carbon::now();
        $current_date  = $current->toDateString();
        $hari6 = $current->subDays(1)->toDateString();
        $hari5 = $current->subDays(1)->toDateString();
        $hari4 = $current->subDays(1)->toDateString();
        $hari3 = $current->subDays(1)->toDateString();
        $hari2 = $current->subDays(1)->toDateString();
        $hari1 = $current->subDays(1)->toDateString();
        $year =$current->year;
        $jum7= User::where('created_at','like',$current_date.'%')->count();
        $jum6= User::where('created_at','like',$hari6.'%')->count();
        $jum5= User::where('created_at','like',$hari5.'%')->count();
        $jum4= User::where('created_at','like',$hari4.'%')->count();
        $jum3= User::where('created_at','like',$hari3.'%')->count();
        $jum2= User::where('created_at','like',$hari2.'%')->count();
        $jum1= User::where('created_at','like',$hari1.'%')->count();
        $jan = User::where('created_at','like',$year.'-01-%')->count();
        $feb = User::where('created_at','like',$year.'%-02-%')->count();
        $mar = User::where('created_at','like',$year.'%-03-%')->count();
        $apr = User::where('created_at','like',$year.'%-04-%')->count();
        $mei = User::where('created_at','like',$year.'%-05-%')->count();
        $jun = User::where('created_at','like',$year.'%-06-%')->count();
        $jul = User::where('created_at','like',$year.'%-07-%')->count();
        $agu = User::where('created_at','like',$year.'%-08-%')->count();
        $sep = User::where('created_at','like',$year.'%-09-%')->count();
        $okt = User::where('created_at','like',$year.'%-10-%')->count();
        $nov = User::where('created_at','like',$year.'%-11-%')->count();
        $des = User::where('created_at','like',$year.'%-12-%')->count();
        $year4 = $current->subYears(1)->year;
        $year3 = $current->subYears(1)->year;
        $year2 = $current->subYears(1)->year;
        $year1 = $current->subYears(1)->year;
        $tahun5 = User::where('created_at','like',$year.'%')->count();
        $tahun4 = User::where('created_at','like',$year4.'%')->count();
        $tahun3 = User::where('created_at','like',$year3.'%')->count();
        $tahun2 = User::where('created_at','like',$year2.'%')->count();
        $tahun1 = User::where('created_at','like',$year1.'%')->count();
        return view('dashboard-admin.laporan.jumlahuser')->with([
            'jumlah_hari' => [
                'hari1' => $jum1,
                'hari2' => $jum2,
                'hari3' => $jum3,
                'hari4' => $jum4,
                'hari5' => $jum5,
                'hari6' => $jum6,
                'hari7' => $jum7
            ],
            'tanggal' => [
                'hari1' => $hari1,
                'hari2' => $hari2,
                'hari3' => $hari3,
                'hari4' => $hari4,
                'hari5' => $hari5,
                'hari6' => $hari6,
                'hari7' => $current_date 
            ],
            'jumlah_bulan' => [
                'bulan1' => $jan,
                'bulan2' => $feb,
                'bulan3' => $mar,
                'bulan4' => $apr,
                'bulan5' => $mei,
                'bulan6' => $jun,
                'bulan7' => $jul,
                'bulan8' => $agu,
                'bulan9' => $sep,
                'bulan10' => $okt,
                'bulan11' => $nov,
                'bulan12' => $des,
                'tahun' => $year
            ],
            'jumlah_tahun' => [
                'tahun1' => $tahun1,
                'tahun2' => $tahun2,
                'tahun3' => $tahun3,
                'tahun4' => $tahun4,
                'tahun5' => $tahun5
            ],
            'tahun' => [
                'tahun1' => $year1,
                'tahun2' => $year2,
                'tahun3' => $year3,
                'tahun4' => $year4,
                'tahun5' => $year  
            ]
        ]);
    }

    public function papaniklan (){
        return view('dashboard-admin.setting.papaniklan');
    }

    public function halaman (){
        return view('dashboard-admin.halaman.halamanbaru');
    }

    public function listHalaman (){
        $page = Halaman::get();

        return view('dashboard-admin.halaman.listHalaman')->with([
                    'page'     => $page
        ]);
    }

    public function store(Request $request)
    {
        $page = new Halaman;
        $page->title = $request->input('judul');
        $page->sub = $request->input('sub');
        $page->content = $request->input('datainput');
        $page->url = preg_replace('/[^a-zA-Z0-9-]/', '-', $request->input('judul'));
        $page->save();
        $pesanAlert = "Halaman barhasil diaktifkan!";

        return redirect()->back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
    }

    public function editHalaman($id) {
        $page = Halaman::where('id', '=', $id)->get();
        return view('dashboard-admin.halaman.halamanedit')->with([
                    'page'     => $page
        ]);
    }

    public function updateHalaman(Request $request) {
        $getPage = $request->input('page_id');

        $page = Halaman::where('id', '=', $getPage)
                ->update([
                    'title' => $request->input('judul'),
                    'sub'   => $request->input('sub'),
                    'content' =>$request->input('datainput')
                    ]);

        $page = Halaman::get();

        return view('dashboard-admin.halaman.listHalaman')->with([
                    'page'     => $page,
                    'alert'         => [    'type'      => 'info', 
                                        'message'   => "Halaman berhasil diupdate"]
        ]);
    }

    public function changePasswordAdmin(Request $request){
        $old = $request->input('old');
        $new = $request->input('new');
        $confirm = $request->input('confirm');

        $user = User::find(1);
        $password = $user->password;

        //$user = User::where('id','=', 1)->get();
        //$password = $user->password;
        //$users = DB::select('select password from users where id = ?', [1])->result_array();

        dd($old);

        //$password = user::select('select password from users where id = 1');
        //dd($password);
        //dd($password);
        if($old == $password)
        {
            dd("AAAA");
        }
        else
        {
            dd("BBB");
        }
    }

    public function list_fasilitas(){
        $fasilitas = FasilitasIklan::get();
        return view('dashboard-admin.setting.listfasilitas')->with([
                    'fasilitas'     => $fasilitas
        ]);
    }

    public function tambah_fasilitas(Request $request){
        $fasilitasi = FasilitasIklan::get();
        $namafasilitas = FasilitasIklan::where('name','=',$request->input('name'))->count();

        if($namafasilitas == null){
        $fasilitas = new FasilitasIklan;
        $fasilitas->name = $request->input('name');
        $fasilitas->description = $request->input('description');
        $fasilitas->save();
        $pesanAlert = "Fasilitas barhasil ditambahkan!";
        }
        else
        {
            $pesanAlert = "Fasilitas sudah ada!";
        }

        return view('dashboard-admin.setting.listfasilitas')->with([
                    'fasilitas'     => $fasilitasi,
                    'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
    }

    public function edit_fasilitas(Request $request){
        $fasilitas = FasilitasIklan::where('id','=',$request->input('id'))
                        ->update(['name' => $request->input('name'),
                                  'description' => $request->input('description')
                                  ]);

        $fasilitasi = FasilitasIklan::get();
        $pesanAlert = "Fasilitas barhasil diubah!";
         return view('dashboard-admin.setting.listfasilitas')->with([
                    'fasilitas'     => $fasilitasi,
                    'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
    }

    public function hapus_fasilitas(Request $request){
        $fasilitas = FasilitasIklan::where('id','=',$request->input('id'))
                        ->delete();

        $fasilitasi = FasilitasIklan::get();
        $pesanAlert = "Fasilitas barhasil dihapus!";
         return view('dashboard-admin.setting.listfasilitas')->with([
                    'fasilitas'     => $fasilitasi,
                    'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
    }
}
