<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Iklan;

use File;
use Storage;
use Response;
use Image;
class GambarController extends Controller
{
    public function getAssetImg($nama_asset, $pixel) {
        $pathGambar = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . '../asset/'.$nama_asset;
        if(!File::exists($pathGambar)) {
            $pathGambar = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().'../image_404.png';
        }

        $gambarRequested = Image::make($pathGambar);
        $gambarRequested->resize($pixel, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $gambarRequested->response($gambarRequested->mime());
    }

    public function getImageIklan($id_iklan, $name_gambar, $pixel) {
        $iklan      = Iklan::find($id_iklan);

        //Set Watermark
        $watermark  = Image::make(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().'../watermark.png');
        $watermark->resize((int)($pixel/2.5), null, function ($constraint) {
            $constraint->aspectRatio();
        });

        if(count($iklan->gambarIklan()->where('file_name', '=', $name_gambar)->get()) == 1)
            $pathGambar = $iklan->gambarIklan()->where('file_name', '=', $name_gambar)->first()->path;
        else
            $pathGambar = 'notfound';

        $pathGambar = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $pathGambar;
        if(!File::exists($pathGambar)) {
            $pathGambar = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().'../image_404.png';
        }
        
        //Set File Gambar
        $newFileCropped = Image::make($pathGambar);
        $newFileCropped->resize($pixel, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $heightImage    = $newFileCropped->height();
        $widthImage     = $newFileCropped->width();

        if($heightImage > $widthImage) {
            //Vertical
            $newFileCropped->crop($widthImage, $widthImage, 0, (int)(($heightImage - $widthImage) / 2));
        } else {
            //Horizontal
            $newFileCropped->crop($heightImage, $heightImage, (int)(($widthImage - $heightImage) / 2), 0);
        }

        $newFileCropped->insert($watermark, 'bottom-left', (int)(($pixel/3)/4), (int)(($pixel/3)/4));
        
        return $newFileCropped->response($newFileCropped->mime());
    }
}
