<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::user()->hasRole('administrator'))
            return redirect('admin');
        else
            return view('dashboard');
    }

    public function settings($setting = 'kontak') {
        return view('dashboard-user.pengaturan')
            ->with([
                'setting'     => true,
                'iklanState'  => $setting,
            ]);
    }

}
