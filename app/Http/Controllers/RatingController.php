<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Feedback;
use App\Iklan;

class RatingController extends Controller
{
    public function store(Request $request) {
        // dd($request->input());
        $feedback = new Feedback;
        $iklan    = Iklan::find($request->input('iklan_id'));

        $feedback->feedback_title   = "Rating";
        $feedback->feedback_content = $request->input('feedback');
        $feedback->rating_value     = $request->input('rating');
        $feedback->user_id          = \Auth::user()->id;
        $iklan->feedbacks()->save($feedback);

        $totalRating = 0;
        $totalRater  = 0;
        foreach ($iklan->feedbacks as $key => $feedback) {
            if($feedback->rating_value > 0 && $feedback->rating_value <= 5) {
                $totalRating += $feedback->rating_value;
                $totalRater++;
            }
        }
        $iklan->rating = ceil($totalRating/$totalRater);
        $iklan->save();
    }
}
