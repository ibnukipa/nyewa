<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Requests;
use App\User;
use App\Iklan;
use App\IklanDetail;
use App\WaktuSewa;
use App\Kategori;
use App\FasilitasIklan;
use App\GambarIklan;
use App\Paket;
use App\Logic\UserRepo as UserRepo;

use Storage;
use Auth;
use File;

class IklanController extends Controller
{
    protected $userRepo;

    public function __construct()
    {
        // $this->middleware('auth');
        $this->userRepo = new UserRepo;
    }

    private function getStatusIklan($state) {
        if($state == 'aktif') {
            return 'approved';
        } else if($state == 'moderasi') {
            return 'new';
        } else if($state == 'gagal') {
            return 'blocked';
        } else if($state == 'nonaktif') {
            return 'nonaktif';
        } else {
            return 'nothing';
        }
    }

    public function index($iklan = 'aktif') {
        
        if($iklan == 'aktif' || $iklan == 'moderasi' || $iklan == 'gagal' || $iklan == 'nonaktif') {
            $listIklan = \Auth::user()->iklan()->where('status', '=', $this->getStatusIklan($iklan))->paginate(5);
            return view('dashboard-user.iklan')
                ->with([
                    'iklan'         => true,
                    'iklanState'    => $iklan,
                    'listIklan'     => $listIklan,
                    // 'totalIklan'    => $totalIklan,
                    // 'alert'         => [    'type'      => 'success', 
                    //                     'message'   => "<strong>Berhasil!</strong> Iklan barhasil dihapus!"]
                ]);
        } else {
            abort(404);
        }
    }

    public function show($url_iklan) {
        $iklan = Iklan::where('url_iklan', '=', str_replace('.html', '', $url_iklan))->first();
        
        if($iklan && $iklan->status == 'approved') {
            $iklan->seen += 1;
            $iklan->save();
        }
        
        if($iklan && !\Auth::guest()) {
            if($iklan->status == 'approved' || \Auth::user()->hasRole('administrator')) {
                return view('iklan.show')->with([
                    'iklan'     => $iklan,
                ]);
            } elseif($iklan->user_id == \Auth::user()->id) {
                return view('iklan.show')->with([
                    'iklan'     => $iklan,
                ]);
            } else abort('404');
        } elseif($iklan) {
            if($iklan->status == 'approved') {
                return view('iklan.show')->with([
                    'iklan'     => $iklan,
                ]);
            } else abort('404');
        } else abort('404');
    }

    public function create() {
        $paket          = Paket::get();
        $kategori_utama = Kategori::where('level', '=', 1)->get();
        return view('iklan.create')->with([
            'kategori_utama'    => $kategori_utama,
            'paket_iklan'       => $paket
        ]);
    }

    public function edit($id_iklan) {
        $paket          = Paket::get();
        $kategori_utama = Kategori::where('level', '=', 1)->get();
        return view('iklan.edit')->with([
            'kategori_utama'    => $kategori_utama,
            'paket_iklan'       => $paket,
            'iklan'             => Iklan::find($id_iklan)
        ]);
    }

    public function update(Request $request) {
        // dd($request->input());
        $currentIklan = Iklan::find($request->input('iklan_id'));
        if($request->input('paket')) {
            $currentPaket = $currentIklan->paket()->where('active','=',0)->where('start_at', '=', null)->first();
            if($currentPaket) {
                \Auth::user()->saldo += $currentPaket->harga;
                \Auth::user()->save();
                $currentIklan->paket()->detach($currentPaket);
            }

            $chosenPaket    = Paket::find($request->input('paket'));
            $hargaPaket     = $chosenPaket->harga;
            \Auth::user()->saldo -= $hargaPaket;
            \Auth::user()->save();
            $currentIklan->paket()->attach($request->input('paket'), ['active' => 0]);
            
        }

        $currentIklan->details()->delete();
        $currentIklan->waktuSewa()->delete();

        $waktu = $request->input('waktu');
        $harga = $request->input('harga');

        //Mengambil id kategori berdasarkan level
        $kategoriLevel = count($request->input('kategori'));
        $kategoriId = $request->input('kategori')[$kategoriLevel-1];

        $currentIklan->name         = $request->input('name_iklan');
        $currentIklan->description  = str_replace(array("\r\n"),"<br>",$request->input('description'));
        $currentIklan->kategori_id  = $kategoriId;

        $currentIklan->url_iklan    = preg_replace('/[^a-zA-Z0-9-]/', '-', $request->input('name_iklan')).'-'.$currentIklan->id;
        $currentIklan->tempat_id    = $request->input('kota');
        $currentIklan->save();
        $this->userRepo->updateKontak($request->input());

        $kategori = Kategori::find($kategoriId);
        foreach($kategori->details as $value) {
            if($value->name == 'fasilitas') {
                $fasilitass = $request->input($value->name);
                foreach($fasilitass as $key => $idFasilitas) {
                    $fasilitasName          = FasilitasIklan::where('id', '=', $idFasilitas)->first()->name;
                    $iklanDetail            = new IklanDetail;
                    $iklanDetail->name      = $value->name;
                    $iklanDetail->value     = $fasilitasName;
                    $currentIklan->details()->save($iklanDetail);        
                }
            } else {
                $iklanDetail            = new IklanDetail;
                $iklanDetail->name      = $value->name;
                $iklanDetail->value     = $request->input($this->spaceToUnderline($value->name));
                $currentIklan->details()->save($iklanDetail);
            }
        }

        $lowestPrice = $harga[0];
        $highestPrice = $harga[0];
        for ($i=0; $i < count($waktu); $i++) {
            $waktuSewa = new WaktuSewa;
            $waktuSewa->waktu = $waktu[$i]; 
            $waktuSewa->harga = $this->dotToNothing($harga[$i]);

            if($waktuSewa->harga > $highestPrice)
                $highestPrice = $waktuSewa->harga;

            if($waktuSewa->harga < $lowestPrice)
                $lowestPrice = $waktuSewa->harga;

            $currentIklan->waktuSewa()->save($waktuSewa);
        }

        $currentIklan->highest_price = $highestPrice;
        $currentIklan->lowest_price = $lowestPrice;
        if($currentIklan->status != 'new')
            $currentIklan->status = 'edited';
        $currentIklan->save();
        
        if($request->file('files')[0] != null) {
            foreach ($request->file('files') as $key => $file) {
                $nameFileNewImage   = $key.substr(hash('sha512',Carbon::now()->toDayDateTimeString()), -8).'_'.$this->toFileName($currentIklan->name).'.'.$file->clientExtension();
                $pathNewImage       = 'gambar/'.Auth::user()->email.'/'.$nameFileNewImage;
                $contentNewImage    = file_get_contents($file->getRealPath());
                $sizeNewImage       = filesize($file->getRealPath());
                $typeNewImage       = mime_content_type($file->getRealPath());

                $gambar             = new GambarIklan;
                $gambar->path       = $pathNewImage;
                $gambar->file_name  = $nameFileNewImage;
                $gambar->file_mime  = $typeNewImage;
                $gambar->file_size  = $sizeNewImage;

                $currentIklan->gambarIklan()->save($gambar);
                Storage::put($pathNewImage, $contentNewImage);
            }
        }

        return redirect()->route('iklan.tipe', ['moderasi'])->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => "Iklan <strong>". $request->input('name') ."</strong> telah di-edit! <br> Admin akan segera memverifikasi iklan untuk segera ditayangkan"]
        ]);
    }

    public function store(Request $request) {
        // dd($request->file());
        $user = $this->userRepo->getUser();

        //Data harga
        $waktu = $request->input('waktu');
        $harga = $request->input('harga');

        //Mengambil id kategori berdasarkan level
        $kategoriLevel = count($request->input('kategori'));
        $kategoriId = $request->input('kategori')[$kategoriLevel-1];

        //Membuat iklan baru
        $newIklan = new Iklan;        
        $newIklan->name         = $request->input('name_iklan');
        $newIklan->description  = str_replace(array("\r\n"),"<br>",$request->input('description'));
        $newIklan->status       = 'new';
        $newIklan->kategori_id  = $kategoriId;

        $user->iklan()->save($newIklan);
        $newIklan->url_iklan    = preg_replace('/[^a-zA-Z0-9-]/', '-', $request->input('name_iklan')).'-'.$newIklan->id;
        $newIklan->tempat_id    = $request->input('kota');
        $newIklan->save();
        $this->userRepo->updateKontak($request->input());

        //Membuat Paket
        if($request->input('paket')) {
            $chosenPaket    = Paket::find($request->input('paket'));
            $hargaPaket     = $chosenPaket->harga;
            \Auth::user()->saldo -= $hargaPaket;
            \Auth::user()->save();
            $newIklan->paket()->attach($request->input('paket'), ['active' => 0]);
        }

        //Membuat detail iklan berdasarkan kategori detail
        $kategori = Kategori::find($kategoriId);
        foreach($kategori->details as $value) {
            if($value->name == 'fasilitas') {
                $fasilitass = $request->input($value->name);
                foreach($fasilitass as $key => $idFasilitas) {
                    $fasilitasName          = FasilitasIklan::where('id', '=', $idFasilitas)->first()->name;
                    $iklanDetail            = new IklanDetail;
                    $iklanDetail->name      = $value->name;
                    $iklanDetail->value     = $fasilitasName;
                    $newIklan->details()->save($iklanDetail);        
                }
            } else {
                $iklanDetail            = new IklanDetail;
                $iklanDetail->name      = $value->name;
                $iklanDetail->value     = $request->input($this->spaceToUnderline($value->name));
                $newIklan->details()->save($iklanDetail);
            }
        }

        //Membuat harga sewa
        $lowestPrice = $harga[0];
        $highestPrice = $harga[0];
        for ($i=0; $i < count($waktu); $i++) {
            $waktuSewa = new WaktuSewa;
            $waktuSewa->waktu = $waktu[$i]; 
            $waktuSewa->harga = $this->dotToNothing($harga[$i]);

            if($waktuSewa->harga > $highestPrice)
                $highestPrice = $waktuSewa->harga;

            if($waktuSewa->harga < $lowestPrice)
                $lowestPrice = $waktuSewa->harga;

            $newIklan->waktuSewa()->save($waktuSewa);
        }

        $newIklan->highest_price = $highestPrice;
        $newIklan->lowest_price = $lowestPrice;
        $newIklan->save();
        
        //Menyimpan gambar iklan
        foreach ($request->file('files') as $key => $file) {
            $nameFileNewImage   = $key.substr(hash('sha512',Carbon::now()->toDayDateTimeString()), -8).'_'.$this->toFileName($newIklan->name).'.'.$file->clientExtension();
            $pathNewImage       = 'gambar/'.Auth::user()->email.'/'.$nameFileNewImage;
            $contentNewImage    = file_get_contents($file->getRealPath());
            $sizeNewImage       = filesize($file->getRealPath());
            $typeNewImage       = mime_content_type($file->getRealPath());

            $gambar             = new GambarIklan;
            $gambar->path       = $pathNewImage;
            $gambar->file_name  = $nameFileNewImage;
            $gambar->file_mime  = $typeNewImage;
            $gambar->file_size  = $sizeNewImage;

            $newIklan->gambarIklan()->save($gambar);
            Storage::put($pathNewImage, $contentNewImage);
        }

        return redirect()->route('iklan.tipe', ['moderasi'])->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => "Iklan <strong>". $request->input('name') ."</strong> telah terbuat! <br> Admin akan segera memverifikasi iklan untuk segera ditayangkan"]
        ]);
    }

    public function changeStatus(Request $request) {
        try {
            Iklan::where('id', '=', $request->input('iklan_id'))
                ->update(['status' => $this->getStatusIklan($request->input('tobe_status'))]);
            
            return redirect()->back()->with([
                'flash_notif'         => [  'type'      => 'success',
                                            'pesan'     => 'Berhasil me'.$request->input('tobe_status').'kan iklan!',
                                        ]
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'flash_notif'         => [  'type'      => 'danger',
                                            'pesan'     => 'Gagal me'.$request->input('tobe_status').'kan iklan!',
                                        ]
            ]);
        }
    }

    public function promote(Request $request) {
    
        $iklan = Iklan::find($request->input('iklan_id'));
        
        if(count($iklan->paket) > 0 && count($iklan->paket()->where('active', '=', 1)->get()) > 0){
            if(strtotime($iklan->paket()->where('active', '=', 1)->first()->pivot->end_at) >= strtotime(Carbon::now()->toDayDateTimeString())) {
                return redirect()->back()->with([
                    'flash_notif'         => [  'type'      => 'danger',
                                                'pesan'     => 'Maaf! Iklan Anda sedang dalam masa Promosi.',
                                            ]
                ]);     
            }
            $iklan->paket()->where('active', '=', 1)->first()->pivot->update(['active' => 0]);
        }

        $chosenPaket    = Paket::find($request->input('paket'));

        $hargaPaket     = $chosenPaket->harga;
        if(\Auth::user()->saldo >= $hargaPaket)
            \Auth::user()->saldo -= $hargaPaket;
        else {
            return redirect()->back()->with([
                'flash_notif'         => [  'type'      => 'danger',
                                            'pesan'     => 'Maaf! Saldo Anda tidak mencukupi untuk membeli paket ini.',
                                        ]
            ]);    
        }

        $durationPaket  = $chosenPaket->value;
        $current_time   = Carbon::now()->toDayDateTimeString();
        $startPaket     = strtotime($current_time);
        $endPaket       = mktime(date('H', strtotime($current_time)), date('i', strtotime($current_time)), date('s', strtotime($current_time)), date('m', strtotime($current_time)), date('d', strtotime($current_time)) + $durationPaket, date('Y', strtotime($current_time)));
        $iklan->paket()->attach($request->input('paket'), ['start_at' => date('Y-m-d H:i:s', $startPaket), 'end_at' => date('Y-m-d H:i:s', $endPaket), 'active' => 1]);

        \Auth::user()->save();
        return redirect()->back()->with([
            'flash_notif'         => [  'type'      => 'success',
                                        'pesan'     => 'Berhasil mempromosikan iklan '.$iklan->name.' Anda!',
                                    ]
        ]);
    }

    public function destroy(Request $request) {
        $iklan_id = $request->input('iklan_id');

        $iklan = Iklan::find($iklan_id);
        $iklan->status = 'deleted';
        $iklan->save();
        // foreach($iklan->gambarIklan as $gambarIklan) {
        //     Storage::delete($gambarIklan->path);
        // }

        $pesanAlert = "<strong>". $iklan->name ."</strong> barhasil dihapus!";

        // if($iklan->delete()) {
        return redirect()->back()->with([
            'alert'         => [    'type'      => 'success', 
                                    'message'   => $pesanAlert]
        ]);
        // }
    }

    private function toFileName($data) {
        return preg_replace('/[^a-zA-Z0-9-]/', '', $data);
    }

    private function spaceToUnderline($data) {
        return str_replace(' ', '_', $data);
    }

    private function dotToNothing($data) {
        return str_replace('.', '', $data);
    }
}
