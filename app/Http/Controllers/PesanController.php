<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Pesan;
use App\User;
use App\Iklan;

use Auth;

class PesanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function getPesanIklanKu() {
        return Pesan::whereIn('iklan_id', function($query){
                $query->select('id')->from(with(new Iklan)->getTable())->where('user_id', '=', Auth::user()->id);
            });
    }

    private function getPesanIklanLain() {
        return Pesan::whereNotIn('iklan_id', function($query) {
                $query->select('id')->from(with(new Iklan)->getTable())->where('user_id', '=', Auth::user()->id);
            });
    }

    public function index($pesan) {
        if($pesan == 'masuk' || $pesan == 'keluar') {
            $listPesan;
            $groupedPesan;
            if($pesan == 'masuk') {
                $listPesan      = $this->getPesanIklanKu();
                $groupedPesan   = $listPesan
                                            ->where('from_user_id', '!=', Auth::user()->id)
                                            ->groupBy('from_user_id', 'iklan_id')
                                            ->orderBy('created_at', 'desc')
                                            ->get();
                
                
                $opositeListPesan   = $this->getPesanIklanLain()->get();
                $listPesan      = $this->getPesanIklanKu();
                $listPesan      = $listPesan
                                                ->where('from_user_id', '!=', Auth::user()->id)
                                                ->groupBy('iklan_id')
                                                ->orderBy('created_at', 'desc')
                                                ->get();
            }
            else {
                $listPesan      = $this->getPesanIklanLain();
                $groupedPesan   = $listPesan
                                            ->where('from_user_id', '=', Auth::user()->id)
                                            ->groupBy('from_user_id', 'iklan_id')
                                            ->orderBy('created_at', 'desc')
                                            ->get();

                $opositeListPesan   = $this->getPesanIklanKu()->get();
                $listPesan      = $this->getPesanIklanLain();
                $listPesan      = $listPesan
                                                ->where('from_user_id', '=', Auth::user()->id)
                                                ->groupBy('iklan_id')
                                                ->orderBy('created_at', 'desc')
                                                ->get();
            }

            return view('dashboard-user.pesan')
                ->with([
                    'pesan'             => true,
                    'iklanState'        => $pesan,
                    'listPesan'         => $groupedPesan,
                    'opositeListPesan'  => $opositeListPesan,
                    'listPesan2'        => $listPesan,
            ]);
        } else abort('404');
    }

    public function show(Request $request) {
        $pesanOne      = Pesan::find($request->input('pesan_id'));
        $iklan         = Iklan::find($pesanOne->iklan_id);
        $me_id         = $pesanOne->to_user_id;
        $you_id        = $pesanOne->from_user_id;
        $pesans         = Pesan::
                        where(function($query) use ($me_id, $you_id, $iklan)  {
                            $query->where('iklan_id', '=', $iklan->id)
                                ->where(function($query) use ($me_id, $you_id) {
                                    $query->where('from_user_id', '=', $you_id)
                                    ->where('to_user_id', '=', $me_id);
                                });
                        })
                        ->orWhere(function($query) use ($me_id, $you_id, $iklan) {
                            $query->where('iklan_id', '=', $iklan->id)
                                ->where(function($query) use ($me_id, $you_id) {
                                    $query->where('from_user_id', '=', $me_id)
                                    ->where('to_user_id', '=', $you_id);
                                });
                        })
                        ->get();
        return view('pesan.detail')->with([
            'value'     => $iklan,
            'pesanOne'  => $pesanOne,
            'pesans'     => $pesans,
        ]);
    }

    public function showPesanMasukByIdIklan($pesan, $idiklan) {
        try {
            $iklan = Iklan::find($idiklan);

            if($pesan == 'masuk' || $pesan == 'keluar') {
                if($pesan == 'masuk') {
                    $listPesan      = $this->getPesanIklanKu();
                    $groupedPesan   = $listPesan
                                                ->where('from_user_id', '!=', Auth::user()->id)
                                                ->where('iklan_id', '=', $iklan->id)
                                                ->groupBy('from_user_id')
                                                ->orderBy('created_at', 'desc')
                                                ->get();
                    
                    
                    $opositeListPesan   = $this->getPesanIklanLain();
                    $listPesan      = $this->getPesanIklanKu();
                    $listPesan      = $listPesan
                                                ->where('from_user_id', '!=', Auth::user()->id)
                                                ->groupBy('iklan_id')
                                                ->orderBy('created_at', 'desc')
                                                ->get();
                    
                }
                else {
                    $listPesan      = $this->getPesanIklanLain();
                    $groupedPesan   = $listPesan
                                                ->where('from_user_id', '=', Auth::user()->id)
                                                ->where('iklan_id', '=', $iklan->id)
                                                ->groupBy('from_user_id')
                                                ->orderBy('created_at', 'desc')
                                                ->get();

                    $opositeListPesan   = $this->getPesanIklanKu();
                    $listPesan      = $this->getPesanIklanLain();
                    $listPesan      = $listPesan
                                                ->where('from_user_id', '=', Auth::user()->id)
                                                ->groupBy('iklan_id')
                                                ->orderBy('created_at', 'desc')
                                                ->get();
                }

                return view('dashboard-user.pesan')
                    ->with([
                        'pesan'         => true,
                        'iklanState'    => $pesan,
                        'listPesan'     => $groupedPesan,
                        'listPesan2'    => $listPesan,
                        'id_iklan_selected_by_sorting' => $idiklan,
                        'opositeListPesan'  => $opositeListPesan
                ]);
                
            } else abort('404');

        } catch (\Exception $e) {
            abort('404');
        }
    }

    public function balas(Request $request) {
        try {
            $fromUser   = User::find($request->input('to_user_id'));
            $toUser     = User::find($request->input('from_user_id'));
            $iklan_id   = $request->input('iklan_id');

            $newPesan   = new Pesan;
            $newPesan->from_user_id     = $fromUser->id;
            $newPesan->to_user_id       = $toUser->id;
            $newPesan->iklan_id         = $iklan_id;
            $newPesan->value            = str_replace("\r\n", "<br>", $request->input('pesan'));

            if($newPesan->save()) {

                return response()->json([
                    'pesan_terbuat'     => date('Y/m/d, H:i:s', strtotime($newPesan->created_at)),
                    'pesan_value'       => $newPesan->value,
                ]);
            } else {
                return response()->json(false);
            }  
        } catch (\Exception $e) {
            return false;
        }
    }

    public function store(Request $request) {
        // dd($request->input('isi_pesan'));
        // try {
            $iklan      = Iklan::find($request->input('iklan_id'));
            $fromUser   = \Auth::user();
            $toUser     = User::find($request->input('to_user_id'));

            $newPesan   = new Pesan;
            $newPesan->from_user_id     = $fromUser->id;
            $newPesan->to_user_id       = $toUser->id;
            $newPesan->iklan_id         = $iklan->id;
            $newPesan->value            = str_replace("\r\n", "<br>", $request->input('isi_pesan'));
        // } catch (\Exception $e) {
        //     abort('404');
        // }

        if($newPesan->save()) {
            return redirect()->back()->with([
                'flash_notif'         => [  'type'      => 'success',
                                            'pesan'     => 'Berhasil mengirim pesan kepada '. $toUser->name,
                                        ]
            ]);
        } else {
            return redirect()->back()->with([
                'flash_notif'         => [  'type'      => 'danger',
                                            'pesan'     => 'Gagal mengirim pesan kepada '. $toUser->name,
                                        ]
            ]);
        }
    }
}
