<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Logic\UserRepo as UserRepo;

class SettingController extends Controller
{
    protected $userRepo;

    public function __construct(UserRepo $userRepo) {
        $this->userRepo = new UserRepo;
    }
    
    private function statusGenerator($status, $content) {
        return [
            'status'    => $status,
            'content'   => $content,
       ];
    }

    public function updateKontak(Request $request) {
        $result = $this->userRepo->updateKontak($request->input());

        if($result === 'nochange') {
            return redirect('setting/kontak#formupdatekontak')->with([
                'status'    => $this->statusGenerator('warning', '<strong>Opps!</strong> Data kontak Anda tidak ada yang berubah!')
            ]);
        }
        elseif($result) {
            return redirect('setting/kontak#formupdatekontak')->with([
                'status'    => $this->statusGenerator('success', '<strong>Selamat!</strong> Anda berhasil mengupdate data kontak Anda!')
            ]);
        } else {
            return redirect('setting/kontak#formupdatekontak')->with([
                'status'    => $this->statusGenerator('failed', 'Maaf! terjadi kesalahan. Mohon coba beberapa saat lagi.')
            ]);
        }
    }

    public function updatePassword(Request $request) {
        if($request->input('password_baru') === $request->input('password_baru_confirmation')) {
            \Auth::user()->password = bcrypt($request->input('password_baru'));
        } else {
            return redirect()->back()->with([
                'flash_notif'         => [  'type'      => 'danger',
                                            'pesan'     => 'Maaf! gagal memperbarui password!',
                                        ]
            ]);
        }
    }
}
