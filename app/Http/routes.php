<?php

Route::get('user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');

Route::auth();

Route::group(['middleware' => 'auth'], function() {

    //Dashboard
    Route::get('/dashboard', [
        'uses'  => 'HomeController@index'
    ]);

    //Iklan
    Route::get('/iklan/create', [
        'as'    => 'iklan.create',
        'uses'  => 'IklanController@create'
    ]);

    Route::get('/iklan/edit/{id_iklan}', [
        'as'    => 'iklan.edit',
        'uses'  => 'IklanController@edit'
    ]);

    Route::post('/iklan/store', [
        'as'    => 'iklan.store',
        'uses'  => 'IklanController@store'
    ]);

    Route::post('/iklan/update', [
        'as'    => 'iklan.update',
        'uses'  => 'IklanController@update'
    ]);

    Route::post('/iklan/destroy', [
        'as'    => 'iklan.destroy',
        'uses'  => 'IklanController@destroy'
    ]);

    Route::post('/iklan/promote', [
        'as'    => 'iklan.promote',
        'uses'  => 'IklanController@promote'
    ]);

    Route::post('/iklan/change', [
        'as'    => 'iklan.change',
        'uses'  => 'IklanController@changeStatus'
    ]);

    Route::get('/iklan/{tipe}', [
        'as'    => 'iklan.tipe',
        'uses'  => 'IklanController@index'
    ]);

    Route::post('/iklan/rating', [
        'as'    => 'iklan.rating.store',
        'uses'  => 'RatingController@store'
    ]);

    //Ajax Iklan
    Route::post('/iklan/getCountSubKategori', [
        'uses'  => 'AjaxController@getCountSubKategori'
    ]);

    Route::post('/iklan/detail/view', [
        'as'    => 'iklan.detail.view',
        'uses'  => 'AjaxController@getDetailIklanView'
    ]);

    Route::post('/iklan/getSubKategori', [
        'uses'  => 'AjaxController@getSubKategori'
    ]);

    // Ajax Upload Image
    Route::post('/upload/image', [
        'as'    => 'upload.image.store',
        'uses'  => 'AjaxController@storeUploadImage'
    ]);

    Route::delete('/upload/image/delete', [
        'as'    => 'upload.image.delete',
        'uses'  => 'AjaxController@deleteUploadImage'
    ]);
    
    //Pesan
    Route::post('/pesan/store', [
        'as'    => 'pesan.iklan.store',
        'uses'  => 'PesanController@store'
    ]);
    
    Route::get('/pesan/{tipe}', [
        'uses'  => 'PesanController@index'
    ]);

    Route::get('/pesan/{tipe}/{id_iklan}', [
        'as'    => 'pesan.show.byIdIklan',
        'uses'  => 'PesanController@showPesanMasukByIdIklan'
    ]);

    Route::get('/pesan/detail/detail/a', [
        'uses'  => 'PesanController@show'
    ]);

    Route::post('/pesan/balas', [
        'uses'  => 'PesanController@balas'
    ]);

    //Dompet
    Route::get('/dompet/{tipe}', [
        'uses'  => 'DompetController@index'
    ]);

    //Pengaturan
    Route::get('/setting/{tipe}', [
        'uses'  => 'HomeController@settings'
    ]);

    Route::post('/setting/kontak/update', [
        'uses'  => 'SettingController@updateKontak'
    ]);

    Route::post('/setting/password/update', [
        'uses'  => 'SettingController@updatePassword'
    ]);

    // Ajax Pengaturan
    Route::get('/setting/ajak/getKota', [
        'uses'  => 'AjaxController@getKota'
    ]);
});

Route::get('social/login/redirect/{provider}', [
    'uses' => 'Auth\AuthController@redirectToProvider', 
    'as' => 'social.login'
]);

Route::get('social/login/{provider}', 'Auth\AuthController@handleProviderCallback');

Route::group(['prefix' => 'admin', 'middleware' => 'authAdmin'], function () {
    //percobaan
    Route::get('/', 'AdminController@index');
    Route::get('user', 'AdminController@user');
    Route::get('user/blocked', 'AdminController@userBlocked');
    Route::post('user/blokir', [
        'as' => 'user.blocked',
        'uses' =>  'AdminController@blocked'
        ]);
    Route::post('user/unblokir', [
        'as' => 'user.unblocked',
        'uses' =>  'AdminController@unblocked'
        ]);
    Route::get('iklan/baru', 'AdminController@iklanbaru');
    Route::get('iklan/edit', 'AdminController@editiklan');
    Route::get('iklan/blocked', 'AdminController@blokirIklan');
    Route::get('iklan', 'AdminController@iklan');
    Route::get('komentar/moderasi', 'AdminController@moderasi');
    Route::get('komentar', 'AdminController@komentar');
    Route::get('komplain', 'AdminController@komplain');
    Route::get('laporan/keuangan', 'AdminController@keuangan');
    Route::get('laporan/jumlah_user', 'AdminController@jumlahUser');
    Route::get('setting/papaniklan', 'AdminController@papaniklan');
    Route::get('iklan/diterima/{id}', 'AdminController@approved_iklan');
    Route::post('iklan/ditolak', [
        'as' => 'iklan.ditolak',
        'uses' => 'AdminController@blocked_iklan']);
    Route::get('setting/halaman', 'AdminController@listHalaman');
    Route::get('setting/halaman/baru', 'AdminController@halaman');
    Route::post('setting/halaman/baru/terbitkan', 'AdminController@store');
    //edit per Halaman
    Route::get('setting/halaman/edit/{id}', [
        'as'    => 'edit.halaman',
        'uses'  => 'AdminController@editHalaman'
    ]);
    Route::post('setting/halaman/update', 'AdminController@updateHalaman');
    Route::post('ubah_password', 'AdminController@changePasswordAdmin');
    Route::get('setting/iklan/fasilitas', 'AdminController@list_fasilitas');
    Route::post('setting/iklan/fasilitas/input', 'AdminController@tambah_fasilitas');
    Route::post('setting/iklan/fasilitas/edit', 'AdminController@edit_fasilitas');
    Route::post('setting/iklan/fasilitas/hapus', 'AdminController@hapus_fasilitas');
});

Route::get('/', function () {
    $listKategori = DB::table('kategori_iklan')->where('level', '=', 1)->get();
    return view('welcome')->with('listKategori', $listKategori);
});

//Pencarian dengan keyword
Route::get('/cari', [
    'as'    => 'pencarian.umum',
    'uses'  => 'SearchController@generalSearch'
]);

Route::get('/{kategori}/cari', [
    'as'    => 'pencarian.umum.kategori',
    'uses'  => 'SearchController@generalSearch'
]);

//Sorting
// Route::post('/sort', [
//     'as'    => 'sort',
//     'uses'  => 'SearchController@sortingResult'
// ]);

//Detail per Iklan
Route::get('/iklan/detail/{nameUrl}', [
    'as'    => 'iklan.detail',
    'uses'  => 'IklanController@show'
]);

//Pencarian dengan kategori level 1
// Route::get('/{kategori}', [
//     'uses'  => 'SearchController@searchKategori',
//     'as'    => 'search.kategori'
// ]);

//Gambar
Route::get('img/{id_iklan}/{name_gambar}/{pixel}', [
    'as'    => 'img.iklan',
    'uses'  => 'GambarController@getImageIklan'
]);

Route::get('asset/img/{nama_asset}/{pixel}', [
    'as'    => 'asset.img',
    'uses'  => 'GambarController@getAssetImg'
]);

Route::get('/hc/{nameUrl}', [
    'as'    => 'hc.show',
    'uses'  => 'CommonController@show'
]);