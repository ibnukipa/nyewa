/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
    $('#side-menu').metisMenu();
});


$(function () {
        $('#harian').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Laporan Harian'
            },
            colors: ['#e65100'],
            xAxis: {
                categories: [
                    '1 Nov',
                    '2 Nov',
                    '3 Nov',
                    '4 Nov',
                    '5 Nov',
                    '6 Nov',
                    '7 Nov'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Rupiah'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} Rupiah </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: "Pemasukkan",
                data: [50000, 55000, 55000, 50000, 100000, 200000, 300000]
            }]
        });
    });

$(function () {
        $('#bulanan').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Laporan Bulanan'
            },
            colors: ['#e65100'],
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Rupiah'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} Rupiah </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: "Pemasukkan",
                data: [50000, 55000, 55000, 50000, 100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000]
            }]
        });
    });

$(function () {
        $('#tahunan').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Laporan Tahunan'
            },
            colors: ['#e65100'],
            xAxis: {
                categories: [
                    '2012',
                    '2013',
                    '2014',
                    '2015',
                    '2016'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Rupiah'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} Rupiah </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: "Pemasukkan",
                data: [50000, 55000, 55000, 50000, 100000]
            }]
        });
    });

$(function () {
        $('#bulanan_ju').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Laporan Bulanan'
            },
            colors: ['#e65100'],
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'user'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} user </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: "Jumlah User",
                data: [20, 30, 40, 50, 60, 70, 80, 120, 200, 250, 300, 500]
            }]
        });
    });

$(function () {
        $('#tahunan_ju').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Laporan Tahunan'
            },
            colors: ['#e65100'],
            xAxis: {
                categories: [
                    '2012',
                    '2013',
                    '2014',
                    '2015',
                    '2016'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'user'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} user </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: "Jumlah User",
                data: [200, 300, 400, 500, 600]
            }]
        });
    });

$(function () {
        $('#statistik').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Statistik Bulanan'
            },
            colors: ['#e65100'],
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'pengunjung'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} Pengunjung </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: "Jumlah Pengunjung",
                data: [200, 300, 400, 500, 600,700,800,900,1000,1100,1200,2000]
            }]
        });
    });


//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});
