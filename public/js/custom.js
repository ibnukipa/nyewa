
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = $(this).scrollTop();
var delta = 5;
var navbarHeight = $('.heading').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // Scroll Down dan Scroll Up
    if (st > lastScrollTop && st > navbarHeight){        
        if (st > navbarHeight + 200)
            $('#nav-top').addClass('up');
    } else {
        if(st < navbarHeight + 300)
            $('#nav-top').removeClass('up');
    }
    
    lastScrollTop = st;
}