#### **Quick Project Setup**

 1. `git clone https://gitlab.com/ibnukipa/laravel-monta.git`
 2. Create a database for the project
 3. From the projects root run `cp .env.example .env`
 4. Configure `.env` file
	 - setting database nya
	 - yang lain biarin dulu aja
 5. Run `composer update` from the projects root folder
 6. From the projects root folder run `php artisan key:generate`
 7. From the projects root folder run `php artisan migrate`
 8. From the projects root folder run `composer dump-autoload`
 9. From the projects root folder run `php artisan db:seed`
 10. From the projects root folder run `php artisan laravolt:indonesia:seed`
 
http://devartisans.com/articles/complete-laravel5-socialite-tuorial
https://github.com/laravolt/indonesia

#### **Reset Database**
 1. Empty Databasenya
 2. From the projects root folder run `php artisan migrate:refresh --seed`
 3. From the projects root folder run `php artisan laravolt:indonesia:seed`