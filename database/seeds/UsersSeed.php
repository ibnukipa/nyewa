<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $admin = User::firstOrCreate([
            'name' => 'Admin NyewAja',
            'email' => 'admin@nyewaja.com',
            'active' => true,
            'password' => bcrypt('admin123'),
        ]);
        $admin->roles()->attach(2);
    }
}
