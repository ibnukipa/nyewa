<?php

use Illuminate\Database\Seeder;
use App\Kategori;

class KategoriSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori_iklan')->delete();

        $parent = Kategori::create([
            'name'      => 'Mobil',
            'level'     => '1',
            'path_icon' => 'img/kategori/mobil.png'
        ]);
            $kategori = Kategori::create([
                'name'      => 'Mobil',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(1);
                $kategori->assignDetail(2);
                $kategori->assignDetail(3);
                $kategori->assignDetail(4);
            $kategori = Kategori::create([
                'name'      => 'Aksesoris',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Audio Mobil',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Sparepart',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);

        $parent = Kategori::create([
            'name'      => 'Motor',
            'level'     => '1',
            'path_icon' => 'img/kategori/motor.png'
        ]);
            $kategori = Kategori::create([
                'name'      => 'Motor',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(1);
                $kategori->assignDetail(2);
                $kategori->assignDetail(4);
            $kategori = Kategori::create([
                'name'      => 'Aksesoris',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Helm',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Sparepart',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);

        $parent = Kategori::create([
            'name'      => 'Properti',
            'level'     => '1',
            'path_icon' => 'img/kategori/properti.png'
        ]);
            $kategori = Kategori::create([
                'name'      => 'Rumah',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(5);
                $kategori->assignDetail(6);
                $kategori->assignDetail(7);
                $kategori->assignDetail(8);
                $kategori->assignDetail(9);
                $kategori->assignDetail(10);
                $kategori->assignDetail(11);
                $kategori->assignDetail(12);
            $kategori = Kategori::create([
                'name'      => 'Apartement',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(6);
                $kategori->assignDetail(7);
                $kategori->assignDetail(8);
                $kategori->assignDetail(9);
                $kategori->assignDetail(10);
                $kategori->assignDetail(11);
                $kategori->assignDetail(12);
            $kategori = Kategori::create([
                'name'      => 'Indekos',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(6);
                $kategori->assignDetail(9);
                $kategori->assignDetail(11);
                $kategori->assignDetail(12);
            $kategori = Kategori::create([
                'name'      => 'Bangunan Komersial',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(5);
                $kategori->assignDetail(6);
                $kategori->assignDetail(10);
                $kategori->assignDetail(11);
                $kategori->assignDetail(12);
            $kategori = Kategori::create([
                'name'      => 'Tanah',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(5);
                $kategori->assignDetail(11);
                $kategori->assignDetail(12);

        $parent = Kategori::create([
            'name'      => 'Elektronik & Gadget',
            'level'     => '1',
            'path_icon' => 'img/kategori/elektronik.png'
        ]);
            $kategori = Kategori::create([
                'name'      => 'Handphone',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(1);
                $kategori->assignDetail(2);
            $kategori = Kategori::create([
                'name'      => 'Kamera',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(1);
                $kategori->assignDetail(2);
            $kategori = Kategori::create([
                'name'      => 'Tablet',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(1);
                $kategori->assignDetail(2);
            $kategori = Kategori::create([
                'name'      => 'Games',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Printer',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(1);
                $kategori->assignDetail(2);
            $kategori = Kategori::create([
                'name'      => 'Laptop',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(1);
                $kategori->assignDetail(2);
            $kategori = Kategori::create([
                'name'      => 'Proyektor',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(1);
                $kategori->assignDetail(2);
            $kategori = Kategori::create([
                'name'      => 'TV',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(1);
                $kategori->assignDetail(2);
            $kategori = Kategori::create([
                'name'      => 'Lampu',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Orkes',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Lainnya',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);

        $parent = Kategori::create([
            'name'      => 'Keperluan Pribadi',
            'level'     => '1',
            'path_icon' => 'img/kategori/fashion.png'
        ]);
            $kategori = Kategori::create([
                'name'      => 'Dress',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(13);
                $kategori->assignDetail(14);
            $kategori = Kategori::create([
                'name'      => 'Kebaya',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(13);
                $kategori->assignDetail(14);
            $kategori = Kategori::create([
                'name'      => 'Sepatu & Sandal',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(13);
                $kategori->assignDetail(14);
            $kategori = Kategori::create([
                'name'      => 'Aksesoris',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Perhiasan',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Toga',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(13);
                $kategori->assignDetail(14);
            $kategori = Kategori::create([
                'name'      => 'Baju',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(13);
                $kategori->assignDetail(14);
            $kategori = Kategori::create([
                'name'      => 'Jas',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(13);
                $kategori->assignDetail(14);
            $kategori = Kategori::create([
                'name'      => 'Jaket & Sweeter',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(13);
                $kategori->assignDetail(14);
            $kategori = Kategori::create([
                'name'      => 'Jam Tangan',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);

        $parent = Kategori::create([
            'name'      => 'Hobi & Olahraga',
            'level'     => '1',
            'path_icon' => 'img/kategori/hobi.png'
        ]);
            $kategori = Kategori::create([
                'name'      => 'Pakaian Olahraga',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(13);
                $kategori->assignDetail(14);
            $kategori = Kategori::create([
                'name'      => 'Alat Musik',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Alat Olahraga',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Lapangan Olahraga',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(15);
                $kategori->assignDetail(11);
                $kategori->assignDetail(12);
            $kategori = Kategori::create([
                'name'      => 'Musik & Film',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Mainan',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Buku',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
                $kategori->assignDetail(16);
                $kategori->assignDetail(17);

        $parent = Kategori::create([
            'name'      => 'Rumah Tangga',
            'level'     => '1',
            'path_icon' => 'img/kategori/rumahtangga.png'
        ]);
            $kategori = Kategori::create([
                'name'      => 'Furniture',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);
            $kategori = Kategori::create([
                'name'      => 'Dekorasi Rumah',
                'level'     => '2',
                'parent_id' => $parent->id
            ]);

        $parent = Kategori::create([
            'name'      => 'Jasa',
            'level'     => '1',
            'path_icon' => 'img/kategori/jasa.png'
        ]);

    }
}
