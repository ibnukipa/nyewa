<?php

use Illuminate\Database\Seeder;
use App\FasilitasIklan;

class FasilitasIklanSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fasilitas_iklan')->delete();

        FasilitasIklan::create([
            'name'          => 'AC'
        ]);

        FasilitasIklan::create([
            'name'          => 'Swimming Pool'
        ]);

        FasilitasIklan::create([
            'name'          => 'Taman'
        ]);

        FasilitasIklan::create([
            'name'          => 'Garasi'
        ]);

        FasilitasIklan::create([
            'name'          => 'PAM'
        ]);

        FasilitasIklan::create([
            'name'          => 'Water Heater'
        ]);

        FasilitasIklan::create([
            'name'          => 'Telephone'
        ]);
    }
}
