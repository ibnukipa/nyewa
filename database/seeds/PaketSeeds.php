<?php

use Illuminate\Database\Seeder;
use App\Paket;
class PaketSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paket')->delete();

        Paket::create([
            'name'          => '30 Hari Top Listing',
            'description'   => 'Iklan berada di urutan paling atas sesuai kategori barang yang diiklankan; durasi 30 hari. Pastikan masa aktif iklan Anda masih 30 hari atau lebih. Hanya bisa dipakai untuk 1 (satu) iklan yang sama.',
            'value'         => 30,
            'harga'         => 75000
        ]);

        Paket::create([
            'name'          => '14 Hari Top Listing',
            'description'   => 'Iklan berada di urutan paling atas sesuai kategori barang yang diiklankan; durasi 14 hari. Pastikan masa aktif iklan Anda masih 14 hari atau lebih. Hanya bisa dipakai untuk 1 (satu) iklan yang sama.',
            'value'         => 14,
            'harga'         => 45000
        ]);

        Paket::create([
            'name'          => '7 Hari Top Listing',
            'description'   => 'Iklan berada di urutan paling atas sesuai kategori barang yang diiklankan; durasi 7 hari. Pastikan masa aktif iklan Anda masih 7 hari atau lebih. Hanya bisa dipakai untuk 1 (satu) iklan yang sama.',
            'value'         => 7,
            'harga'         => 25000
        ]);

        Paket::create([
            'name'          => '3 Hari Top Listing',
            'description'   => 'Iklan berada di urutan paling atas sesuai kategori barang yang diiklankan; durasi 3 hari. Pastikan masa aktif iklan Anda masih 3 hari atau lebih. Hanya bisa dipakai untuk 1 (satu) iklan yang sama.',
            'value'         => 3,
            'harga'         => 15000
        ]);
    }
}
