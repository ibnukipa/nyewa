<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(RolesSeed::class);
        $this->call(UsersSeed::class);
        $this->call(PaketSeeds::class);
        $this->call(DetailKategoriSeed::class);
        $this->call(KategoriSeed::class);
        $this->call(FasilitasIklanSeed::class);

        Model::reguard();
    }
}
