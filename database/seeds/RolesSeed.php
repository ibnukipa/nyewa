<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        Role::create([
            'name'          => 'perusahaan',
            'description'   => 'User yang telah login dan bisa mengelola iklan'
        ]);

        Role::create([
            'name'          => 'administrator',
            'description'   => 'User pemilik'
        ]);
    }
}
