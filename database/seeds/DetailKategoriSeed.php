<?php

use Illuminate\Database\Seeder;
use App\KategoriDetail;

class DetailKategoriSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategori_iklan_detail')->delete();

        // Mobil Motor
        KategoriDetail::create([
            'name'          => 'merk'
        ]);
        KategoriDetail::create([
            'name'          => 'tipe'
        ]);
        KategoriDetail::create([
            'name'          => 'transmisi'
        ]);
        KategoriDetail::create([
            'name'          => 'tahun'
        ]);

        //Properti
        KategoriDetail::create([
            'name'          => 'luas tanah'
        ]);
        KategoriDetail::create([
            'name'          => 'luas bangunan'
        ]);
        KategoriDetail::create([
            'name'          => 'lantai'
        ]);
        KategoriDetail::create([
            'name'          => 'kamar tidur'
        ]);
        KategoriDetail::create([
            'name'          => 'kamar mandi'
        ]);
        KategoriDetail::create([
            'name'          => 'sertifikasi'
        ]);
        KategoriDetail::create([
            'name'          => 'fasilitas'
        ]);
        KategoriDetail::create([
            'name'          => 'alamat lokasi'
        ]);

        //Keperluan Probadi
        KategoriDetail::create([
            'name'          => 'ukuran'
        ]);
        KategoriDetail::create([
            'name'          => 'warna'
        ]);

        //Hobi dan Olahraga
        KategoriDetail::create([
            'name'          => 'luas lapangan'
        ]);
        KategoriDetail::create([
            'name'          => 'pengarang'
        ]);
        KategoriDetail::create([
            'name'          => 'tahun cetak'
        ]);
    }
}
