<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGambarIklanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gambar_iklan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iklan_id')->unsigned()->index();
            $table->foreign('iklan_id')->references('id')->on('iklan')->onDelete('cascade');
            $table->string('path');
            $table->string('file_name');
            $table->string('file_mime');
            $table->string('file_size');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gambar_iklan');
    }
}
