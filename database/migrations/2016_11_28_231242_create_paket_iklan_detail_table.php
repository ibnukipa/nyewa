<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaketIklanDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paket_iklan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iklan_id')->unsigned()->index();
            $table->foreign('iklan_id')->references('id')->on('iklan')->onDelete('cascade');

            $table->integer('paket_id')->unsigned()->index();
            $table->foreign('paket_id')->references('id')->on('paket')->onDelete('cascade');
            
            $table->boolean('active')->default(0);
            $table->timestamp('start_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paket_iklan');
    }
}
