<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIklanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iklan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('status');
            $table->string('seen')->default('0');
            $table->string('url_iklan')->nullable();
            $table->integer('tempat_id');
            $table->integer('lowest_price');
            $table->integer('highest_price');
            $table->double('rating');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('kategori_id')->unsigned()->index();
            $table->foreign('kategori_id')->references('id')->on('kategori_iklan')->onDelete('no action');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('iklan');
    }
}
