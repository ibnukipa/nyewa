<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesan', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('from_user_id')->unsigned()->index();
            $table->integer('to_user_id')->unsigned()->index();
            $table->integer('iklan_id')->unsigned()->index();

            $table->string('value');
            $table->boolean('seen')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pesan');
    }
}
