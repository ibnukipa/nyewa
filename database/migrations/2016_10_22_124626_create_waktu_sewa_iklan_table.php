<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaktuSewaIklanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waktu_sewa_iklan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iklan_id')->unsigned()->index();
            $table->foreign('iklan_id')->references('id')->on('iklan')->onDelete('cascade');
            $table->string('waktu');
            $table->decimal('harga', 16, 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('waktu_sewa_iklan');
    }
}
