<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailKategori extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_kategori', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ketegori_id')->unsigned()->index();
            $table->foreign('ketegori_id')->references('id')->on('kategori_iklan')->onDelete('cascade');
            $table->integer('kategori_iklan_detail_id')->unsigned()->index();
            $table->foreign('kategori_iklan_detail_id')->references('id')->on('kategori_iklan_detail')->onDelete('no action');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_kategori');
    }
}
